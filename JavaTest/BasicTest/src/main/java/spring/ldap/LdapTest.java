package spring.ldap;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.LdapName;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.ldap.core.AuthenticationSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AbstractContextMapper;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.query.ContainerCriteria;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
//@ComponentScan(basePackages="spring.boot.ldap")
public class LdapTest implements CommandLineRunner {
    String url = "ldap://localhost:10389";
    String basedn = "dc=example,dc=com";
    String userdn = "uid=admin,ou=system";
    String password = "secret";

    @Override
    public void run(String... args) throws Exception {
        System.out.println(ldapTemplate);
        System.out.println(ldapTemplate.getContextSource());

        String credentials="secret";
        
        LDAPUserEntry userEntry = null;
        String ou = "EMC";
        LdapName ouDN = new LdapName("ou=" + ou);

        //Create ou DN
        try {
            Attribute ocattr = new BasicAttribute("objectClass");
            ocattr.add("top");
            ocattr.add("organizationalUnit");
            Attributes attrs = new BasicAttributes();
            attrs.put(ocattr);
            attrs.put("ou", ou);
            ldapTemplate.bind(ouDN, null, attrs);
            System.out.println(ouDN + " is created.");
        } catch (Exception e) {
            System.out.println(ouDN + " is already existed.");
        }

        //Query
        String userName = "nili";
        String dn = "cn=" + userName + ",ou=" + ou;
        LdapName userDN = new LdapName(dn);
        try {
            userEntry = ldapTemplate.findByDn(userDN, LDAPUserEntry.class);
            System.out.println(userEntry);
        } catch (Exception e) {
            System.out.println(dn + " is not existed");
        }

        try {
            //Delete
            ldapTemplate.unbind(userDN);
            System.out.println(userDN + " is deleted ");
        } catch (Exception e) {
            System.out.println(dn + " is not existed");
        }


        //Add
        userEntry = new LDAPUserEntry();
        userEntry.setDn(userDN);
        userEntry.setDepartment(ou);
        userEntry.setGivenName("Li");
        userEntry.setUserName(userName);
        userEntry.setSurName("ni");
        userEntry.setNtId(UUID.randomUUID().toString());
        userEntry.setUserPassword(credentials);
        System.out.println("creating "+userEntry);
        ldapTemplate.create(userEntry);
        //        addUser(userEntry);

        //Update
        userEntry.setNtId(UUID.randomUUID().toString());
        updateUser(userEntry);


        //Get all
        List<LDAPUserEntry> list = listLdapUsers();
        for (LDAPUserEntry user : list) {
            System.out.println(user);
        }

        //Get specific
        List<LDAPUserEntry> listByUserName =
            listLdapUsersByUserNameList(Arrays.asList(new String[] {userName}));
        for (LDAPUserEntry user : listByUserName) {
            System.out.println(user);
        }
        
        
        boolean flag=authenticate(userName, credentials);
        System.out.println(flag);

    }

    //////////////////////////////////////////////////////////////


    public List<LDAPUserEntry> listLdapUsers() {
        return ldapTemplate.findAll(LDAPUserEntry.class);
    }

    public List<LDAPUserEntry> listLdapUsersByUserNameList(List<String> userNameList) {
        if (userNameList == null || userNameList.isEmpty()) {
            return new ArrayList<LDAPUserEntry>();
        }

        ContainerCriteria finalContainerCriteria = null;

        for (String userName : userNameList) {
            ContainerCriteria containerCriteria = query().where("cn").is(userName);
            if (finalContainerCriteria != null) {
                finalContainerCriteria.or(containerCriteria);
            } else {
                finalContainerCriteria = containerCriteria;
            }
        }
        return ldapTemplate.find(finalContainerCriteria, LDAPUserEntry.class);
    }

    //TODO
    public boolean authenticate(String userDn, String credentials) {
        try {
            ldapTemplate.authenticate(query().where("cn").is(userDn), credentials);
            return true;
        } catch (Exception e) {
            logger.error("Login failed", e);
            return false;
        }
    }

    //TODO
    public String getOuForUser(String cn) {
        List<String> result =
            ldapTemplate.search(query().where("cn").is(cn), new AbstractContextMapper<String>() {
                @Override
                protected String doMapFromContext(DirContextOperations ctx) {
                    return ctx.getNameInNamespace();
                }
            });
        if (result.size() != 1) {
            return null;
        }

        return extractOuFromDn(result.get(0));
    }

    private String extractOuFromDn(String dn) {
        String[] spilited = dn.split(",");
        for (String element : spilited) {
            if (element.contains("ou="))
                return element.substring(element.indexOf('=') + 1, element.length());
        }
        return null;
    }


    public void updateUser(LDAPUserEntry userEntry) {
        ldapTemplate.modifyAttributes(userEntry.getDn(),
            new ModificationItem[] {
                new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute("cn", userEntry.getUserName().trim())),
                new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute("sn", userEntry.getSurName().trim())),
                new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute("ou", userEntry.getDepartment().trim())),
                new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute("givenName", userEntry.getGivenName().trim())),
                new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute("uid", userEntry.getNtId().trim()))});
    }

    public void addUser(LDAPUserEntry userEntry) {
        Attribute ocattr = new BasicAttribute("objectClass");
        ocattr.add("top");
        ocattr.add("person");
        ocattr.add("inetOrgPerson");
        ocattr.add("organizationalPerson");

        Attributes attrs = new BasicAttributes();
        attrs.put(ocattr);
        attrs.put("cn", StringUtils.trimToEmpty(userEntry.getUserName()));
        attrs.put("sn", StringUtils.trimToEmpty(userEntry.getSurName()));
        attrs.put("ou", StringUtils.trimToEmpty(userEntry.getDepartment()));
        attrs.put("givenName", StringUtils.trimToEmpty(userEntry.getGivenName()));
        attrs.put("uid", StringUtils.trimToEmpty(userEntry.getNtId()));

        ldapTemplate.bind(userEntry.getDn(), null, attrs);
        //        ldapTemplate.bind("uid=" + userEntry.getUserName().trim(), null, attrs);
    }

    //    public LDAPUserEntry getUserById(String uid, String ou) {
    //        String filter = "(&(objectclass=inetOrgPerson)(uid=" + uid + "))";
    //        List<LDAPUserEntry> list = ldapTemplate.search("ou=" + ou, filter, (AttributesMapper<LDAPUserEntry>) attributes -> {
    //            LDAPUserEntry user = new LDAPUserEntry();
    //
    //            Attribute a = attributes.get("cn");
    //            if (a != null)
    //                user.setUserName((String) a.get());
    //
    //            a = attributes.get("uid");
    //            if (a != null)
    //                user.setUsername((String) a.get());
    //
    //            return user;
    //        });
    //        if (list.isEmpty())
    //            return null;
    //        return list.get(0);
    //    }



    ///////////////////////////////////////////////////////////

    @Autowired
    private LdapTemplate ldapTemplate;

    Logger logger = LoggerFactory.getLogger(LdapTest.class);

    //ldap connection configuration
    //For ApacheDS
    @Bean
    public LdapContextSource contextSource() {
        LdapContextSource contextSource = new LdapContextSource();
        contextSource.setUrl(url);
        contextSource.setBase(basedn);
        contextSource.setUserDn(userdn);
        contextSource.setPassword(password);
        return contextSource;
    }

    public LdapContextSource contextSource2() {
        LdapContextSource cs = new LdapContextSource();
        cs.setCacheEnvironmentProperties(false);
        cs.setUrl(url);
        cs.setBase(basedn);
        cs.setAuthenticationSource(new AuthenticationSource() {
            @Override
            public String getCredentials() {
                return password;
            }

            @Override
            public String getPrincipal() {
                return userdn;
            }
        });
        return cs;
    }

    @Bean
    public LdapTemplate ldapTemplate(LdapContextSource contextSource) {
        return new LdapTemplate(contextSource);
    }



    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(LdapTest.class);
        app.setWebEnvironment(false);
        app.run(args);
    }


}

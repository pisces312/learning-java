import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Pattern;

import org.junit.Test;

import $.nili.tools.JsonUtil;


public class _Main {

    public static void main(String[] args) {

        Stack<Integer> s = new Stack<>();
        s.add(1);
        s.get(0);
        s.pop();
        s.peek();

//        int[] a = new int[1];
//        a.toString();
//        System.out.println(a.getClass());
//        System.out.println(a.getClass().getSuperclass());

        Runnable r = new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

            }
        };

        System.out.println(Runnable.class.getSuperclass());


        HashMap<String, String> map = new HashMap<>();

        //Only have one "null" key and the value can also be 'null'
        //It can be updated by data
        map.put(null, "a");
        map.put(null, null);

        //Different keys can all have 'null' value
        map.put("a", null);
        map.put("b", null);


        System.out.println(map);


    }

    @Test
    public void testNotifyOrder() {
        Object obj = new Object();
        synchronized (obj) {
            obj.notifyAll();
            System.out.println("notified");
            try {
                obj.wait(5000);
                System.out.println("finished");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }


    @Test
    public void testSuperCtor() {
        class Parent {
            protected int a;

            Parent() {
                a = 2;
            }
        }
        class Child extends Parent {
            Child() {

            }

            public void print() {
                System.out.println(a);
            }


        }
        new Child().print();
    }

    @Test
    public void testConvertLongToTime() {

        //        long time=Long.valueOf("1484555151000");
        long time = Long.valueOf("1490839239000");
        //        long time=Long.valueOf("1483239600000");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        System.out.println(sdf.format(new Date(time)));
    }

    @Test
    public void testReg() {
        boolean match = Pattern.matches("^jdbc:hive2://.*$", "jdbc:hive2://172.16.9.98:10000");
        //        boolean match = Pattern.matches("^jdbc:hive2:", "jdbc:hive2://172.16.9.98:10000");
        //        boolean match = Pattern.matches("^jdbc:hive2://", "jdbc:hive2://172.16.9.98:10000");
        System.out.println(match);
    }

    @Test
    public void testJDBCURL() {
        //        URI uri=URI.create("jdbc:hive2://123.123.123.123:10000");



        //        UriBuilder builder= UriBuilder.fromUri("jdbc:hive2://123.123.123.123:10000");

        //        System.out.println(builder.port(10002).toString());


        //        URI uri=URI.create("jdbc:hive2://123.123.123.123:10000");
        //        URI uri=URI.create("//123.123.123.123:10000");
        URI uri = URI.create("hive2://123.123.123.123:10000");
        //        
        System.out.println(uri);
        System.out.println(uri.getScheme());
        System.out.println(uri.getHost());
    }

    @Test
    public void testJson() {
        Map map = JsonUtil.fromJson("{a.b:\"c\"}", Map.class);
        //        Map map=JsonUtil.fromJson("{\"a.b\":\"c\"}", Map.class);
        //        Map map=JsonUtil.fromJson("{\"a\":\"b\"}", Map.class);
        System.out.println(map);
    }

    @Test
    public void testNullAppend() {
        System.out.println(null + "abc");
    }

    @Test
    public void testURI() {
        String targetURl = "mongodb://172.16.64.156:27017";
        //        String targetURl = "/var/dac/11111.csv";
        //        String targetURl = "hdfs://abc:8020/var/dac/11111.csv";
        URI uri;
        try {
            uri = new URI(targetURl);
            System.out.println(uri.getHost());
            System.out.println(uri.getPath());
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //        URI uri = URI.create(targetURl);

        //        uri.getPath()

    }

    @Test
    public void testTrim() {
        String a = "n\n";
        System.out.println(a);
        System.out.println("n".equals(a));
        System.out.println("n".equals(a.trim()));
    }

    @Test
    public void testStrListClassType() {
        System.out.println(List.class);
    }

    @Test
    public void testMapWithOnlyKey() {
        Map<String, String> map = new HashMap<>();
        map.put("a", null);


    }

    private interface Interface1 {
        void func1();

    }

    private interface Interface2 {
        void func2();
    }

    private interface Interface3 extends Interface1, Interface2 {

    }

    private abstract class TestAbstractClass {

    }

    @Test
    public void testParentOfArray() {

        System.out.println(Object.class.getName());
        System.out.println(int[].class.getSuperclass().getName());
    }

    @Test
    public void testDoublePrecision() {
        System.out.println(3 * 0.1 == 0.3);
    }


    public static void test() {
        // 26,26+26*26
        // String str = "a";
        // String str = "z";
        String str = "abcdefg";

        long w = 1;
        long p = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            char ch = str.charAt(i);
            int t = ch - 'a' + 1;
            p += w * t;
            System.out.println("cur " + ch + " " + (p - 1));
            w *= 26;
        }

        System.out.println("postion " + (p - 1));
    }

    public static void test2(final int a) {
        // 26,26+26*26
        // String str = "a";
        // String str = "z";
        String str = "aabadfzdfa";
        BigInteger w = BigInteger.ONE;
        BigInteger p = BigInteger.ZERO;
        BigInteger c = new BigInteger("26");
        Hashtable<Character, BigInteger> table = new Hashtable();
        char ch;
        int i;
        for (ch = 'a', i = 1; ch <= 'z'; ch++, i++) {
            table.put(Character.valueOf(ch), new BigInteger(String.valueOf(i)));
            // table.put(new Character(ch), new BigInteger(String.valueOf(i)));
        }

        for (i = str.length() - 1; i >= 0; i--) {
            ch = str.charAt(i);
            p = p.add(w.multiply(table.get(new Character(ch))));
            System.out.println("cur " + ch + " " + (p.subtract(BigInteger.ONE)));
            w = w.multiply(c);
        }
        System.out.println("postion " + (p.subtract(BigInteger.ONE)));
    }



}


package core.thread;

import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;

import org.junit.Test;

public class StopThreadTest {

    //Use stop
    public final class Container1 implements Runnable {
        private final Vector<Integer> vector = new Vector<Integer>(10000);
        private int c = 0;

        public Vector<Integer> getVector() {
            return vector;
        }

        public int getCount() {
            return c;
        }

        @Override
        public synchronized void run() {
            Random number = new Random(123L);
            int max = vector.capacity();
            while (c < max) {
                vector.add(number.nextInt(100));
                ++c;
            }
        }
    }

    /**
     * vector's size and actually added numbers can be different
     * 
     * E.g. vector's size is 5608
     * 
     * added number count is 6161
     * 
     * @throws InterruptedException
     */

    @Test
    public void testContainer1() throws InterruptedException {
        Container1 c = new Container1();
        Thread thread = new Thread(c);
        thread.start();
        Thread.sleep(1);
        try {
            thread.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Vector<Integer> v = c.getVector();
        System.out.println(v.size());
        System.out.println(c.getCount());
    }


    //Use volatile flag
    public final class Container2 implements Runnable {
        private final Vector<Integer> vector = new Vector<Integer>(10000);
        private volatile boolean done = false;

        public Vector<Integer> getVector() {
            return vector;
        }

        public void shutdown() {
            done = true;
            System.out.println("2 setdone");
        }

        @Override
        public synchronized void run() {
            System.out.println("1 run");
            Random number = new Random(123L);
            int i = vector.capacity();
            while (!done && i > 0) {
                vector.add(number.nextInt(100));
                i--;
            }
            System.out.println("3 exit");
        }


    }

    @Test
    public void testContainer2() throws InterruptedException {
        Container2 container = new Container2();
        Thread thread = new Thread(container);
        thread.start();
        Thread.sleep(1);
        container.shutdown();
    }


    //Use interrupt
    public final class Container3 implements Runnable {
        private final Vector<Integer> vector = new Vector<Integer>(10000);

        public Vector<Integer> getVector() {
            return vector;
        }

        @Override
        public synchronized void run() {
            Random number = new Random(123L);
            int i = vector.capacity();
            while (i > 0) {
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                if(Thread.interrupted()) {
                    System.out.println("2 interrupted");
                    break;
                }
                vector.add(number.nextInt(100));
                i--;
            }
            System.out.println("3 exit");
        }


    }

    @Test
    public void testContainer3() throws InterruptedException {
        Container3 c = new Container3();
        Thread thread = new Thread(c);
        thread.start();
        Thread.sleep(1);
//        System.out.println("1 interrupt other thread");
        thread.interrupt();
        Hashtable<String,String> t=new Hashtable<>();
//        t.put("a", null);
    }

}

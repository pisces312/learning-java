package core.methodhandle.invokedynamic2;

import java.lang.invoke.CallSite;
import java.lang.invoke.ConstantCallSite;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;

//
//Call void static method:
//
//    public static void func(java.lang.String);
//      descriptor: (Ljava/lang/String;)V
//      flags: ACC_PUBLIC, ACC_STATIC
//      Code:
//        stack=1, locals=1, args_size=1
//           0: aload_0
//           1: invokedynamic #20,  0             // InvokeDynamic #0:foo:(Ljava/lang/String;)V
//           6: return
//  }
//  BootstrapMethods:
//    0: #17 invokestatic core/methodhandle/invokedynamic2/InvokeDynamicDemo.bootstrap:(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;
//      Method arguments:
//
//
//Call instance method:
//public static java.lang.String func();
//descriptor: ()Ljava/lang/String;
//flags: ACC_PUBLIC, ACC_STATIC
//Code:
//  stack=1, locals=0, args_size=0
//     0: invokedynamic #22,  0             // InvokeDynamic #0:foo:()Ljava/lang/String;
//     5: areturn
//}
//BootstrapMethods:
//0: #17 invokestatic core/methodhandle/invokedynamic2/InvokeDynamicDemo.bootstrapForIns:(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/Object;)Ljava/lang/invoke/CallSite;
//Method arguments:
//  #19 hello

public class InvokeDynamicDemo {

    //user-defined bootstrap method
    public static CallSite bootstrap(Lookup lookup, String name, MethodType callSiteType) {
        System.out.println("name=" + name + ", desc=" + callSiteType.toMethodDescriptorString());
        final MethodHandle mh;

        try {
            if (name.equals("foo")) {
                //trigger the static method implemented in InvokeDynamicDemo
                mh = lookup.findStatic(InvokeDynamicDemo.class, "fooImpl",
                    callSiteType.changeReturnType(Void.TYPE));
            } else if (name.equals("bar")) {
                mh = lookup.findStatic(InvokeDynamicDemo.class, "implForBar",
                    callSiteType.changeReturnType(Void.TYPE));
            } else {
                throw new BootstrapMethodError();
            }
        } catch (IllegalAccessException | NoSuchMethodException e) {
            throw new BootstrapMethodError(e);
        }
        return new ConstantCallSite(mh);
    }

    //user-defined logic    
    public static void fooImpl(String s) {
        System.out.println("fooImpl invoked with " + s);
    }

    public static void implForBar(String s) {
        System.out.println("implForBar invoked with " + s);
    }

    //Util method for demo
    private static void invokeStaticFuncByDyn(String methodName, MethodType methodType, String arg)
        throws Throwable {
        new DynamicIndy().invokeDynamic(methodName, methodType, InvokeDynamicDemo.class,
            "bootstrap", MethodType.methodType(CallSite.class, MethodHandles.Lookup.class,
                String.class, MethodType.class))
            .invokeExact(arg);
    }

    /*        trigger          */
    public static void main(String[] args) throws Throwable {
        MethodType mt = MethodType.methodType(Void.TYPE, String.class);
        invokeStaticFuncByDyn("foo", mt, "x");
        invokeStaticFuncByDyn("bar", mt, "y");
        testInvokeInstanceMethod();

    }



    /*****************************************************************************/

    //call instance's method
    //encapsulate bsm parameters
    private static <T> T invokeInsFuncDyn(String methodName, MethodType methodType, T ins,
        Object... methodArgs) throws Throwable {
        MethodHandle mh = new DynamicIndy().invokeDynamic(methodName, methodType,
            //setup bsm parameters
            InvokeDynamicDemo.class, "bootstrapForIns", MethodType.methodType(CallSite.class,
                MethodHandles.Lookup.class, String.class, MethodType.class, Object.class),
            ins);
        if (methodArgs.length == 0) {
//            System.out.println("methodArgs.length==0");
            //throw error if use invokeExact, because return value is Object
            return (T) mh.invoke();
        }

        return (T)mh.invoke(methodArgs);
    }

    private static void testInvokeInstanceMethod() throws Throwable {
        MethodType mt = MethodType.methodType(String.class);

        //Correct
        //To use invokeExact, must ensure to cast to correct return type explicitly
//        String result = (String) new DynamicIndy().invokeDynamic("foo", mt, InvokeDynamicDemo.class,
//            "bootstrap2", MethodType.methodType(CallSite.class, MethodHandles.Lookup.class,
//                String.class, MethodType.class, Object.class),
//            "hello").invokeExact();
        //
        String instance="hello";
        String result = invokeInsFuncDyn("foo", mt, instance);

        System.out.println(result);
    }

    //user-defined bootstrap method
    public static CallSite bootstrapForIns(Lookup lookup, String name, MethodType callSiteType,
        Object instance) {
        System.out.println("name=" + name + ", desc=" + callSiteType.toMethodDescriptorString()
            + ", instance=" + instance);
        final MethodHandle mh;
        try {
            if (name.equals("foo")) {
                //You can load any method
                mh = lookup.findVirtual(String.class, "toUpperCase", callSiteType)
                    .bindTo((String) instance);
            } else {
                throw new BootstrapMethodError();
            }
        } catch (IllegalAccessException | NoSuchMethodException e) {
            throw new BootstrapMethodError(e);
        }
        return new ConstantCallSite(mh);
    }

}

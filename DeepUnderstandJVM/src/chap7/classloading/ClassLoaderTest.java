package chap7.classloading;

import java.io.IOException;
import java.io.InputStream;

/**
 * Class is loaded by different class loader
 * 
 * instanceof will show false
 */
public class ClassLoaderTest {

    public static void main(String[] args) throws Exception {

        //Custom class loader
        ClassLoader myLoader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                try {
                    String fileName = name.substring(name.lastIndexOf(".") + 1) + ".class";
                    InputStream is = getClass().getResourceAsStream(fileName);
                    if (is == null) {
                        return super.loadClass(name);
                    }
                    byte[] b = new byte[is.available()];
                    is.read(b);
                    return defineClass(name, b, 0, b.length);
                } catch (IOException e) {
                    throw new ClassNotFoundException(name);
                }
            }
        };

        Object obj = myLoader.loadClass(ClassLoaderTest.class.getName()).newInstance();

        System.out.println(obj.getClass());
        
        //!! false
        System.out.println(obj instanceof ClassLoaderTest);
    }
}


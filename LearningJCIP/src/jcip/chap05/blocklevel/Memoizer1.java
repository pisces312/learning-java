package jcip.chap05.blocklevel;

import java.math.BigInteger;
import java.util.*;

import jcip.annotations.*;
import net.jcip.examples.Computable;

/**
 * Memoizer1
 *
 * Initial cache attempt using HashMap and synchronization
 * 
 *
 * @author Brian Goetz and Tim Peierls
 */
public class Memoizer1 <A, V> implements Computable<A, V> {
    //HashMap is not thread safe. Need client locking
    @GuardedBy("this") private final Map<A, V> cache = new HashMap<A, V>();
    private final Computable<A, V> c;

    public Memoizer1(Computable<A, V> c) {
        this.c = c;
    }

    //Sync whole function, poor performance
    public synchronized V compute(A arg) throws InterruptedException {
        V result = cache.get(arg);
        if (result == null) {
            result = c.compute(arg);
            cache.put(arg, result);
        }
        return result;
    }
}


class ExpensiveFunction
        implements Computable<String, BigInteger> {
    public BigInteger compute(String arg) {
        // after deep thought...
        return new BigInteger(arg);
    }
}

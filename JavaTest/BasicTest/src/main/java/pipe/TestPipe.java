/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pipe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class TestPipe {

    static class Sender implements Runnable {

        public PipedOutputStream s = new PipedOutputStream();

        public void run() {
            try {
//                BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
//                s.write(b.readLine().getBytes("gb2312"));     //向管道中写数据
                String str = "Hello";
                s.write(str.getBytes());
                System.out.println("send " + str);

            } catch (Exception e) {
            }


        }
    }

    static class Receiver implements Runnable {

        public PipedInputStream r = new PipedInputStream();

        public void run() {
            try {

                byte b[] = new byte[1024];                
                int len = r.read(b);                     //如果管道流中没有数据可读，则该线程处于等待状态，直到管道中有数据可读
                System.out.println("receive " + new String(b, 0, len));
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {


        try {
            Sender x = new Sender();
            Receiver y = new Receiver();
            x.s.connect(y.r);
            new Thread(y).start(); //开启新线程，但此时管道中没有数据可读，所以该线程执行到r.read(b)时会被挂起
            //new   Thread(x).start();
            //System.out.println("ok");
//                int i = 0;
//                while (i < 10) {
//                    System.out.println(Thread.currentThread());
//                    Thread.sleep(1000);
//                    i++;
//                }
            try {
                //开启新线程，但此时管道中没有数据可读，所以该线程执行到r.read(b)时会被挂起
                //new   Thread(x).start();
                //System.out.println("ok");
                Thread.sleep(1000); //确保马上启动新线程
            } catch (InterruptedException ex) {
                Logger.getLogger(TestPipe.class.getName()).log(Level.SEVERE, null, ex);
            }
            new Thread(x).start(); //开启另一个新线程，向管道中写数据
        } catch (IOException ex) {
            Logger.getLogger(TestPipe.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

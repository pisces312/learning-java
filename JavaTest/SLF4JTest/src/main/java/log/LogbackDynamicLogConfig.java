package log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

public class LogbackDynamicLogConfig {
    final static Logger logger = LoggerFactory.getLogger(LogbackDynamicLogConfig.class);

    public static void main(String[] args) {
        // assume SLF4J is bound to logback in the current environment
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

        try {
            JoranConfigurator configurator = new JoranConfigurator();
            configurator.setContext(context);
            System.out.println(configurator.getContext());


            // Call context.reset() to clear any previous configuration, e.g. default
            // configuration. For multi-step configuration, omit calling context.reset().
//            context.reset();
//            if(args.length>0)
            //File name
//            configurator.doConfigure(args[0]);
        } catch (Exception e) {
            // StatusPrinter will handle this
            e.printStackTrace();
        }
        StatusPrinter.printInCaseOfErrorsOrWarnings(context);




        logger.debug("debug log");
        logger.info("Entering application.");


        logger.info("Exiting application.");
    }

}

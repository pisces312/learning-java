package core.debug;

import java.util.Map;

public class GenerateThreadDump {


    public static void main(String[] args) {
        
        String threaddump=new GenerateThreadDump().build();
        System.out.println(threaddump);

    }

    public String build() {
        StringBuilder output = new StringBuilder(1000);
        for (Map.Entry<Thread, StackTraceElement[]> stackTrace : Thread.getAllStackTraces()
            .entrySet()) {
            appendThreadStackTrace(output, (Thread) stackTrace.getKey(),
                (StackTraceElement[]) stackTrace.getValue());
        }
        return output.toString();
    }


    private void appendThreadStackTrace(StringBuilder output, Thread thread,
        StackTraceElement[] stack) {
        // 忽略当前线程的堆栈信息
        if (thread.equals(Thread.currentThread())) {
            return;
        }
        output.append(thread).append("\n");
        for (StackTraceElement element : stack) {
            output.append("\t").append(element).append("\n");
        }
    }



}

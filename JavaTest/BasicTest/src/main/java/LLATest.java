
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LLATest {

    static class IPAddress {

        public static final int TYPE_UNKNOWN = 0;

        public static final int TYPE_IPV4    = 1;

        public static final int TYPE_IPV6    = 2;

        /**
         * IP string
         */
        private String          ip           = "";

        /**
         * Type of the IP address, possible values are<br>
         * <li>0 Unknown</li><br>
         * <li>1 IPv4 Only</li><br>
         * <li>2 IPv6 Only</li><br>
         */
        private int             type         = 0;

        /**
         * Indicator of if this address is a floating IP address
         */
        private boolean         floating     = false;

        public boolean isFloating() {
            return floating;
        }

        public void setFloating(boolean floating) {
            this.floating = floating;
        }

        public IPAddress(String ip, int type) {
            this.ip = ip;
            this.type = type;
        }

        /**
         * @return the ip
         */
        public String getIp() {
            return ip;
        }

        /**
         * @param ip
         *            the ip to set
         */
        public void setIp(String ip) {
            this.ip = ip;
        }

        /**
         * @return the type
         */
        public int getType() {
            return type;
        }

        /**
         * @param type
         *            the type to set
         */
        public void setType(int type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "IPAddress [ip=" + ip + ", type=" + type + ", floating="
                    + floating + "]";
        }
    }

    /**
     * select primary ip according to the priorities of ip as below, IPv6
     * -floating IPv4 -floating IPv6 (global) -fixed IPv6 LLA IPv4 (global) IPV4
     * (local)
     * 
     * the connectivity will be tested by ping in the function
     * 
     * The ut is located at
     * com.ibm.director.hw.bc2.autodiscovery.PrimaryIpSelectTest
     * 
     * @param ipAddressList
     * @return
     */
    public static IPAddress primaryIpSelector(List<IPAddress> ipAddresses) {
        final String methodName = "primaryIpSelector";
        logger.entering(CLASS_NAME, methodName);
        if (ipAddresses == null || ipAddresses.isEmpty()) {
            logger.logp(Level.INFO, CLASS_NAME, methodName, "ip list is null");
            // if ip list is null,return null
            return null;
        }
        // sort the ip according to priority, the first element will have the
        // highest priority
        Collections.sort(ipAddresses, ipPriorityComparator);
        // create a new container for removing operation later
        List<IPAddress> ipAddressList = new ArrayList<IPAddress>(ipAddresses);
        boolean canBeConnected = false;
        Iterator<IPAddress> itr = ipAddressList.iterator();
        IPAddress ip = null;
        while (itr.hasNext()) {
            ip = itr.next();
            try {
                logger.logp(Level.INFO, CLASS_NAME, methodName,
                        "IP:" + ip.getIp());
                InetAddress netAddr = InetAddress.getByName(ip.getIp());
                if (netAddr != null) {
                    // eliminate loopback address (127.0.0.1)
                    // ipv4 0.0.0.0
                    // ipv6 :: or 0:0:0:0:0:0:0:0 or 00:00:00:00:00:00:00:00
                    if (netAddr.isLoopbackAddress()
                            || netAddr.isAnyLocalAddress()) {
                        itr.remove();
                        ip = null;
                        logger.logp(Level.INFO, CLASS_NAME, methodName,
                                "eliminate a loopback IP");
                        continue;
                    }
                }
                canBeConnected = pingIPAddress(ip.getIp());
                System.out.println("ping " + ip.getIp() + " " + canBeConnected);
            } catch (UnknownHostException unknownHost) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.logp(Level.SEVERE, CLASS_NAME, methodName,
                            "Error happened when ping IP:" + ip.getIp(),
                            unknownHost);
                }
                continue;
            } catch (IOException io) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.logp(Level.SEVERE, CLASS_NAME, methodName,
                            "Error happened when ping IP:" + ip.getIp(), io);
                }
                continue;
            } catch (Exception e) {
                if (logger.isLoggable(Level.SEVERE)) {
                    if (ip != null)
                        logger.logp(Level.SEVERE, CLASS_NAME, methodName,
                                "Unknown Error for " + ip.getIp(), e);
                }
                continue;
            }
            if (canBeConnected) {
                break;
            }
        }
        if ((!canBeConnected) && (!ipAddressList.isEmpty())) {
            logger.logp(Level.INFO, CLASS_NAME, methodName,
                    "no ip is reachable, return the most priority one");
            // if no ip is reachable, return the most priority one, it can't be
            // loopback address.
            // TODO: return most probably one by matching submask
            ip = ipAddressList.get(0);
        }
        if (ip != null) {
            logger.logp(Level.INFO, CLASS_NAME, methodName,
                    "Find the primary IP");
        }
        logger.exiting(CLASS_NAME, methodName);
        return ip;
    }

    /**
     * a comparator for sorting IP priority
     * 
     * if ip1's priority is higher or equal than ip2's, HIGHER_PRIORITY value
     * will be returned. Otherwise LOWER_PRIORITY will be returned
     */
    public static final Comparator<IPAddress> ipPriorityComparator    = new Comparator<IPAddress>() {
                                                                          // the higher or lower is for ip1.
                                                                          private static final int HIGHER_PRIORITY = -1;
                                                                          private static final int LOWER_PRIORITY  = 1;

                                                                          public int compare(IPAddress ip1,
                                                                                  IPAddress ip2) {
                                                                              if (ip1.isFloating() == ip2.isFloating()) {
                                                                                  // 1. same floating values
                                                                                  if (ip1.getType() == ip2.getType()) {
                                                                                      // 2. same ip versions
                                                                                      // global has higher priority
                                                                                      if (!isLinkLocalAddress(ip1.getIp())) {
                                                                                          return HIGHER_PRIORITY;
                                                                                      }

                                                                                      if (!isLinkLocalAddress(ip2.getIp())) {
                                                                                          return LOWER_PRIORITY;
                                                                                      }
                                                                                      // both are not LLA,choose first
                                                                                      return HIGHER_PRIORITY;
                                                                                  }
                                                                                  // 2. different ip versions
                                                                                  // ipv6 has higher priority
                                                                                  if (IPAddress.TYPE_IPV6 == ip1.getType()) {
                                                                                      return HIGHER_PRIORITY;
                                                                                  }
                                                                                  return LOWER_PRIORITY;
                                                                              }
                                                                              // 1. different floating values
                                                                              // floating one has higher priority
                                                                              if (ip1.isFloating()) {
                                                                                  return HIGHER_PRIORITY;
                                                                              }
                                                                              return LOWER_PRIORITY;
                                                                          }
                                                                      };
    static String                             CLASS_NAME              = "LLATest";
    String                                    ipv6GlobalFloating      = "2002:c058:6301::c058:6301";                   // can pass ping
    // String ipv6GlobalFixed = "2002:97b:e78d::97b:e78d";//can pass ping
    String                                    ipv6GlobalFixed         = "2002:97b:e78d::97b:e78f";                     // can not pass ping
    String                                    ipv6LLA                 = "fe80::20c:29ff:fec8:6ba1";                    // can not pass ping

    String                                    ipv4GlobalFloating      = "9.123.198.254";
    // String ipv4Global = "9.123.198.220";
    String                                    ipv4Global              = "9.123.198.254";                               // can not pass ping

    String                                    ipv4Local               = "169.254.1.9";

    // String ipv6FailedPing = "fe80::20c:29ff:fec8:6ba1";
    static List<IPAddress>                    ipAddressList           = new ArrayList<IPAddress>();

    // private static final String PKG = LLATest.class.getPackage().getName();
    private static final Logger               logger                  = Logger.getLogger("LLATest");

    IPAddress                                 ipv6GlobalFloatingAddr1 = new IPAddress(ipv6GlobalFloating,
                                                                              IPAddress.TYPE_IPV6);

    IPAddress                                 ipv4GlobalFloatingAddr1 = new IPAddress(ipv4GlobalFloating,
                                                                              IPAddress.TYPE_IPV4);

    IPAddress                                 ipv6GlobalFixedAddr1    = new IPAddress(ipv6GlobalFixed,
                                                                              IPAddress.TYPE_IPV6);

    IPAddress                                 ipv6LLAAddr1            = new IPAddress(ipv6LLA, IPAddress.TYPE_IPV6);

    IPAddress                                 ipv4GlobalAddr1         = new IPAddress(ipv4Global, IPAddress.TYPE_IPV4);

    IPAddress                                 ipv4LocalAddr1          = new IPAddress(ipv4Local, IPAddress.TYPE_IPV4);

    static int                                PING_TIMEOUT            = 10;

    public static boolean pingIPAddress(String ipAddress)
            throws UnknownHostException, IOException {
        // final String methodName = "pingIPAddress";
        // logger.entering(CLASS_NAME, methodName);

        InetAddress netAddr = InetAddress.getByName(ipAddress);
        boolean result = false;
        if (netAddr != null) {
            result = netAddr.isReachable(PING_TIMEOUT);
        }
        // if (logger.isLoggable(Level.INFO)) {
        // logger.logp(Level.INFO, CLASS_NAME, methodName, ipAddress +
        // " ping result: " + result);
        // }

        // logger.exiting(CLASS_NAME, methodName);
        return result;
    }

    public static boolean isLinkLocalAddress(String ip) {
        try {
            return InetAddress.getByName(ip).isLinkLocalAddress();
        } catch (Exception e) {}
        return false;
    }

    public static void pingIP(IPAddress i) {
        System.out
                .println(i.getIp() + " " + i.getType() + " " + i.isFloating());
    }

    public static void printSortedIPs() {
        if (ipAddressList != null && !ipAddressList.isEmpty()) {
            for (IPAddress i : ipAddressList) {
                pingIP(i);
            }
        }
    }

    /**
     * no reachable both ipv4 and ipv6 are floating, choose ipv6
     */

    public void testCase1() {
        ipAddressList.clear();
        // pass
        // ipAddressList.add(ipv6GlobalFloatingAddr1);
        // ipAddressList.add(ipv6GlobalFixedAddr1);
        // ipAddressList.add(ipv6LLAAddr1);
        // ipAddressList.add(ipv4GlobalAddr1);
        // ipAddressList.add(ipv4LocalAddr1);
        // pass

        // pass
        // ipAddressList.add(ipv6GlobalFixedAddr1);
        // ipAddressList.add(ipv4LocalAddr1);
        // ipAddressList.add(ipv6LLAAddr1);
        // ipAddressList.add(ipv6GlobalFloatingAddr1);
        // ipAddressList.add(ipv4GlobalAddr1);

        // ipAddressList.add(ipv6LLAAddr1);
        // ipAddressList.add(ipv6GlobalFixedAddr1);
        // ipAddressList.add(ipv4LocalAddr1);
        // ipAddressList.add(ipv6GlobalFloatingAddr1);
        // ipAddressList.add(ipv4GlobalAddr1);
        ipAddressList.add(ipv4LocalAddr1);
        ipAddressList.add(ipv4GlobalAddr1);
        ipAddressList.add(ipv4GlobalFloatingAddr1);
        ipAddressList.add(ipv6LLAAddr1);
        ipAddressList.add(ipv6GlobalFixedAddr1);
        ipAddressList.add(ipv6GlobalFloatingAddr1);
        IPAddress ip = primaryIpSelector(ipAddressList);
        pingIP(ip);
        printSortedIPs();
        // Assert.assertTrue(ip == ipv6GlobalFloatingAddr1);
    }

    /**
     * no reachable choose highest
     */

    public void testCase2() {
        ipAddressList.clear();
        String ip_4 = "169.1.5.32";
        String ip_6_static = "fd8c:215d:178e:c0de:a17:f4ff:fe84:1eef";
        String ip_lla = "fe80::a17:f4ff:fe84:1eef";

        // 1
        IPAddress ip_4_addr = new IPAddress(ip_4, IPAddress.TYPE_IPV4);
        // 2
        IPAddress ip_6_static_addr = new IPAddress(ip_6_static,
                IPAddress.TYPE_IPV6);
        ip_6_static_addr.setFloating(false);
        // 3
        IPAddress ip_lla_addr = new IPAddress(ip_lla, IPAddress.TYPE_IPV6);

        ipAddressList.add(0, ip_4_addr);
        ipAddressList.add(1, ip_lla_addr);
        ipAddressList.add(2, ip_6_static_addr);

        IPAddress selectedIP = primaryIpSelector(ipAddressList);
        System.out.println("primay ip found is " + selectedIP.getIp() + "\n");
        printSortedIPs();
        // Assert.assertTrue(selectedIP == ip_6_static_addr);
    }

    /**
     * only loopback return null
     */

    public void testCase3() {
        ipAddressList.clear();
        String ip_4 = "127.0.0.1";
        IPAddress ip_4_addr = new IPAddress(ip_4, IPAddress.TYPE_IPV4);

        ipAddressList.add(ip_4_addr);
        IPAddress ip = primaryIpSelector(ipAddressList);
        if (ip == null) {
            System.out.println("null");
        } else {
            System.out.println("not null");
        }
        printSortedIPs();
        // Assert.assertTrue(ip == null);

    }

    /**
     * 0.0.0.0
     */

    public void testCase4() {
        ipAddressList.clear();
        String ip_4 = "0.0.0.0";
        IPAddress ip_4_addr = new IPAddress(ip_4, IPAddress.TYPE_IPV4);

        ipAddressList.add(ip_4_addr);
        IPAddress ip = primaryIpSelector(ipAddressList);
        if (ip == null) {
            System.out.println("null");
        } else {
            System.out.println("not null");
        }
        printSortedIPs();
        // Assert.assertTrue(ip == null);

    }

    public static void testLLAping(String [] args) {
        if (args.length == 0) {
            return;
        }
        try {
            System.out.println(pingIPAddress(args[0]));
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void test5() {
        logger.setLevel(Level.INFO);

        ipAddressList.clear();
        String ip_4 = "10.9.28.10";
        String ipLLA = "fe80::5ef3:fcff:fe25:e091";
        IPAddress ip_4_addr = new IPAddress(ip_4, IPAddress.TYPE_IPV4);
        IPAddress ipLLAaddr = new IPAddress(ipLLA, IPAddress.TYPE_IPV6);

        ipAddressList.add(ip_4_addr);
        ipAddressList.add(ipLLAaddr);
        System.out.println("=================all ips====================");
        printSortedIPs();
        System.out.println("==================result====================");
        IPAddress ip = primaryIpSelector(ipAddressList);
        if (ip == null) {
            System.out.println("null");
        } else {
            System.out.println("Primary IP: " + ip.getIp() + " " + ip.getType()
                    + " " + ip.isFloating());
        }
    }

    /**
     * @param args
     */
    public static void main(String [] args) {
        try {
            System.out.println("try");
            throw new Exception();
        } catch (Exception e) {
            System.out.println("catch");
        }
        System.out.println("abc");

    }

}

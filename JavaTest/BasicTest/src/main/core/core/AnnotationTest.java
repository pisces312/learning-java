package core;

import java.util.Date;
import java.util.List;
import java.lang.annotation.*;
import java.lang.reflect.Method;

public class AnnotationTest {
    // ////////////////////////////////////////////////////////////////////////////////////
    // annotation
    /**
     * * Describes the Request-For-Enhancement(RFE) that led * to the presence of the annotated API
     * element.
     */
    public @interface RequestForEnhancement {
        int id();

        String synopsis();

        String engineer() default "[unassigned]";

        String date() default "[unimplemented]";
    }

    @RequestForEnhancement(id = 2868724, synopsis = "Enable time-travel", engineer = "Mr. Peabody",
        date = "4/1/3007")
    public static void travelThroughTime(Date destination) {}

    /**
     * no member
     * 
     * Indicates that the specification of the annotated API element is preliminary and subject to
     * change.
     */
    public @interface Preliminary {
    }
    @Preliminary
    public class TimeTravel {
    }

    /** * Associates a copyright notice with the annotated API element. */
    public @interface Copyright {
        String value();
    }
    @Copyright("2002 Yoyodyne Propulsion Systems")
    public class OscillationOverthruster {
    }

    /**
     * * Indicates that the annotated method is a test method. * This annotation should be used only
     * on parameterless static methods.
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface Test {
    }

    // //////////////////////////////////////////////////////////////////////////////////
    /**
     * Deprecated Override SuppressWarnings
     */
    public static class UsingBuiltInAnnotation { // 食物类
        class Food {
        } // 干草类
        class Hay extends Food {
        } // 动物类
        class Animal {
            Food getFood() {
                return null;
            }

            // 使用Annotation声明Deprecated方法
            @Deprecated
            void deprecatedMethod() {}
        } // 马类-继承动物类
        class Horse extends Animal {
            // 使用Annotation声明覆盖方法
            @Override
            Hay getFood() {
                return new Hay();
            }

            // 使用Annotation声明禁止警告
            @SuppressWarnings({"unchecked", "rawtypes"})
            void callDeprecatedMethod(List horseGroup) {
                Animal an = new Animal();
                an.deprecatedMethod();
                horseGroup.add(an);
            }
        }

    }

    public static void main(String[] args) {

        System.out.println(Test.class.getName());
        System.out.println(Test.class.getSuperclass());
//        System.out.println(Test.class.getSuperclass().getSuperclass());

        int passed = 0, failed = 0;
        String c = AnnotationTest.class.getName();
        // String c = "basic.AnnotationTest.Foo";
        try {
            for (Method m : Class.forName(c).getMethods()) {
                if (m.isAnnotationPresent(Test.class)) {
                    try {
                        m.invoke(null);
                        passed++;
                    } catch (Throwable ex) {
                        System.out.printf("Test %s failed: %s %n", m, ex.getCause());
                        failed++;
                    }
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.printf("Passed: %d, Failed %d%n", passed, failed);
    }

}

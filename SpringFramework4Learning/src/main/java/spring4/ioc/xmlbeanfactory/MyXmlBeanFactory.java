package spring4.ioc.xmlbeanfactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

public class MyXmlBeanFactory {
    private static final Logger logger = Logger.getLogger(MyXmlBeanFactory.class);

    public static final class ClientService {
        private ClientService() {}

        //factory-method
        public static ClientService createInstance() {
            logger.info("createInstance");
            return new ClientService();
        }
        
        public void print() {
            System.out.println("this is a client service");
        }
    }


    public static void main(String[] args) {
        String clName = ClientService.class.getName();
        String beanXml = "<bean id=\"clientService\" class=\"" + clName + "\" factory-method=\"createInstance\"/>";
        Resource resource = new ByteArrayResource(
            ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<beans xmlns=\"http://www.springframework.org/schema/beans\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                + "xsi:schemaLocation=\"http://www.springframework.org/schema/beans "
                + "http://www.springframework.org/schema/beans/spring-beans-3.0.xsd\">" + beanXml + "</beans>").getBytes());
        //
        // deprecated in spring 3.1
        // BeanFactory factory = new XmlBeanFactory(resource);
        // ClientService service = factory.getBean(ClientService.class);

        //
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);
        reader.loadBeanDefinitions(resource);
        ClientService service = factory.getBean(ClientService.class);
        service.print();

    }

}

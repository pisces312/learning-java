package chap8.resolution;

/**
 * ������̬������ʾ
 * 
 */
//Compile:
//javac StaticResolution.java
//
//Decompile:
//javap -v StaticResolution
//
//Some bytecode:
//public chap8.resolution.StaticResolution();
//descriptor: ()V
//flags: ACC_PUBLIC
//Code:
//  stack=1, locals=1, args_size=1
//     0: aload_0
//     1: invokespecial #1                  // Method java/lang/Object."<init>":()V
//     4: return
//  LineNumberTable:
//    line 8: 0
//
//public static void sayHello();
//descriptor: ()V
//flags: ACC_PUBLIC, ACC_STATIC
//Code:
//  stack=2, locals=0, args_size=0
//     0: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
//     3: ldc           #3                  // String hello world
//     5: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
//     8: return
//  LineNumberTable:
//    line 11: 0
//    line 12: 8
//
//public static void main(java.lang.String[]);
//descriptor: ([Ljava/lang/String;)V
//flags: ACC_PUBLIC, ACC_STATIC
//Code:
//  stack=0, locals=1, args_size=1
//     0: invokestatic  #5                  // Method sayHello:()V
//     3: return
//  LineNumberTable:
//    line 15: 0
//    line 16: 3
public class StaticResolution {

    public static void sayHello() {
        System.out.println("hello world");
    }

    public static void main(String[] args) {
        StaticResolution.sayHello(); //invokestatic
    }

}


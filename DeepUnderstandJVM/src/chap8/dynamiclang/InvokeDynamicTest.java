package chap8.dynamiclang;

import static java.lang.invoke.MethodHandles.lookup;

import java.lang.invoke.CallSite;
import java.lang.invoke.ConstantCallSite;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;

/**
 * Cannot find Indify.java, use ASM library instead
 * 
 * Take DynamicIndy.java for example
 * 
 * 
INDY tool

https://blogs.oracle.com/jrose/a-modest-tool-for-writing-jsr-292-code
!!Do not change method name
 *
 */
public class InvokeDynamicTest {

    public static void main(String[] args) throws Throwable {
        INDY_BootstrapMethod().invokeExact("hello world");
    }

    public static void testMethod(String s) {
        System.out.println("hello String:" + s);
    }


    public static CallSite BootstrapMethod(Lookup lookup, String name, MethodType mt)
        throws Throwable {
        MethodHandle mh = lookup.findStatic(InvokeDynamicTest.class, name, mt);
        ConstantCallSite callSite = new ConstantCallSite(mh);
        return callSite;
    }

    private static MethodType MT_BootstrapMethod() {
        //args:   Lookup, String, MethodType
        //return: CallSite
        MethodType mt = MethodType.fromMethodDescriptorString(
            "(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;",
            null);
        return mt;
    }

    private static MethodHandle MH_BootstrapMethod() throws Throwable {
        String methodName = "BootstrapMethod";
        MethodType mt = MT_BootstrapMethod();
        return MethodHandles.lookup().findStatic(InvokeDynamicTest.class, methodName, mt);
    }

    private static MethodHandle INDY_BootstrapMethod() throws Throwable {
        String methodName = "testMethod";
        MethodType mt = MethodType.fromMethodDescriptorString("(Ljava/lang/String;)V", null);
        //
        CallSite cs =
            (CallSite) MH_BootstrapMethod().invokeWithArguments(lookup(), methodName, mt, null);
        return cs.dynamicInvoker();
    }

}


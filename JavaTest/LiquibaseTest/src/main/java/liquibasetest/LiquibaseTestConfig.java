package liquibasetest;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
/**
 * Load to environmentProperties (PropertySource)
 *
 * Use @Autowired Environment env to get key/value
 */
@PropertySource("classpath:/liquibasetest/app.properties")
public class LiquibaseTestConfig {

    @Value("${db_username}")
    private String username;

    @Value("${db_password}")
    private String password;

    @Value("${db_url}")
    private String url;

    @Value("${db_driver}")
    private String driver;

    @Autowired
    private ApplicationContext ctx;

    /**
     * http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-auto-configuration.
     * html
     * 
     * if you add your own DataSource bean, the default embedded database support will back away.
     * 
     * @return
     */
    @Bean
    public DataSource createDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }


    @Bean
    public SpringLiquibase liquibaseConfig() {
        DataSource dataSource = ctx.getBean(DataSource.class);
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
//        liquibase.setChangeLog("classpath:/liquibasetest/db.changelog.xml");
        liquibase.setChangeLog("classpath:/liquibasetest/db.changelog.sql");
        liquibase.setContexts("test");
        return liquibase;
    }

    //    @Bean
    //    public PropertySourcesPlaceholderConfigurer appConfig() {
    //        return new PropertySourcesPlaceholderConfigurer();
    //    }

}

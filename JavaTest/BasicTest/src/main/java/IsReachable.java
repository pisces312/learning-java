import java.net.InetAddress;

public class IsReachable {

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            return;
        }
        InetAddress ip = InetAddress.getByName(args[0]);
        System.out.println(ip.isReachable(1000));
    }
}

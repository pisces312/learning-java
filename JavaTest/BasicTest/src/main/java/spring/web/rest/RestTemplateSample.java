package spring.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

@RestController
// contained @ResponseBody
@EnableAutoConfiguration
public class RestTemplateSample implements CommandLineRunner {

    // static SpringApplication app;

    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }

    // http://localhost:8080/get?id=1
    @RequestMapping(value = "/get", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public String get(@RequestParam String id) {
        return "Success";
    }

    // public void exit(){
    // SpringApplication.exit(app, new ExitCodeGenerator() {
    //
    // @Override
    // public int getExitCode() {
    // return 0;
    // }
    // });
    // }
    // ///////////////////////////////

    public static void main(String[] args) throws Exception {
        // app = new SpringApplication(RestTemplateSample.class);
        SpringApplication.run(RestTemplateSample.class, args);
    }

    // /////////////////////////////////////

    private final static String baseURL = "http://localhost:8080";

    @Autowired
    RestTemplate template;

    @Autowired
    AsyncRestTemplate aynscTemplate;

    @Autowired
    RestTemplate template2;

    @Override
    public void run(String... as) throws Exception {
        System.out.println(template.getClass().toString());
        System.out.println(template2.getClass().toString());
        System.out.println(template == template2);
        // RestTemplate template = new RestTemplate();
        String res = template.getForObject(baseURL + "/get?id=test", String.class);
        System.out.println("Sync way result:" + res);

        // Async way
        // async call
        ListenableFuture<ResponseEntity<String>> futureEntity =
            aynscTemplate.getForEntity(baseURL + "/get?id=test", String.class, "test");

        // get the concrete result - synchronous call
        // ResponseEntity<String> entity = futureEntity.get();
        // System.out.println("Async way result:"+entity.getBody());


        // register a callback
        futureEntity.addCallback(new ListenableFutureCallback<ResponseEntity<String>>() {
            @Override
            public void onSuccess(ResponseEntity<String> entity) {
                System.out.println("Async way result:" + entity.getBody());
                // SpringApplication.exit(RestTemplateSample.this,);
            }

            @Override
            public void onFailure(Throwable t) {}
        });



    }

    // //////////////////////////////////////

    @Configuration
    // @ComponentScan//(basePackages={"spring.web.rest"})
    static class TestConfig {
        @Bean
        @Scope(BeanDefinition.SCOPE_PROTOTYPE)
        public AsyncRestTemplate getAsyncRestTemplate() {
            return new AsyncRestTemplate();
        }

        @Bean
        @Scope(BeanDefinition.SCOPE_PROTOTYPE)
        public RestTemplate getRestTemplate() {
            // System.out.println("getRestTemplate");
            // return new TestRestTemplate();
            return new RestTemplate();
        }

        @Bean
        @Primary
        @Scope(BeanDefinition.SCOPE_PROTOTYPE)
        public RestTemplate getTestRestTemplate() {
            // System.out.println("getRestTemplate");
            // return new TestRestTemplate();
            return new TestRestTemplate();
        }
    }
}

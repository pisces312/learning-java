package jcip.chap05.blocklevel;

import java.util.*;
import java.util.concurrent.*;

import net.jcip.examples.Computable;

/**
 * Memoizer2
 * <p/>
 * Replacing HashMap with ConcurrentHashMap
 *
 * @author Brian Goetz and Tim Peierls
 */
public class Memoizer2<A, V> implements Computable<A, V> {
    //Thread safe
    private final Map<A, V> cache = new ConcurrentHashMap<A, V>();

    private final Computable<A, V> c;

    public Memoizer2(Computable<A, V> c) {
        this.c = c;
    }

    public V compute(A arg) throws InterruptedException {
        V result = cache.get(arg);
        if (result == null) {
            //May have duplicate calculation
            //!Multiple threads can touch it at the same time
            result = c.compute(arg);
            cache.put(arg, result);
        }
        return result;
    }
}

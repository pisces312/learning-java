package jcip.chap07.taskcancel.primesamples;

import java.math.BigInteger;
import java.util.concurrent.*;

/**
 * PrimeProducer
 * <p/>
 * Using interruption for cancellation
 *
 * @author Brian Goetz and Tim Peierls
 */
public class PrimeProducer extends Thread {
    private final BlockingQueue<BigInteger> queue;

    PrimeProducer(BlockingQueue<BigInteger> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            BigInteger p = BigInteger.ONE;
            //Check interrupt status
            while (!Thread.currentThread().isInterrupted()) {
                queue.put(p = p.nextProbablePrime());
            }
            System.out.println("Will not reach here");
        } catch (InterruptedException consumed) {
            /* Allow thread to exit (correct way)*/
            System.out.println("put is interrupted");
        }
    }

    public void cancel() {
        interrupt();
    }
    
    public static void main(String[] args) {
        PrimeProducer primeProducer=new PrimeProducer(new LinkedBlockingQueue<>());
        primeProducer.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        primeProducer.cancel();
    }
}

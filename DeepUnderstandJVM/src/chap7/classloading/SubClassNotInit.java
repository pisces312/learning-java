package chap7.classloading;

/**
 * 被动使用类字段演示一：
* 通过子类引用父类的静态字段，不会导致子类初始化
 **/
public class SubClassNotInit {

    private static class SuperClass {

        static {
            System.out.println("SuperClass init!");
        }

        public static int value = 123;
    }

    private static class SubClass extends SuperClass {

        static {
            //Not called!!
            System.out.println("SubClass init!");
            assert (false);
        }
    }

    public static void main(String[] args) {
        //SubClass is not load actually
        System.out.println(SubClass.value);
    }
}

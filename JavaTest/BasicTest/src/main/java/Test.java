public class Test {
    public int a = 0;

    public Test() {
        a = 3;
    }

    public void m() {
        System.out.println("Base");
    }

    public final int test() {
        int b = 2;
        a = 2;
        System.out.println("abc");
        // boolean flag=null;
        // Boolean flag=null;

        return b;

    }

    public static void main(String[] arg) {

        Test2 t = new SubTest2();
        System.out.println(t.a);
        t.m();

    }
}


abstract class Test2 {
    public int a = 0;

    public Test2() {
        a = 3;
    }

    public void m() {
        System.out.println("Base");
    }

}


class SubTest2 extends Test2 {
    public int a = 2;

    public SubTest2() {
        a = 5;
    }

    @Override
    public void m() {
        System.out.println("subbase");
    }
}


class SubTest extends Test {
    public int a = 2;

    public SubTest() {
        a = 5;
    }

    @Override
    public void m() {
        System.out.println("subbase");
    }


    // public final int test(){
    // return 0;
    // }

}

package chap3.allocpolicy;

public class MinorGC {
    private static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        testMinorGCWithSerial();
    }

    //Must use Serial GC
    //-verbose:gc -Xms20M -Xmx20M -Xmn10M  -XX:+UseSerialGC -XX:+PrintGCDetails -XX:SurvivorRatio=8
    @SuppressWarnings("unused")
    public static void testMinorGCWithSerial() {
        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[2 * _1MB];
        allocation2 = new byte[2 * _1MB];
        allocation3 = new byte[2 * _1MB];
        allocation4 = new byte[4 * _1MB]; // Minor GC happened
    }

    //Default Parallel Scavenger GC
//-verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
    @SuppressWarnings("unused")
    public static void testMinorGCWithPS() {
//      [GC (Allocation Failure) [PSYoungGen: 8151K->648K(9216K)] 8151K->656K(19456K), 0.0011504 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
//      Heap
//       PSYoungGen      total 9216K, used 3962K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
//        eden space 8192K, 40% used [0x00000000ff600000,0x00000000ff93cb78,0x00000000ffe00000)
//        from space 1024K, 63% used [0x00000000ffe00000,0x00000000ffea2020,0x00000000fff00000)
//        to   space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
//       ParOldGen       total 10240K, used 8K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
//        object space 10240K, 0% used [0x00000000fec00000,0x00000000fec02000,0x00000000ff600000)
//       Metaspace       used 2649K, capacity 4486K, committed 4864K, reserved 1056768K
//        class space    used 285K, capacity 386K, committed 512K, reserved 1048576K

        final int xmn = 10;
        final int eden = 8;
        final int SURVIVOR_SPACE = (xmn - eden) / 2;

        //make each object smaller than "from/to"
        for (int i = 0; i < (eden + SURVIVOR_SPACE); ++i) {
            byte[] allocation1 = new byte[SURVIVOR_SPACE * _1MB];
        }
        byte[] allocation1 = new byte[SURVIVOR_SPACE * _1MB];
    }



}

package beancopy;

public interface IMethodCallBack {

    String getMethodName();

    ToBean callMethod(FromBean frombean) throws Exception;

}

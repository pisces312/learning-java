package core.proxy.dynamic.cglib;

import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author baliuka
 */
public abstract class Bean implements Serializable {

    private static final long serialVersionUID = -7249253368864929568L;
    String sampleProperty;

    abstract public void addPropertyChangeListener(PropertyChangeListener listener);

    abstract public void removePropertyChangeListener(PropertyChangeListener listener);

    public String getSampleProperty() {
        return sampleProperty;
    }

    public void setSampleProperty(String value) {
        this.sampleProperty = value;
    }

    public String toString() {
        return "sampleProperty is " + sampleProperty;
    }

}

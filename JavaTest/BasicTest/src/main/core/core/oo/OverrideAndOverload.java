package core.oo;

import org.junit.Test;

/**
 * ?Only use override or overload, one type
 *
 */
public class OverrideAndOverload {
    abstract class Human {
        protected String name;

        public Human(String name) {
            this.name = name;
        }

        public void print(Human human) {
            System.out.println("Invoke Human's printHuman");
            System.out.println(human.name);
        }

        public void print(Man man) {
            System.out.println("Invoke Human's printMan");
            System.out.println(man.name);
        }
    }
    class Man extends Human {
        public Man(String name) {
            super(name);
        }

        @Override
        public void print(Human human) {
            System.out.println("Invoke Man's printHuman");
            System.out.println(human.name);
        }

        @Override
        public void print(Man man) {
            System.out.println("Invoke Man's printMan");
            System.out.println(man.name);
        }
    }

    @Test
    public void test() {
        Man man = new Man("man");
        Human human = man;
        //Human is invoker, Human type is passed
        //printHuman is override by Man
        //printHuman is overload 
        //Call Man's method
        //!
        human.print(human);
        human.print(man);
        man.print(human);
        man.print(man);
    }
}

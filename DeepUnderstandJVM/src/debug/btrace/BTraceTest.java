package debug.btrace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BTraceTest {

    //method to trace
    public int add(int a, int b) {
        return a + b;
    }

    public static void main(String[] args) throws IOException {
        System.out.println("input any string and enter to continue");
        //[Test] Wait user input to continue
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        reader.readLine();
        //
        //[Logic]
        BTraceTest test = new BTraceTest();
        for (int i = 0; i < 10; i++) {
            int a = (int) Math.round(Math.random() * 1000);
            int b = (int) Math.round(Math.random() * 1000);
            System.out.println(test.add(a, b));
        }
    }
}


package ssl;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * 
 * 
 * Load "cacerts" from $JAVA_HOME/jre/lib/security folder and check between the folder we put the
 * 3rd certs to see if these ".cer" is already in it, if not go step 4. Loop and append all the
 * ".cer" extension files in the folder into a "cacerts" file.
 * 
 * Append by using command like :
 * 
 * $JAVA_HOME/bin/keytool -import -noprompt -trustcacerts -alias bt -file BT.cer -keystore cacerts
 * -storepass changeit
 * 
 * $JAVA_HOME/bin/keytool -import -noprompt -trustcacerts -alias dsd -file DSD.cer -keystore cacerts
 * -storepass changeit
 * 
 * $JAVA_HOME/bin/keytool -import -noprompt -trustcacerts -alias bedrock -file BEDROCK.cer -keystore
 * cacerts -storepass changeit
 * 
 * Java library to execute SSH http://www.jcraft.com/jsch/ (Refer ScriptExecutionTasklet.process() )
 * Copy this "cacerts" file to $JAVA_HOME/jre/lib/security folder
 * 
 * @author nil4
 *
 */
public class EnableSSL {
    private List<String> getAllCerts(String folder) {
        List<String> fileList = new ArrayList<String>();
        File[] files = new File(folder).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase()
                    .endsWith(".cer");
            }
        });
        for (File file : files) {
            fileList.add(file.getName());
        }
        return fileList;
    }

    private void addSSL() {
        String javaHome = System.getenv("JAVA_HOME");
        String sslFolder = javaHome + "\\jre\\lib\\security";
        System.out.println(sslFolder);

        //This is the folder path

        File sourceFile = new File("C:\\cacerts");
        File destDir = new File(sslFolder);
        try {
            FileUtils.copyFileToDirectory(sourceFile, destDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

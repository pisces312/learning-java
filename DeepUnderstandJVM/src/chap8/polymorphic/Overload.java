package chap8.polymorphic;

import java.io.Serializable;

public class Overload {

    //1
    public static void sayHello(char arg) {
        System.out.println("hello char");
    }

    //2
    public static void sayHello(int arg) {
        System.out.println("hello int");
    }

    //3
    public static void sayHello(long arg) {
        System.out.println("hello long");
    }

    //4
    public static void sayHello(Character arg) {
        System.out.println("hello Character");
    }

    //5
    // Character implements java.io.Serializable
    public static void sayHello(Serializable arg) {
        System.out.println("hello Serializable");
    }

    //6
    public static void sayHello(Object arg) {
        System.out.println("hello Object");
    }

//7
//var arg is matched at last
    public static void sayHello(char... arg) {
        System.out.println("hello char ...");
    }



    public static void main(String[] args) {
        sayHello('a');
    }
}


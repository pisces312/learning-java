package akka.sample.hello;

import akka.actor.PoisonPill;
import akka.actor.UntypedActor;

public class GreeterActor extends UntypedActor {

    public static enum Msg {
        GREET, DONE;
    }

    @Override
    public void onReceive(Object msg) {
        if (msg instanceof Msg) {
            switch ((Msg) msg) {
                case GREET:
                    System.out.println("Hello World!");
                    //Reply to sender
                    getSender().tell(Msg.DONE, getSelf());
                    break;
                // case DONE:
                default:
                    unhandled(msg);
                    break;
            }
        }else if(msg instanceof PoisonPill){
            System.out.println(getSelf()+" is stopped");
//            getContext().stop(getSelf());
            
        }else if(msg instanceof String){
            System.out.println(msg);
        }

    }

}

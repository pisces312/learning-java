

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetPropertyTest {
    public static InputStream guessPropFile(Class<?> cls, String propFile) {
        try {

            ClassLoader loader = cls.getClassLoader();


            java.io.InputStream in = loader.getResourceAsStream(propFile);
            if (in != null)
                return in;


            Package pack = cls.getPackage();
            if (pack != null) {
                String packName = pack.getName();
                String path = "";
                if (packName.indexOf(".") < 0)
                    path = packName + "/";
                else {
                    int start = 0, end = 0;
                    end = packName.indexOf(".");
                    while (end != -1) {
                        path = path + packName.substring(start, end) + "/";
                        start = end + 1;
                        end = packName.indexOf(".", start);
                    }
                    path = path + packName.substring(start) + "/";
                }
                in = loader.getResourceAsStream(path + propFile);
                if (in != null)
                    return in;
            }


            File f = null;
            String curDir = System.getProperty("user.dir");
            f = new File(curDir, propFile);
            if (f.exists())
                return new FileInputStream(f);


            String classpath = System.getProperty("java.class.path");
            String[] cps = classpath.split(System.getProperty("path.separator"));

            for (int i = 0; i < cps.length; i++) {
                f = new File(cps[i], propFile);
                if (f.exists())
                    break;
                f = null;
            }
            if (f != null)
                return new FileInputStream(f);
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        InputStream inputStream = guessPropFile(GetPropertyTest.class, "db.conf");// GetPropertyTest.class.getClassLoader().getResourceAsStream("db.conf");
        if (inputStream == null) {
            System.out.println("inputstream");
        }
        Properties p = new Properties();
        try {
            p.load(inputStream);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        System.out.println("ip:" + p.getProperty("ip") + ",port:" + p.getProperty("port"));

    }

}

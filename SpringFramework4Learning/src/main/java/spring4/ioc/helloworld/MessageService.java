package spring4.ioc.helloworld;

public interface MessageService {
    String getMessage();
}
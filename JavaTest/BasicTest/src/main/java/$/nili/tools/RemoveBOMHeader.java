package $.nili.tools;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * 
 * Remove BOM header(\uFEFF) from Java source file
 * 
 * @author nil4
 *
 */
public class RemoveBOMHeader {

    public static void removeBOMHeader(String rootDir) throws Exception {
        Queue<File> queue = new LinkedBlockingDeque<>();

        File rootDirFile = new File(rootDir);
        if (!rootDirFile.isDirectory()) {
            return;
        }
        // FileFilter fileFilter=new FileFilter() {
        // @Override
        // public boolean accept(File pathname) {
        // return (pathname.isFile()&&pathname.getName().endsWith(".java") );
        // }
        // };

        // File[] files = rootDirFile.listFiles();


        queue.add(rootDirFile);
        while (!queue.isEmpty()) {
            File file = queue.poll();
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                queue.addAll(Arrays.asList(files));
            } else {
                if (file.getName().endsWith(".java")) {
                    System.out.println("matched " + file.getName());

                    String outputFileName = file.getAbsolutePath() + ".tmp";
                    File outputFile = new File(outputFileName);
                    if (outputFile.exists()) {
                        outputFile.delete();
                        outputFile.createNewFile();
                    }

                    try (BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));) {
                        try (BufferedReader br = new BufferedReader(new FileReader(file));) {
                            String line = br.readLine();
                            if (line.startsWith("\uFEFF")) {
                                line = line.substring(1);
                                line = line.replace("\uFEFF", "");
                            }
                            if (line != null) {
                                bw.write(new String(line.getBytes(), Charset.forName("UTF-8")));
                                bw.append("\n");
                                while ((line = br.readLine()) != null) {
                                    bw.write(new String(line.getBytes(), Charset.forName("UTF-8")));
                                    bw.append("\n");
                                }
                                bw.flush();
                            }
                        }
                    }

                    file.delete();

                    outputFile.renameTo(file);


                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        String rootDir = "C:\\nili\\workspace\\JavaTest\\src";

        removeBOMHeader(rootDir);
    }
}

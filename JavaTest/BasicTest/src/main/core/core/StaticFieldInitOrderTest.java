package core;

/**
 * 
 * According to assignment order
 * 
 * @author nili6
 *
 */
public class StaticFieldInitOrderTest {
    static int a = 2;

    static {
        a = 3;
    }
    static {
        a = 4;
    }

    public static void main(String[] args) {
        assert (4 == a);
    }
}

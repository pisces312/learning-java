package core.methodhandle.invokedynamic2;


import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ACC_SUPER;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.H_INVOKESTATIC;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.IRETURN;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.V1_8;

import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.Arrays;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import core.methodhandle.WriteClassUtils;

public class DynamicIndy extends ClassLoader {
    private static volatile int ID_GENERATOR = 0;
    
    //can be any name
    private static final String GENERATED_METHOD_NAME="func";

    /**
     * 
     * @param name
     *  method name
     * @param desc
     * @param bsmClass
     * @param bsmName
     * @param bsmType
     * @param bsmArgs
     * @return
     */
    public MethodHandle invokeDynamic(String name, MethodType desc, Class<?> bsmClass,
        String bsmName, MethodType bsmType, Object... bsmArgs) {
        String targetClassName="Gen" + (ID_GENERATOR++);
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        cw.visit(V1_8, ACC_PUBLIC | ACC_SUPER, targetClassName, null, "java/lang/Object",
            null);

        //init method
        MethodVisitor init = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
        init.visitCode();
        init.visitVarInsn(ALOAD, 0);
        init.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
        init.visitInsn(RETURN);
        init.visitMaxs(-1, -1);
        init.visitEnd();

        //func method
        String descriptor = desc.toMethodDescriptorString();
        System.out.println("Descriptor:"+descriptor);
        MethodVisitor mv =
            cw.visitMethod(ACC_PUBLIC | ACC_STATIC, GENERATED_METHOD_NAME, descriptor, null, null);
        int slot = 0;
        for (Type parameterType : Type.getArgumentTypes(descriptor)) {
            mv.visitVarInsn(parameterType.getOpcode(ILOAD), slot);
            slot += parameterType.getSize();
        }

        System.out.println("Args:"+Arrays.toString(bsmArgs));
        mv.visitInvokeDynamicInsn(name, descriptor, new Handle(H_INVOKESTATIC,
            bsmClass.getName().replace('.', '/'), bsmName, bsmType.toMethodDescriptorString(),bsmClass.isInterface()),
            bsmArgs);
        Type returnType = Type.getReturnType(descriptor);
        mv.visitInsn(returnType.getOpcode(IRETURN));
        mv.visitMaxs(-1, -1);
        mv.visitEnd();

        cw.visitEnd();


        
        byte[] array = cw.toByteArray();
        //
        try {
            WriteClassUtils.writeToFile(targetClassName, array);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        //
        Class<?> clazz = defineClass(null, array, 0, array.length);
        try {
            return MethodHandles.lookup().findStatic(clazz, GENERATED_METHOD_NAME, desc);
        } catch (IllegalAccessException | NoSuchMethodException e) {
            throw (AssertionError) new AssertionError().initCause(e);
        }
    }
}

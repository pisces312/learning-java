package spring.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Test for @Scope, @Primary
 * 
 * @author nil4
 *
 */
@RestController
@EnableAutoConfiguration
public class BeanScopeTest implements CommandLineRunner {

    static class MyBean {

    }

    static class MyBean2 extends MyBean {

    }

    @Configuration
    static class TestConfig {
        @Bean
        @Scope("prototype")
        public MyBean createBean() {
            return new MyBean();
        }

        @Bean
        @Primary
        @Scope("prototype")
        public MyBean createBean2() {
            return new MyBean2();
        }

    }


    @Autowired
    MyBean bean;

    @Autowired
    MyBean bean2;


    @Override
    public void run(String... args) throws Exception {
        System.out.println(bean.getClass().toString());
        System.out.println(bean2.getClass().toString());
        System.out.println(bean == bean2);

        System.out.println(new RestTemplate() == new RestTemplate());

    }

    public static void main(String[] args) {
        SpringApplication.run(BeanScopeTest.class, args);

    }



}

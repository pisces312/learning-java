package core.thread;

import org.junit.Test;

public class InterruptTest {
    @Test
    public void testInterruptBeforeSleep() {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; ++i) {
                System.out.print(i + " ");
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("interrupted in common function, no exception");
                    break;
                }
            }

            System.out.println("before sleep");
            //No need interrupt() called during sleep()
            //sleep() just check interrupted flag
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println("interrupted in sleep by exception");
            }
            System.out.println("after sleep");
        });
        t1.start();


        t1.interrupt();

        //Wait until finish
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

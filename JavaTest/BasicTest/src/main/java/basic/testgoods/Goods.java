package basic.testgoods;

/**
 * 在这个例子里类Content和GDestination被定义在了类Goods内部，并且分别有着protected和private修饰符来控制访问级别。Content代表着Goods的内容，
 * 而GDestination代表着Goods的目的地。它们分别实现了两个接口Content和Destination。在后面的main方法里，直接用 Contents c和Destination
 * d进行操作，你甚至连这两个内部类的名字都没有看见！这样，内部类的第一个好处就体现出来了——隐藏你不想让别人知道的操作，也即封装性。
 * 同时，我们也发现了在外部类作用范围之外得到内部类对象的第一个方法
 * ，那就是利用其外部类的方法创建并返回。上例中的cont()和dest()方法就是这么做的。那么还有没有别的方法呢？当然有，其语法格式如下： outerObject=new
 * outerClass(Constructor Parameters); outerClass.innerClass innerObject=outerObject.new
 * InnerClass(Constructor Parameters);
 * 
 * @author pisces
 */
public class Goods {

     private int i;
    private int valueRate = 2;

    // 声明为了私有类
    private class Content implements ContentsInterface {
        // Java编译器在创建内部类对象时，隐式的把其外部类对象的引用也传了进去并一直保存着
        public Content() {}

        // 一个内部类对象可以访问创建它的外部类对象的内容，甚至包括私有变量!
        // 这里的i和Goods中的i名字相同
        private int i = 11 * valueRate;

        public int value() {
            /**
             * 有人会问，如果内部类里的一个成员变量与外部类的一个成员变量同名， 也即外部类的同名成员变量被屏蔽了，怎么办？ 没事，Java里用如下格式表达外部类的引用：
             * outerClass.this 　　有了它，我们就不怕这种屏蔽的情况了。
             */
            return i;
        }
    }
    // 声明为了保护类
    protected class GDestination implements DestinationInterface {

        private String label;

        private GDestination(String whereTo) {
            label = whereTo;
        }

        public String readLabel() {
            return label;
        }
    }

    /**
     * 和普通的类一样，内部类也可以有静态的。不过和非静态内部类相比，区别就在于静态内部类没有了指向外部的引用
     * 这实际上和C++中的嵌套类很相像了，Java内部类与C++嵌套类最大的不同就在于是否有指向外部的引用这一点上，当然从设计的角度以及以它一些细节来讲还有区别。
     * 除此之外，在任何非静态内部类中，都不能有静态数据，静态方法或者又一个静态内部类（内部类的嵌套可以不止一层）。不过静态内部类中却可以拥有这一切。这也算是两者的第二个区别吧。
     */
    public static class Content2 implements ContentsInterface {

        public int value() {
            return 10;
            // throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    // 我们也发现了在外部类作用范围之外得到内部类对象的第一个方法，那就是利用其外部类的方法创建并返回
    public DestinationInterface dest(String s) {
        return new GDestination(s);
    }

    public ContentsInterface cont() {
        return new Content();
    }

    public static void main(String[] args) {
        Goods p = new Goods();
        // 内部类的第一个好处就体现出来了——隐藏你不想让别人知道的操作，也即封装性
        // 看不到Content和GDestination两个类
        ContentsInterface c = p.cont();
        DestinationInterface d = p.dest("Beijing");

        System.out.println(c.value());
        System.out.println(d.readLabel());
    }
}

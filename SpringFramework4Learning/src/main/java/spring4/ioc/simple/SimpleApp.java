package spring4.ioc.simple;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;


@Configuration
//@ComponentScan
public class SimpleApp {

    public static void main(String[] args) {
        try (ConfigurableApplicationContext context =
            new AnnotationConfigApplicationContext(SimpleApp.class);) {
            SimpleApp app = context.getBean(SimpleApp.class);
            System.out.println(app);
        }
    }
}

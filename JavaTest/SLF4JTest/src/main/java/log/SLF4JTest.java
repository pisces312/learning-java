package log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * To switch logging frameworks, just replace slf4j bindings on your class path
 * @author nil4
 *
 */
public class SLF4JTest {

    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(SLF4JTest.class);
        
        logger.info("class:{}",logger.getClass());
        
        logger.info("Hello World");
        
        logger.info("old:{}, new:{}",1,2);

    }

}

package oom;
/**
 * VM Args��-Xss108k -Xss at least 108K
 * 
 * Create too many threads, OOM
 * 
 * Actually will not throw!!
 * 
 */
public class JavaVMStackOOM {

    static int threadNum = 0;

    private void dontStop() {
        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //        while (true) {
        //            
        //        }
    }

    public void stackLeakByThread() {
        while (true) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    dontStop();
                }
            });
            thread.start();
            ++threadNum;
            System.out.println("Thread num: " + threadNum);
        }
    }

    public static void main(String[] args) throws Throwable {
        JavaVMStackOOM oom = new JavaVMStackOOM();
        oom.stackLeakByThread();
    }
}


package oom;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * VM Args�� -XX:PermSize=10M -XX:MaxPermSize=10M
 * 
 */
public class JavaMethodAreaOOM {

    public static void main(String[] args) {
        String ver = System.getProperty("java.version");
        System.out.println(ver);
        if (ver.startsWith("1.8")) {
            System.out.println("No perm space in Java " + ver);
            return;
        }
        while (true) {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(OOMObject.class);
            enhancer.setUseCache(false);
            enhancer.setCallback(new MethodInterceptor() {
                public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                    return proxy.invokeSuper(obj, args);
                }
            });
            enhancer.create();
        }
    }

    static class OOMObject {

    }
}


package core.methodhandle;

import java.lang.invoke.CallSite;
import java.lang.invoke.ConstantCallSite;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.MutableCallSite;
import java.lang.invoke.VolatileCallSite;

import org.junit.Test;

public class MethodHandleTest {


    /**
     * 'ConstantCallSite' means MethodHandle must be initialized 
     */
    @Test
    public void useConstantCallSite() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();

        //Create MethodHandle
        String mn = "substring";
        MethodType type = MethodType.methodType(String.class, int.class, int.class);
        //find 'substring' method in 'String' class
        MethodHandle mh = lookup.findVirtual(String.class, mn, type);

        //Define ConstantCallSite
        ConstantCallSite callSite = new ConstantCallSite(mh);
        MethodHandle invoker = callSite.dynamicInvoker();
        //
        String strPar = "Hello World";
        int startIdx = 6, endIdx = 11;

        String result = (String) invoker.invoke(strPar, startIdx, endIdx);
        System.out.println(result);

        assert (strPar.substring(startIdx, endIdx).equals(result));
    }

    /**
     * 'MutableCallSite' is able to select MethodHandle dynamically
     * by 'setTarget'
     */
    @Test
    public void useMutableCallSite() throws Throwable {
        MethodType type = MethodType.methodType(int.class, int.class, int.class);
        MutableCallSite callSite = new MutableCallSite(type);
        //for multi-threads
//        callSite.syncAll(sites);
        useCallSiteDynamically(callSite);
    }


    /**
     * VolatileCallSite is for multi-threads.
     * more convenient to use
     */
    @Test
    public void useVolatileCallSite() throws Throwable {
        MethodType type = MethodType.methodType(int.class, int.class, int.class);
        VolatileCallSite callSite = new VolatileCallSite(type);
        useCallSiteDynamically(callSite);
    }


    private void useCallSiteDynamically(CallSite callSite) throws Throwable {
        MethodHandle invoker = callSite.dynamicInvoker();
        MethodType type = invoker.type();

        MethodHandles.Lookup lookup = MethodHandles.lookup();

        //use 'max' method
        MethodHandle mhMax = lookup.findStatic(Math.class, "max", type);
        callSite.setTarget(mhMax);
        int result = (int) invoker.invoke(3, 5); //值为5
        System.out.println(result);
        assert (5 == result);

        //use 'min' method
        MethodHandle mhMin = lookup.findStatic(Math.class, "min", type);
        callSite.setTarget(mhMin);
        result = (int) invoker.invoke(3, 5); //值为3
        System.out.println(result);
        assert (3 == result);
    }



}

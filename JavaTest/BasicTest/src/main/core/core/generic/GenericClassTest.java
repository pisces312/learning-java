package core.generic;

public class GenericClassTest<T> {

    T t;

    public GenericClassTest(T t) {
        this.t = t;
    }

    public T getT() {
        return t;
    }


    public static void main(String[] args) {
        GenericClassTest<String> main = new GenericClassTest<String>("abc");
        System.out.println(main.getT());
    }
}

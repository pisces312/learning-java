package akka.sample.hello;

import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.ActorRef;

public class HelloWorldActor extends UntypedActor {

    public HelloWorldActor() {
        System.out.println("ctor");
    }

    @Override
    public void preStart() {
        System.out.println("preStart");
        // Sub level actor
        // create the greeter actor
        final ActorRef greeter = getContext().actorOf(Props.create(GreeterActor.class), "GreeterActor");
        System.out.println("sub level actor path: "+greeter.path());
        
        System.out.println(getSelf() + " sent greet");
        // tell it to perform the greeting
        greeter.tell(GreeterActor.Msg.GREET, getSelf());


    }

    @Override
    public void postStop() throws Exception {
        System.out.println("postStop");
    }

    @Override
    public void onReceive(Object msg) {
        if (msg == GreeterActor.Msg.DONE) {
            System.out.println(getSelf() + " received DONE.");
            // when the greeter is done, stop this actor and with it the application
            getContext().stop(getSelf());
        } else
            unhandled(msg);
    }
}

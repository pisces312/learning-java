package core.debug.hsdb;

public class Test {

    //t1: method area
    static Test2 t1 = new Test2();

    //t2: Java heap
    Test2 t2 = new Test2();

    public void fn() {
        //t3: stack
        Test2 t3 = new Test2();
    }
}


class Test2 {
}

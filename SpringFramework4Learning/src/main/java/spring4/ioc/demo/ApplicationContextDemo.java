package spring4.ioc.demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import spring4.ioc.demo.common.IBankSecurityService;
import spring4.utils.Utils;

public class ApplicationContextDemo {

    public static void main(String[] args) {
        String xmlPath = Utils.getXmlPath(BeanFactoryDemo.class, "ioc_applicationcontext.xml");
        try (ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlPath);) {
            IBankSecurityService bankSecurityService = applicationContext.getBean(IBankSecurityService.class);
            bankSecurityService.bankToSecurity(2000.00);
            bankSecurityService.securityToBank(2000.00);
        }
    }

}


package spring4.aop.aspectj.sample;
/**
 * For enhancing Hello interface
 *
 */
public class IntroductionOk implements Ok {

    public void sayOk() {
        System.out.println("Ok!");
    }
}

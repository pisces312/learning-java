package jzlibtest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.jcraft.jzlib.JZlib;
import com.jcraft.jzlib.ZStream;

/**
 *
 * @author pisces312
 */
public class JzlibTest {

    public static byte[] deflate(byte[] content) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] outputBuffer = new byte[1024];
        Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION, false);

        // 这里将这次待压缩的内容放入输入缓存
        deflater.setInput(content);
        deflater.finish();

        while (!deflater.finished()) {
            int r = deflater.deflate(outputBuffer);
            baos.write(outputBuffer, 0, r);
        }

        System.out.println("输入压缩内容大小:" + deflater.getTotalIn());
        System.out.println("输出压缩内容大小2:" + deflater.getTotalOut());

        deflater.end();
        return baos.toByteArray();

    }

    public static String getInflateStringUsingJzlib(byte[] b, int indeflateBufferSize) {

        int comprLen = b.length;
        int uncomprLen = indeflateBufferSize;
        // byte[] compr = new byte[comprLen];
        byte[] uncompr = new byte[uncomprLen];
        ZStream d_stream = new ZStream();
        d_stream.next_in = b;
        d_stream.next_in_index = 0;
        d_stream.avail_in = comprLen;
        int err = d_stream.inflateInit();

        while (true) {
            d_stream.next_out = uncompr;
            d_stream.next_out_index = 0;
            d_stream.avail_out = uncomprLen;
            err = d_stream.inflate(JZlib.Z_NO_FLUSH);
            if (err == JZlib.Z_STREAM_END) {
                break;
            }
        }

        err = d_stream.inflateEnd();

        // if (d_stream.total_out != 2 * uncomprLen + comprLen / 2) {
        // System.out.println("bad large inflate: " + d_stream.total_out);
        // System.exit(1);
        // } else {
        // System.out.println("large_inflate(): OK");
        // }
        return new String(uncompr);
    }

    public static String getInflateStringUsingJzlib2(byte[] b, int inflateBufferSize) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int comprLen = b.length;
        // int uncomprLen = indeflateBufferSize;
        // byte[] compr = new byte[comprLen];
        byte[] uncompr = new byte[inflateBufferSize];
        ZStream d_stream = new ZStream();
        d_stream.next_in = b;
        d_stream.next_in_index = 0;
        d_stream.avail_in = comprLen;
        int err = d_stream.inflateInit();
        int offset = 0;
        int out = 0;
        while (true) {
            d_stream.next_out = uncompr;
            d_stream.next_out_index = 0;
            d_stream.avail_out = inflateBufferSize;


            err = d_stream.inflate(JZlib.Z_NO_FLUSH);
            out = (int) (d_stream.total_out - offset);
            offset = (int) d_stream.total_out;
            baos.write(uncompr, 0, out);
            if (err == JZlib.Z_STREAM_END) {
                break;
            }

        }

        err = d_stream.inflateEnd();
        return baos.toString();
    }

    public static String getInflateString(byte[] b, int indeflateBufferSize) {

        byte[] buffer = new byte[indeflateBufferSize];

        ByteArrayOutputStream baos = new ByteArrayOutputStream(b.length * 2);
        // DataOutputStream dos=new DataOutputStream(baos);
        Inflater inflater = new Inflater();
        inflater.setInput(b);
        int last = 0, out = 0;
        int c = 1;
        while (!inflater.finished()) {
            try {
                System.out.println("uncompressed..." + (c++));
                last = out;
                // last=inflater.getTotalOut();
                // 返回从压缩内容读取了多少
                inflater.inflate(buffer);
                // 返回当前一共解压出多少字节
                out = inflater.getTotalOut();
                // 这次解压了多少
                baos.write(buffer, 0, out - last);
            } catch (DataFormatException ex) {
                Logger.getLogger(JzlibTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("finished");
        // int totalOut = inflater.getTotalOut();
        inflater.end();
        return baos.toString();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            byte[] deflateBytes = deflate("hello world!".getBytes());
            System.out.println(getInflateStringUsingJzlib2(deflateBytes, 1024));
            File file = new File("f:\\压缩内容.txt");
            if (!file.exists()) {


                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            fos.write(deflateBytes);
            fos.close();

            // System.out.println(getInflateStringUsingJzlib(deflateBytes, 1024));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JzlibTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JzlibTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(JzlibTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // System.out.println(getInflateStringUsingJzlib(deflateBytes, 1024));


    }
}

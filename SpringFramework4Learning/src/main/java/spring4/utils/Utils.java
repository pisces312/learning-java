package spring4.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Utils {
    public static void setSpringLogLevel(Level level) {
        Logger root = Logger.getRootLogger();
        root.setLevel(level);
    }

    public static String getXmlPath(Class<?> clazz, String xmlFileName) {
        String pkg = clazz.getPackage().getName();
        return pkg.replace('.', '/') + "/" + xmlFileName;
    }
}

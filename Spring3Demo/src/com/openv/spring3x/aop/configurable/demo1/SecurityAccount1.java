package com.openv.spring3x.aop.configurable.demo1;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Configurable;

import com.openv.spring3x.aop.configurable.IBankSecurityService;

/**
 * 证券帐号，领域对象
 */
// 用于config.xml配置
@Configurable
public class SecurityAccount1 {
	private static final Log log = LogFactory.getLog(SecurityAccount1.class);
	private IBankSecurityService bankSecurityService;

	public SecurityAccount1() {
		log.info("ctor SecurityAccount bankSecurityService="
				+ bankSecurityService);
		log.info("preConstruction is " + (bankSecurityService != null));
		// Assert.notNull(this.bankSecurityService);
	}

	public void setBankSecurityService(IBankSecurityService bankSecurityService) {
		this.bankSecurityService = bankSecurityService;
	}

	public void toBank(Double money) {
		this.bankSecurityService.securityToBank(money);
	}

	public void toSecurity(Double money) {
		this.bankSecurityService.bankToSecurity(money);
	}

	private String accountId;

	private String accountPwd;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountPwd() {
		return accountPwd;
	}

	public void setAccountPwd(String accountPwd) {
		this.accountPwd = accountPwd;
	}

}

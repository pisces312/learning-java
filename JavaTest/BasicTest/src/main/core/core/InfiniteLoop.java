package core;


public class InfiniteLoop {

    static int count = 0;

    public InfiniteLoop() {
        System.out.println(count++);
        if (count < 100) {
            new InfiniteLoop();
        }
    }


    public static void main(String[] args) {
        new InfiniteLoop();
    }
}

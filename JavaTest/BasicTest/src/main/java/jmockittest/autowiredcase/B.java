package jmockittest.autowiredcase;

import org.springframework.beans.factory.annotation.Autowired;

class B extends X {
    @Autowired
    C c;

    @Override
    public void method() {
        //do something
        c.callMethodInC();
        //do something
    }

    public void callMethodInB() {
        System.out.println("callMethodInB");
    }

}

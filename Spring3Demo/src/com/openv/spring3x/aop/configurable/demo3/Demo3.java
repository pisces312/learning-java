package com.openv.spring3x.aop.configurable.demo3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * 演示Configurable注解的使用
 * 
 * 3 借助<context:spring-configured/>元素能起到隐式使用 AnnotationBeanConfigurerAspect
 * 切面的目的，下面给出了配置示例（springconfigured.xml）。
 */
public class Demo3 {
	private static final Log log = LogFactory.getLog(Demo3.class);

	public static void main(String[] args) {
		// 启用Spring DI，并完成AspectJ 6切面的配置工作，比如将IoC容器暴露给切面
		new ClassPathXmlApplicationContext("aop_springconfigured.xml");
		log.info("即将构建领域对象");
		// 很多时候，Hibernate/JPA/应用代码会负责创建领域对象
		SecurityAccount3 securityAccount = new SecurityAccount3();
		log.info("AspectJ 6已经完成了领域对象的配置工作");
		// 没有显式设置securityAccount领域对象的协作者，但IBankSecurityService服务确实不再是null
		securityAccount.toBank(10000.00);
		securityAccount.toSecurity(10000.00);
	}
}

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.common.io.CharStreams;

public class ReadStringFromFile {

//    public static String getStringFromFile(Class<?> cls, String filePath) {
//        try {
//            InputStream is = cls.getResourceAsStream(filePath);
//            return CharStreams.toString(new InputStreamReader(is));
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//
//    }

    public static void main(String[] args) {
        //        String filePath = "src/main/reaources/sharespeare.raw";
        String filePath = "/sharespeare.raw";
        //        String fileContent = getStringFromFile(ReadStringFromFile.class, filePath);
        String fileContent = null;
        try {
            fileContent = CharStreams.toString(
                new InputStreamReader(ReadStringFromFile.class.getResourceAsStream(filePath)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(fileContent);
    }

}

package spring.asyncrest;

import java.util.UUID;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
public class AsyncProcessController {
    private static Logger logger = LoggerFactory.getLogger(AsyncProcessController.class);

    @Autowired
    private AsyncService asyncService;

    @Autowired
    private AsyncProcess process;

    @RequestMapping("/longProcess")
    public String longProcess() throws InterruptedException {
        String jobId = UUID.randomUUID().toString();
        logger.info("Starting long process...");
        Future<String> future = process.asyncCall();
        asyncService.add(jobId, future);
        logger.info("Done!");
        return "{success: " + jobId + "}";

    }

    @RequestMapping("/longProcess/{id}")
    public DeferredResult<String> deferredCall(@PathVariable String id) {
        return asyncService.getAsyncUpdate(id);
    }
}

package core.proxy.dynamic.jdk.simplesample;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * Not recommend this usage. 
 * In actual case, it should pass real subject via ctor or setter for InvocationHandler.
 */
public class SimpleDynProxyMain {


    public static interface MyInterface {
        public void rent();

        public void hello(String str);
    }

    public static class DynamicProxy implements InvocationHandler {
        //In this case, we just use any logic here.
        //In actual case, it should pass real subject via ctor or setter.
        @Override
        public Object invoke(Object object, Method method, Object[] args) throws Throwable {
            System.out.println("Method:" + method + "," + Arrays.toString(args));
//            System.out.println("before");
//            System.out.println(method.getName()+" will be triggered");

            // method.invoke(subject, args);
            return null;
        }

    }

    public static void main(String[] args) {
        //Proxy
        InvocationHandler handler = new DynamicProxy();

        //Associate proxy with interface (must be an interface)
        Object obj = Proxy.newProxyInstance(handler.getClass().getClassLoader(),
            new Class<?>[] {MyInterface.class}, handler);

        //Use interface
        MyInterface subject = (MyInterface) obj;
        System.out.println(subject.getClass().getName());
        subject.rent();
        subject.hello("world");

    }
}

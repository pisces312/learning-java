package chap7.classloading;

/**
 * 被动使用类字段演示二：
* 通过数组定义来引用类，不会触发此类的初始化
 **/
public class ObjArrayNotInit {

    private static class SuperClass {

        static {
            //Not called
            System.out.println("SuperClass init!");
            assert (false);
        }
    }

    public static void main(String[] args) {
        SuperClass[] sca = new SuperClass[10];
    }
}

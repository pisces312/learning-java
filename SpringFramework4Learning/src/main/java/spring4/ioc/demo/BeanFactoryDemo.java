package spring4.ioc.demo;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import spring4.ioc.demo.common.IBankSecurityService;
import spring4.utils.Utils;

public class BeanFactoryDemo {

    public static void main(String[] args) {
        String xmlPath = Utils.getXmlPath(BeanFactoryDemo.class, "ioc_beanfactory.xml");
        Resource resource = new ClassPathResource(xmlPath);

        BeanFactory factory = new XmlBeanFactory(resource);

        IBankSecurityService bankSecurityService = (IBankSecurityService) factory.getBean("bankSecurityService");

        bankSecurityService.bankToSecurity(2000.00);
        bankSecurityService.securityToBank(2000.00);
    }

}

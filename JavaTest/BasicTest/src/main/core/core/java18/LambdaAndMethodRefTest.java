package core.java18;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.Test;

public class LambdaAndMethodRefTest {

    @FunctionalInterface //optional
    interface ConsumerTest {

        public void test(Integer i);
    }

    @Test
    public void testLambdaExpForFuncI() {

//        int i=0;

        Consumer<Integer> consumer = (i) -> {
            System.out.println("count:" + i);
        };

        consumer.accept(0);

        Consumer<Integer> consumer2 = consumer;

        ConsumerTest c = (i) -> {
            System.out.println("count:" + i);
        };
        c.test(23);

        //???
//        ConsumerTest c2=(ConsumerTest) consumer;
//        c2.test(123);

        //exp for func body
        Supplier<String> supplier = () -> "test";
        //exp for func body
        Supplier<String> supplier2 = () -> ("test");
        //block for func body
        Supplier<String> supplier3 = () -> {
            return "test";
        };

        System.out.println(supplier.get());

    }


    @Test
    public void testSortDesOrder() {
        List<Integer> list = Arrays.asList(new Integer[] {3, 2, 5, 1, 6});
        Collections.sort(list, (x, y) -> y - x);
        list.forEach(System.out::println);
    }

    @Test
    public void testRunnableLambda() {
        boolean flag = false;
        //
        //Not allow to change the value of external variable 
//        Runnable r1 = () -> flag=true;
        //
        //No need final for read only operation in lambda's function body
        Runnable r1 = () -> System.out.println(flag);
        Runnable r2 = () -> System.out.println("Hello world two!");
    }

    @Test
    public void testThreadUseLambda() {
        //If only one statement, can just write without ";"
        new Thread(() -> System.out.println("Run!")).start();

        //Must add ";" if use "{}" around statement
        new Thread(() -> {
            System.out.println("Run!");
        }).start();
    }


    public static final class CollectionUtils {
        public static <T, R> List<R> map(List<T> input, Function<T, R> processor) {
            ArrayList<R> result = new ArrayList<R>();
            for (T obj : input) {
                result.add(processor.apply(obj));
            }
            return result;
        }
    }

    @Test
    public void test1() {
        List<String> input = Arrays.asList(new String[] {"apple", "orange", "pear"});
        // lambda expression
        input.forEach((v) -> System.out.println(v));
        // method reference
        input.forEach(System.out::println);

        // ctor ref
        List<Long> dateValues = Arrays.asList(new Long[] {0L, 1000L});
        List<Date> dates = CollectionUtils.map(dateValues, Date::new);
        dates.forEach(System.out::println);

        List<Integer> lengths = CollectionUtils.map(input, v -> v.length());
//    List<Integer> lengths = CollectionUtils.map(input, (v) -> v.length());
        //or
//    List<Integer> lengths = CollectionUtils.map(input, (String v) -> v.length());
        lengths.forEach(System.out::println);

        List<String> uppercases = CollectionUtils.map(input, (String v) -> v.toUpperCase());
        uppercases.forEach(System.out::println);


        Object[] lens = input.stream().map(String::length).toArray();
        System.out.println(Arrays.toString(lens));

        Object[] lens2 = input.stream().map(s -> s.length()).toArray();
        System.out.println(Arrays.toString(lens2));


    }

}

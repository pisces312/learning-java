package spring4.ioc.circular;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanCurrentlyInCreationException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import spring4.utils.Utils;

public class CircleRefTest {


    private String getXmlPath(String xmlFileName) {
        return Utils.getXmlPath(CircleRefTest.class, xmlFileName);
    }

    @Before
    public void setUp() {
        //Set DEBUG level to see what happened clearly
        Utils.setSpringLogLevel(Level.DEBUG);
    }

    @Test(expected = BeanCurrentlyInCreationException.class)
    public void testCircleByConstructor() throws Throwable {
        try {
            new ClassPathXmlApplicationContext(getXmlPath("CircleInCtorConfig.xml"));
            //Singleton bean initialization is covered in IoC refresh
        } catch (Exception e) {
            Throwable beanCurrentlyInCreationException = e.getCause().getCause().getCause();
            throw beanCurrentlyInCreationException;
        }
    }


    @Test(expected = BeanCurrentlyInCreationException.class)
    public void testCircleBySetterAndPrototype() throws Throwable {
        try {
            ClassPathXmlApplicationContext ctx =
                new ClassPathXmlApplicationContext(getXmlPath("CircleInSetterPrototypeConfig.xml"));
            //Must call it explicitly
            //Prototype bean will not be initialized during IoC initialization
            System.out.println(ctx.getBean("circleA"));
        } catch (Exception e) {
            Throwable e1 = e.getCause().getCause().getCause();
            System.out.println(e1);
            throw e1;
        }
    }

    @Test
    public void testCircleBySetterAndSingleton() {
        new ClassPathXmlApplicationContext(getXmlPath("CircleInSetterSingletonConfig.xml"));
    }
}

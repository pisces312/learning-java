package spring4.ioc.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import spring4.ioc.demo.common.BankSecurityDaoImpl;
import spring4.ioc.demo.common.BankSecurityServiceImpl;
import spring4.ioc.demo.common.IBankSecurityDao;
import spring4.ioc.demo.common.IBankSecurityService;


@Configuration
public class AppWithAutowired {


    @Bean
    public IBankSecurityDao bankSecurityDao() {
        return new BankSecurityDaoImpl();
    }

    @Bean
    public IBankSecurityService bankSecurityService() {
        return new BankSecurityServiceImpl();
    }

    @Autowired
    private IBankSecurityService bankSecurityService;

    public void test() {
        bankSecurityService.bankToSecurity(2000.00);
        bankSecurityService.securityToBank(2000.00);
    }

    //Exit automatically
    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext applicationContext =
            new AnnotationConfigApplicationContext(AppWithAutowired.class);) {
            AppWithAutowired app = applicationContext.getBean(AppWithAutowired.class);
            app.test();
        }
    }

}

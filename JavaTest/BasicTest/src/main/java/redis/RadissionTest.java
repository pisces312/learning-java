package redis;


import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.FutureListener;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.reactivestreams.Publisher;
import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.RedissonClient;
import org.redisson.SingleServerConfig;
import org.redisson.api.RAtomicLongReactive;
import org.redisson.api.RedissonReactiveClient;
import org.redisson.core.RAtomicLong;
import org.redisson.core.RAtomicLongAsync;
import org.redisson.core.RBitSet;
import org.redisson.core.RBucket;
import org.redisson.core.RKeys;
import org.redisson.core.RLock;
import org.redisson.core.RMap;
import org.redisson.core.RReadWriteLock;
import org.redisson.core.RedissonMultiLock;

public class RadissionTest {
    String nodeAddress = "10.102.6.39:6379";
    // connects to 127.0.0.1:6379 by default
    Config config = new Config();
    RedissonClient redisson;

    @Before
    public void setUp() throws Exception {
        SingleServerConfig clusterConfig = config.useSingleServer();
        clusterConfig.setAddress(nodeAddress);
        System.out.println(config.toYAML());

        redisson = Redisson.create(config);

        System.out.println(redisson.isShuttingDown());
        System.out.println(redisson.isShutdown());
    }

    @Test
    public void testBasic() throws Exception {

        //Each Redisson object bound to Redis key which means object name and can be read via getName method.
        RMap map = redisson.getMap("mymap");
        System.out.println(map.getName()); // = mymap


        //            All operations with Redis keys extracted to RKeys interface.

        RKeys keys = redisson.getKeys();

        //        Iterable<String> allKeys = keys.getKeys();
        System.out.println("All keys");
        for (String key : keys.getKeys()) {
            System.out.println(key);
        }

        //        Iterable<String> foundedKeys = keys.getKeysByPattern("key*");
        System.out.println("foundedKeys");
        for (String key : keys.getKeysByPattern("key*")) {
            System.out.println(key);
        }

        long numOfDeletedKeys = keys.delete("obj1", "obj2", "obj3");
        long deletedKeysAmount = keys.deleteByPattern("test?");

        String randomKey = keys.randomKey();
        System.out.println("randomKey:" + randomKey);
        long keysAmount = keys.count();
        System.out.println("keysAmount:" + keysAmount);


        //Object
        //        Redisson distributed RBucket object is an universal holder for any type of object.

        RBucket<Long> bucket = redisson.getBucket("anyObject");
        bucket.set(new Long(1));
        Long obj = bucket.get();

        bucket.trySet(new Long(3));
        bucket.compareAndSet(new Long(4), new Long(5));
        bucket.getAndSet(new Long(6));



        //        BitSet

        //        Redisson distributed BitSet object represent vector of bits that grows as needed. BitSet size limited by Redis to 4 294 967 295.

        RBitSet set = redisson.getBitSet("simpleBitset");
        set.set(0, true);
        set.set(1812, false);
        set.clear(0);
        set.andAsync("e");
        set.xor("anotherBitset");
    }

    @Test
    public void testLock() throws Exception {

        //        Redisson distributed reentrant Lock object implements java.util.concurrent.locks.Lock interface and supports TTL.

        RLock lock = redisson.getLock("anyLock");
        // Most familiar locking method
        System.out.println("Enter lock " + lock.getName());
        lock.lock();

        // Lock time-to-live support
        // releases lock automatically after 10 seconds
        // if unlock method not invoked
        //        lock.lock(10, TimeUnit.SECONDS);

        // Wait for 100 seconds and automatically unlock it after 10 seconds
        //        boolean res = lock.tryLock(100, 10, TimeUnit.SECONDS);

        System.out.println("doing something in lock");
        Thread.sleep(1000);


        //        ...
        lock.unlock();
        System.out.println("Exit lock " + lock.getName());
    }

    @Test
    public void testMultiLock() throws Exception {
        //        RedissonMultiLock object groups multiple RLock objects and handles them as one lock. Each RLock object may belong to different Redisson instances.

        RedissonClient redissonInstance1 = redisson;
        RedissonClient redissonInstance2 = redisson;
        RedissonClient redissonInstance3 = redisson;

        RLock lock1 = redissonInstance1.getLock("lock1");
        RLock lock2 = redissonInstance2.getLock("lock2");
        RLock lock3 = redissonInstance3.getLock("lock3");

        RedissonMultiLock lock = new RedissonMultiLock(lock1, lock2, lock3);
        System.out.println("Enter lock " + lock);
        lock.lock();

        System.out.println("doing something in lock");
        Thread.sleep(1000);


        //        ...
        lock.unlock();
        System.out.println("Exit lock " + lock);

    }

    @Test
    public void testReadWriteLock() throws Exception {
        RReadWriteLock rwlock = (RReadWriteLock) redisson.getLock("anyRWLock");
        // Most familiar locking method
        rwlock.readLock().lock();

        rwlock.readLock().unlock();

        // or
        rwlock.writeLock().lock();
        rwlock.writeLock().unlock();

        // Lock time-to-live support
        // releases lock automatically after 10 seconds
        // if unlock method not invoked
        //     rwlock.readLock().lock(10, TimeUnit.SECONDS);
        // or
        //     rwlock.writeLock().lock(10, TimeUnit.SECONDS);

        // Wait for 100 seconds and automatically unlock it after 10 seconds
        //     boolean res = rwlock.readLock().tryLock(100, 10, TimeUnit.SECONDS);
        // or
        //     boolean res = rwlock.writeLock().tryLock(100, 10, TimeUnit.SECONDS);
        //     ...
        //     lock.unlock();
    }

    @Test
    public void testAsyncWay() throws Exception {
        long initVal = 0;
        RAtomicLong longSyncObj = redisson.getAtomicLong("myLong");
        longSyncObj.set(initVal);

        RAtomicLongAsync longObject = redisson.getAtomicLong("myLong");
        System.out.println(longObject);
        Future<Boolean> future = longObject.compareAndSetAsync(initVal, 401);
        future.addListener(new FutureListener<Boolean>() {
            @Override
            public void operationComplete(Future<Boolean> future) throws Exception {
                if (future.isSuccess()) {
                    // get result
                    Boolean result = future.getNow();
                    System.out.println(result);
                    // ...
                } else {
                    // an error has occurred
                    Throwable cause = future.cause();
                    System.out.println(cause);
                }
            }
        });
        Thread.sleep(1000);
    }

    @Test
    public void testReactive() throws Exception {
        RedissonReactiveClient client = Redisson.createReactive(config);
        RAtomicLongReactive longObject = client.getAtomicLong("myLong");

        Publisher<Boolean> csPublisher = longObject.compareAndSet(10, 91);
        System.out.println(csPublisher);
        Publisher<Long> getPublisher = longObject.get();
        System.out.println(getPublisher);
    }
}

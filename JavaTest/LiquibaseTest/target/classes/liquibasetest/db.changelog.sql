--liquibase formatted sql

--changeset nvoxland:1
drop table if exists department;

--changeset nvoxland:2
create table department (
    id int primary key,
    name varchar(255)
);
--rollback drop table department;

--changeset nvoxland:3
insert into department (id, name) values (1, 'a');
insert into department (id, name) values (2, 'b');
package log;

import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaLogTest {
    private static final String PKG        = JavaLogTest.class.getPackage().getName();

    public static final Logger  logger     = Logger.getLogger(PKG);

    private static final String CLASS_NAME = JavaLogTest.class.getName();

    public JavaLogTest() {
        //        LOGGER.addHandler(new ConsoleHandler());
        //        LOGGER.setLevel(level);
    }

    public void setLevel(Level level) {
        logger.setLevel(level);
    }

    public void test(Level level) {
        String methodName = "test";

        logger.setLevel(level);
        if (logger.isLoggable(level)) {
            System.out.println("isLoggable");
            logger.logp(level, CLASS_NAME, methodName, "isLoggable test log");
        } else {
            System.out.println("not isLoggable");
            logger.logp(level, CLASS_NAME, methodName, "not isLoggable test log");
        }
    }

    public void testFinest(Level loggableLevel) {
        String methodName = "test";

        logger.setLevel(loggableLevel);
        System.out.println(loggableLevel);
        if (logger.isLoggable(loggableLevel)) {
            System.out.println("-----------isLoggable");
            logger.logp(Level.FINEST, CLASS_NAME, methodName, "isLoggable test log");
        } else {
            //!only if Level is set to OFF, it won't be logged
            System.out.println("-----------not isLoggable");
            logger.logp(Level.FINEST, CLASS_NAME, methodName, "not isLoggable test log");
        }
    }

    public void test1(Level loggableLevel) {
        //!only level is higher than current can be loggable
        logger.setLevel(loggableLevel);
        System.out.println(loggableLevel);
        if (logger.isLoggable(Level.FINEST)) {

            System.out.println("FINEST-----------isLoggable");
        }

        if (logger.isLoggable(Level.FINER)) {

            System.out.println("FINER-----------isLoggable");
        }
        if (logger.isLoggable(Level.FINE)) {

            System.out.println("FINE-----------isLoggable");
        }
        if (logger.isLoggable(Level.INFO)) {

            System.out.println("INFO-----------isLoggable");
        }
        if (logger.isLoggable(Level.WARNING)) {

            System.out.println("WARNING-----------isLoggable");
        }
        if (logger.isLoggable(Level.SEVERE)) {

            System.out.println("SEVERE-----------isLoggable");
        }
    }

    public void test2(Level level) {
        String methodName = "test";
        logger.logp(level, CLASS_NAME, methodName, "without isLoggable test");
        if (logger.isLoggable(level))
            logger.logp(level, CLASS_NAME, methodName, "with isLoggable test");
    }

    /**
     * @param args
     */
    public static void main(String [] args) {
        String methodName = "main";
//        LogTest logTest = new LogTest();
        //        logTest.test(Level.ALL);
        //        logTest.testFinest(Level.ALL);
        //        logTest.testFinest(Level.FINEST);
        //        logTest.testFinest(Level.FINER);
        //        logTest.testFinest(Level.FINE);
        //        logTest.testFinest(Level.INFO);
        //        logTest.testFinest(Level.WARNING);
        //        logTest.testFinest(Level.SEVERE);
        //        logTest.testFinest(Level.OFF);
        //
        //
        //        logTest.test1(Level.INFO);
        //
        //
//        logTest.setLevel(Level.WARNING);
//        logTest.test2(Level.INFO);

        //        logTest.setLevel(Level.INFO);
        //        logTest.test2(Level.INFO);

        //        logTest.setLevel(Level.INFO);
        //        logTest.test2(Level.FINE);

        //        logTest.setLevel(Level.FINEST);
        //        logTest.test2(Level.INFO);
//
//        LOGGER.setLevel(Level.FINEST);
//        LOGGER.logp(Level.INFO, CLASS_NAME, methodName, "without isLoggable test");
//        if (LOGGER.isLoggable(Level.INFO))
//            LOGGER.logp(Level.INFO, CLASS_NAME, methodName, "with isLoggable test");
//        
//        
//        
//        LOGGER.setLevel(Level.WARNING);
//        LOGGER.logp(Level.INFO, CLASS_NAME, methodName, "without isLoggable test");
//        if (LOGGER.isLoggable(Level.INFO))
//            LOGGER.logp(Level.INFO, CLASS_NAME, methodName, "with isLoggable test");
//        
        logger.setLevel(Level.FINEST);
        if (logger.isLoggable(Level.FINEST)){
            logger.entering(CLASS_NAME, methodName,"abcdefghigjk");
            
        }

    }

}

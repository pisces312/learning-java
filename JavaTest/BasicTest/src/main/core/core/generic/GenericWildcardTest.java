package core.generic;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class GenericWildcardTest {
    public class Creature {

    }
    public class Animal extends Creature {

    }
    public class Cat extends Animal {

    }
    public class Plant extends Creature {

    }
    public class Apple extends Plant {

    }

    @Test
    public void test1() {
        List<?> list1 = new LinkedList<Creature>();
        List<? super Cat> list2 = new LinkedList<Cat>();
        List<? super Cat> list3 = new LinkedList<Creature>();
        List<? extends Animal> list4 = new LinkedList<Cat>();
        List<? extends Animal> list5 = new LinkedList<Animal>();
    }

}

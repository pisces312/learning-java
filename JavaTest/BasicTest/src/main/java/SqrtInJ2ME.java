/**
 *
 * J2ME中，确切的是CLDC1.0/MIDP1.0乃至CLDC1.1/MIDP2.0中，没有开平方的函数（在J2SE中是通过java.lang.Math.sqrt()方法实现的）。
 * 但是在游戏数值平衡中，开平方是一个很基本的方式。
 * 在一次项目会议上，组员提出如何实现开方，我没加思索，提出采用逆向逼近法。但后来考虑这样的性能很低，在手机上如此宝贵的CPU资源不能就这样浪费了！于是考虑到采用实现手算开方法。
 * 在构思流程时突然意识到，同样算法在二进制下应该是可用的！经过验证，有了以下代码：
 * 
 * @author pisces312
 */
public class SqrtInJ2ME {

    public static long sqrt(long x) {
        long y = 0;
        long b = (~Long.MAX_VALUE) >>> 1;
        while (b > 0) {
            if (x >= y + b) {
                x -= y + b;
                y >>= 1;
                y += b;
            } else {
                y >>= 1;
            }
            b >>= 2;
        }
        return y;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(sqrt(100));
        System.out.println(sqrt(120));
        System.out.println(sqrt(148));
    }
}

package chap3.allocpolicy;

/**
 * Auto adjust tenuring threshold
 * 
 * -XX:MaxTenuringThreshold + -XX:+UseSerialGC
 * Hotspot 1.8 will calculate new threshold dynamically.
 * So even we set it to 15, it may be moved to tenured generation
 * http://www.chepoo.com/max-tenuring-threshold-parameter.html
 * 
 * -XX:MaxTenuringThreshold
 * default is 15
 * Object age, when it's survived in Minor GC, it's increased by 1.
 * 
 * -XX:+PrintTenuringDistribution
 * print detail about tenure
 * 
 * 
 */
public class AutoAdjustTenuringThreshold {
    private static final int _1MB = 1024 * 1024;


    public static void main(String[] args) {
        testTenuringThreshold2();
    }

//-verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:+UseSerialGC -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=1 -XX:+PrintTenuringDistribution
    @SuppressWarnings("unused")
    public static void testTenuringThreshold1() {
        byte[] allocation1, allocation2, allocation3;
        allocation1 = new byte[_1MB / 4];
        allocation2 = new byte[4 * _1MB];
        //
        allocation3 = new byte[4 * _1MB];
//        Actually all survivors are moved to tenured generation
        allocation3 = null;
        allocation3 = new byte[4 * _1MB];
    }

//-verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:+UseSerialGC -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -XX:+PrintTenuringDistribution
    @SuppressWarnings("unused")
    public static void testTenuringThreshold2() {
        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[_1MB / 4]; // allocation1+allocation2����survivo�ռ�һ��
        allocation2 = new byte[_1MB / 4];
        allocation3 = new byte[4 * _1MB];
        allocation4 = new byte[4 * _1MB];
        allocation4 = null;
        allocation4 = new byte[4 * _1MB];
    }

}

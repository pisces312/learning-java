package spring4.ioc.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import spring4.ioc.demo.common.BankSecurityDaoImpl;
import spring4.ioc.demo.common.BankSecurityServiceImpl;
import spring4.ioc.demo.common.IBankSecurityDao;
import spring4.ioc.demo.common.IBankSecurityService;

/**
 * 
 * 
 */
@Configuration
public class AnnotatedSingletonBeanSample {

    private static ApplicationContext applicationContext;

    @Bean
    public IBankSecurityDao bankSecurityDao() {
        return new BankSecurityDaoImpl();
    }

    @Bean
    public IBankSecurityService bankSecurityService() {
        return new BankSecurityServiceImpl();
    }

    public static void main(String[] args) {
        applicationContext = new AnnotationConfigApplicationContext(AnnotatedSingletonBeanSample.class);

        IBankSecurityService bankSecurityService = applicationContext.getBean(IBankSecurityService.class);

        bankSecurityService.bankToSecurity(2000.00);
        bankSecurityService.securityToBank(2000.00);
    }

}

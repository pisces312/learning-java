package chap8.dynamiclang;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

/**
 * ???Cannot get expected result in JDK8
 *
 */
public class GrandFatherTest {
    private static abstract class Human {
        public abstract void thinking();
    }
    private static class GrandFather extends Human {
        @Override
        public void thinking() {
            System.out.println("grandfather");
        }
    }

    private static class Father extends GrandFather {
        @Override
        public void thinking() {
            System.out.println("father");
        }
    }

    private static class Son extends Father {

        //Call grandfather's method
        //The current implementation of the MethodHandles/Lookup class 
        //will not allow to call invokeSpecial on any class that is 
        //not private accessible from the current caller class. 
        //
        @Override
        public void thinking() {
            try {
                String mn = "thinking";
                MethodType mt = MethodType.methodType(void.class);
                //
//                Class<?> clazz = GrandFatherTest.class;
//                Class<?> clazz = GrandFather.class;

                Class<?> clazz = getClass();
                MethodHandle mh = MethodHandles.lookup().findSpecial(Human.class, mn, mt, clazz);
                System.out.println(mh);
                mh.invoke(this);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
//        new GrandFatherTest().new Son().thinking();

//        (new GrandFatherTest().new Son()).thinking();
        new Son().thinking();
    }
}

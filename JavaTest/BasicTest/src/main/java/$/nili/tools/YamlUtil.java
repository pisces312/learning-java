package $.nili.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import com.google.common.base.Preconditions;

/**
 * @author Will Sun
 */
public final class YamlUtil {
    
    private static final Logger logger = LoggerFactory.getLogger(YamlUtil.class);

    public static Map<String, Object> load(String yamlString){
        Preconditions.checkArgument(StringUtils.isNotBlank(yamlString), "yaml string can't be empty");
        Yaml yaml = new Yaml();
        return (Map<String, Object>)yaml.load(yamlString);
    }

    public static String toString(Object data){
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setIndent(4);
        Yaml yaml = new Yaml(options);
        StringWriter writer = new StringWriter();
        yaml.dump(data, writer);
        return writer.toString();
    }
    
    public static <T> T loadFromYamlFile(String fileName, Class<T> _class) {
        Preconditions.checkArgument(StringUtils.isNotBlank(fileName), "File name cannot be empty.");
        Yaml yaml = new Yaml();
        try {
            return yaml.loadAs(new FileReader(new File(fileName)), _class);
        } catch (FileNotFoundException e) {
            logger.error("Failed to read yaml file: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

}

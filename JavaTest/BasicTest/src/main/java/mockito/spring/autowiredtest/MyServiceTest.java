package mockito.spring.autowiredtest;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.util.ReflectionTestUtils;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

public class MyServiceTest {
    @Component
    public static class MyService {
        @Autowired
        private MessageService messageService;

        public String say(String name) {
            return messageService.getMessage() + name;
        }
    }

    @Component
    public static class MessageService {
        public String getMessage() {
            return "Hello, ";
        }
    }

    @Test
    public void sayHi() {
        MessageService messageService = mock(MessageService.class);
        when(messageService.getMessage()).thenReturn("Hi, ");

        MyService myService = new MyService();
        // Inject mock into private field:
        ReflectionTestUtils.setField(myService, "messageService", messageService);

        assertEquals("Hi, mrhaki", myService.say("mrhaki"));
    }
}

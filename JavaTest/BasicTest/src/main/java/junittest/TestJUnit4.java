package junittest;


import java.util.ArrayList;
import java.util.List;
import junit.framework.JUnit4TestAdapter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
//import static junit.framework.Assert.assertEquals;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *Junit4的新特性
 * 首先测试的方法不需要以“test”作为前缀
 * 测试类也不需要继承自TestCase
 * @author DELL
 */
public class TestJUnit4 {

    @Test
    public void addition() {

        Assert.assertEquals(12,7+5);
    }

    @Test
    public void subtraction() {
        Assert.assertEquals(9, 12-3);
    }

    /**
     * @Before and @After
    使用@Before和 @After，对应于“setup”和“tearDown”
     * 他们在测试方法之前和之后执行
     */
    @Before
    public void runBeforeEveryTest() {
//        simpleMath = new SimpleMath();
    }

    @After
    public void runAfterEveryTest() {
//        simpleMath = null;
    }

    /**
     * @BeforeClass and @AfterClass
    Use @BeforeClass and @AfterClass annotations for
     * class wide “setup” and “tearDown” respectively.
     * 他们只在所有测试之前和之后开始运行一次
     */
    @BeforeClass
    public static void runBeforeClass() {
        // run for one time before all test cases
    }

    @AfterClass
    public static void runAfterClass() {
        // run for one time after all test cases
    }

    /**
     * Exception Handling
     * 异常处理，
     * “expected” paramater with @Test annotation for test cases that expect exception.
     * Write the class name of the exception that will be thrown.
     */
    @Test(expected = ArithmeticException.class)
    public void divisionWithException() {
        // divide by zero
//        simpleMath.divide(1, 0);
    }

    /**
     * @Ignore
    Put @Ignore annotation for test cases you want to ignore. You can add a string parameter that defines the reason of ignorance if you want.
     */
    @Ignore("Not Ready to Run")
    @Test
    public void multiplication() {
//        assertEquals(15, simpleMath.multiply(3, 5));
    }

    /**
     * Timeout
    Define a timeout period in miliseconds with “timeout” parameter. The test fails when the timeout period exceeds.
     */
    @Test(timeout = 1000)
    public void infinity() {
        while (true) {
        }
    }

    /**
     * New Assertions
     * Compare arrays with new assertion methods. Two arrays are equal if they have the same length and each element is equal to the corresponding element in the other array; otherwise, they’re not.
     */
    public static void assertEquals(Object[] expected, Object[] actual) {
    }

    public static void assertEquals(String message, Object[] expected, Object[] actual) {
    }

    @Test
    public void listEquality() {
        List<Integer> expected = new ArrayList<Integer>();
        expected.add(5);

        List<Integer> actual = new ArrayList<Integer>();
        actual.add(5);

        Assert.assertEquals(expected, actual);
    }

    /**
     * JUnit4Adapter
     * 使Junit4的测试运行在Junit3下
     */
    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(TestJUnit4.class);
    }
}

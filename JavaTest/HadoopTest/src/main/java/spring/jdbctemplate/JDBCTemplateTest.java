package spring.jdbctemplate;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.hive.jdbc.HiveDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.util.CollectionUtils;

@SpringBootApplication
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
//@PropertySource("classpath:/spring/jdbctemplate/app.properties")
public class JDBCTemplateTest implements CommandLineRunner {

    /**
     * Spring’s JdbcTemplate and NamedParameterJdbcTemplate classes are auto-configured and you
     * can @Autowire them directly into your own beans
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... args) throws Exception {
        /**
         * [{database_name=bedrockdemo}, {database_name=bigdatatest}, {database_name=brctest99}]
         */
        List<Map<String, Object>> databaseList = jdbcTemplate.queryForList("show databases");
        System.out.println(databaseList);

        /**
         * [{tab_name=banklist}, {tab_name=banklist_p}, {tab_name=students}]
         */
        List<Map<String, Object>> tableList = jdbcTemplate.queryForList("show tables");
        System.out.println(tableList);

        if (!CollectionUtils.isEmpty(tableList)) {
            String tableName = (String) tableList.get(0).get("tab_name");
            /**
             * [{createtab_stmt=CREATE TABLE `banklist`(}, {createtab_stmt= `bank` string, },
             * {createtab_stmt= `city` string, }, {createtab_stmt= `st` string, }, {createtab_stmt=
             * `cert` string, }, {createtab_stmt= `institution` string, }, {createtab_stmt=
             * `closingdate` string, }, {createtab_stmt= `updateddate` string)}, {createtab_stmt=ROW
             * FORMAT DELIMITED }, {createtab_stmt= FIELDS TERMINATED BY ',' },
             * {createtab_stmt=STORED AS INPUTFORMAT }, {createtab_stmt=
             * 'org.apache.hadoop.mapred.TextInputFormat' }, {createtab_stmt=OUTPUTFORMAT },
             * {createtab_stmt= 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'},
             * {createtab_stmt=LOCATION}, {createtab_stmt=
             * 'hdfs://YongDemoCloudera:8020/user/hive/warehouse/banklist'},
             * {createtab_stmt=TBLPROPERTIES (}, {createtab_stmt= 'COLUMN_STATS_ACCURATE'='true', },
             * {createtab_stmt= 'numFiles'='1', }, {createtab_stmt= 'numRows'='0', },
             * {createtab_stmt= 'rawDataSize'='0', }, {createtab_stmt= 'totalSize'='45472', },
             * {createtab_stmt= 'transient_lastDdlTime'='1471870773')}]
             */
            List<Map<String, Object>> showCreateTableList = jdbcTemplate.queryForList("show create table " + tableName);
            System.out.println(showCreateTableList);
            //            String showCreateTableState = jdbcTemplate.queryForObject("show create table " + tableName, String.class);
            //            System.out.println(showCreateTableState);
        }
        //int rowCount = this.jdbcTemplate.queryForObject("select count(*) from t_actor", Integer.class);
    }

    //////////////////////////////////////////////////////////////
    @Bean
    //    @Profile("hive")
    public DataSource hiveDataSource() throws ClassNotFoundException {
        System.setProperty("socksProxyHost", "10.108.219.37");
        System.setProperty("socksProxyPort", "9876");
        String url = "jdbc:hive2://172.101.105.237:10000";
        String username = "hive";
        String password = "hive";
        return new SimpleDriverDataSource(new HiveDriver(), url, username, password);
    }

    //    @Bean
    ////    @Profile("mysql")
    //    public DataSource mysqlDataSource() {
    //        String driver = "com.mysql.jdbc.Driver";
    //        String url = "jdbc:mysql://localhost:3306/bdldac?createDatabaseIfNotExist=true";
    //        String username = "root";
    //        String password = "123456";
    //        DriverManagerDataSource dataSource = new DriverManagerDataSource();
    //        dataSource.setDriverClassName(driver);
    //        dataSource.setUrl(url);
    //        dataSource.setUsername(username);
    //        dataSource.setPassword(password);
    //        return dataSource;
    //    }

    //////////////////////////////////////////////////////////////


    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(JDBCTemplateTest.class);
        app.setWebEnvironment(false);
        app.run(args);
    }



}

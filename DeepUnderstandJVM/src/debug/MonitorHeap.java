package debug;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Eden space呈锯齿状上升，用完后触发MinorGC，放入Old space
 * 
 * Only set -Xms and -Xmx, not set -Xmn
 * 
 * -Xmn is set automatically as 1/3 of max heap size
 * 
 * Heap Configuration:
   MinHeapFreeRatio         = 40
   MaxHeapFreeRatio         = 70
   MaxHeapSize              = 104857600 (100.0MB)
   NewSize                  = 34930688 (33.3125MB)
   MaxNewSize               = 34930688 (33.3125MB)
   OldSize                  = 69926912 (66.6875MB)
   NewRatio                 = 2
   SurvivorRatio            = 8
   MetaspaceSize            = 21807104 (20.796875MB)
   CompressedClassSpaceSize = 1073741824 (1024.0MB)
   MaxMetaspaceSize         = 17592186044415 MB
   G1HeapRegionSize         = 0 (0.0MB)
 */
//Basic(Just use VisualVM to connect):
//java -Xms100m -Xmx100m -XX:+UseSerialGC -cp . debug.MonitorHeap
//
//jdwp with -Xdebug:
//java -Xms100m -Xmx100m -XX:+UseSerialGC -Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8888 -cp . debug.MonitorHeap
//
//jdwp without -Xdebug:
//java -Xms100m -Xmx100m -XX:+UseSerialGC -Xrunjdwp:transport=dt_socket,server=y,address=8888 -cp . debug.MonitorHeap
//
//jmx:
//java -Xms100m -Xmx100m -XX:+UseSerialGC -Dcom.sun.management.jmxremote.port=10999 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -cp . debug.MonitorHeap
public class MonitorHeap {
    private static class OOMObject {
        private byte[] placeholder = new byte[64 * 1024];
    }


    private static void testFillHeap(boolean isGCInMethod) throws InterruptedException {
        fillHeap(1000, isGCInMethod);
        if (!isGCInMethod) {
            System.gc();
        }
    }


    private static void fillHeap(int num, boolean isGC) throws InterruptedException {
        List<OOMObject> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            // 稍作延时，令监视曲线的变化更加明显
            Thread.sleep(50);
            //64KB each time
            list.add(new OOMObject());
        }
        if (isGC) {
            System.gc();
        }

    }

    public static void main(String[] args) throws Exception {
        System.out.println("started");

        //gc is called in 'fillHeap', old space will not been collected
        //monitor_heap_1.png
//        testFillHeap(true);

        //gc is called outside 'fillHeap', old space will been collected
        //monitor_heap_2.png
        testFillHeap(false);

    }
}

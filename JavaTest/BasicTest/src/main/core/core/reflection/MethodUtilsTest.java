package core.reflection;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.util.ReflectionUtils;

public class MethodUtilsTest {

    public static class Handler {
        public void handle(String str) {
            System.out.println("handle String");
        }

        public void handle(List<?> list) {
            System.out.println("handle List");
        }
    }

    @Test
    public void test() throws Exception {
        Handler handler = new Handler();
        
        org.apache.commons.beanutils.MethodUtils.invokeMethod(handler, "handle", "str");
        org.apache.commons.beanutils.MethodUtils.invokeMethod(handler, "handle", new ArrayList<>());
        
        org.apache.commons.lang3.reflect.MethodUtils.invokeMethod(handler, "handle", "str");
        org.apache.commons.lang3.reflect.MethodUtils.invokeMethod(handler, "handle", new ArrayList<>());
    }
}

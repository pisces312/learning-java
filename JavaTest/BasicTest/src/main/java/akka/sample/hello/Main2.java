package akka.sample.hello;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import scala.concurrent.Await;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.sample.hello.GreeterActor.Msg;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Main2 {

    public static class MyPosionPill extends PoisonPill{

        /**
         * 
         */
        private static final long serialVersionUID = 1817182030840597067L;
        
    }

    @Test
    public void testPoisonPill() throws Exception {
        String actorSystemName = "HelloActorSystem";
        String greeterActorName = "GreeterActor";
        Config config = ConfigFactory.parseString("akka {loglevel = DEBUG}");
        ActorSystem system = ActorSystem.create(actorSystemName, config);



        ActorRef greeterActor = system.actorOf(Props.create(GreeterActor.class), greeterActorName);
        System.out.println("actor path:" + greeterActor.path());
        
        
        greeterActor.tell("test", null);
        
        //xxx
//        greeterActor.tell(new MyPosionPill(), null);

        
        
//        Thread.sleep(1000);
        
//        try {
//            Await.result(system.whenTerminated(),new FiniteDuration(1, TimeUnit.SECONDS));
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        system.awaitTermination(new FiniteDuration(1, TimeUnit.SECONDS));
    }

    @Test
    public void testBasic() {
        String actorSystemName = "HelloActorSystem";
        String helloWorldActorName = "HelloWorldActor";

        // json style
        Config config = ConfigFactory.parseString("akka {loglevel = DEBUG}");
        // property style
        // Config config = ConfigFactory.parseString("akka.loglevel = DEBUG");

        // ActorSystem is a heavy object: create only one per application
        ActorSystem system = ActorSystem.create(actorSystemName, config);

        // system.settings().StdoutLogLevel()

        system.log().debug("ActorSystem started");

        // system.log().

        // This is a handle to the actor instance and the only way to
        // interact with it. The ActorRef is immutable and has a one to one relationship with the
        // Actor it represents.
        ActorRef a = system.actorOf(Props.create(HelloWorldActor.class), helloWorldActorName);

        // actor path: akka://actorSystemName/user/helloWorldActorName
        // user is a system actor
        System.out.println("actor path:" + a.path());

        // ActorRef subLevelActor=a.


        // The parameter of Terminator ctor is a
        system.actorOf(Props.create(TerminatorActor.class, a), "terminator");

        // system.terminate();

    }

    public static void main(String[] args) {

    }


}

package jcip.chap07.taskcancel;

import java.util.concurrent.*;

/**
 * NoncancelableTask
 * <p/>
 * Noncancelable task that restores interruption before exit
 * 
 * Not throw InterruptedException to caller
 *
 * @author Brian Goetz and Tim Peierls
 */
public class NoncancelableTask {
    public Task getNextTask(BlockingQueue<Task> queue) {
        boolean interrupted = false;
        try {
            while (true) {
                try {
                    return queue.take();
                } catch (InterruptedException e) {
                    interrupted = true;
                    //! fall through and retry until success
                    //! so it's noncancelable
                }
            }
        } finally {
            //Record if the task was once interrupted
            if (interrupted)
                Thread.currentThread().interrupt();
        }
    }

    interface Task {
    }
}

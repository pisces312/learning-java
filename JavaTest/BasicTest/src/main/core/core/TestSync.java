package core;
public class TestSync {
    public static class Base {
        public synchronized void foo() {
            System.out.println("Base.foo");
        }
        
        //XXX wrong way to call
        public synchronized void bar(){
            System.out.println("bar");
            bar();
        }

    }
    public static class Subclass extends Base {
        public synchronized void foo() {
            super.foo();
            System.out.println("Subclass.foo");

        }
    }

    /**
     * @param args
     */
    public static void main(String [] args) {
//        new Subclass().foo();
        new Subclass().bar();

    }

}

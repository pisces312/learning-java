package chap3;

/**
 * 此代码演示了两点： 
 * 1.对象可以在被GC时自我拯救。 
 * 2.这种自救的机会只有一次，因为一个对象的finalize()方法最多只会被系统自动调用一次
 * @author zzm
 */
public class FinalizeEscapeGC {

    public static FinalizeEscapeGC SAVE_HOOK = null;

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Enter finalize method");
        super.finalize();
        System.out.println("Rescue SAVE_HOOK");
        //Assign "this" back
        FinalizeEscapeGC.SAVE_HOOK = this;
        System.out.println("Exit finalize method");
    }

    /**
     * Only can rescue once
     * 
     * @return
     * @throws Throwable
     */
    private static boolean rescue() throws Throwable {
        // trigger GC
        SAVE_HOOK = null;
        System.gc();

        // wait for finalize() for the first time
        // sleep 500ms since background Finalizer thread has low priority
        Thread.sleep(500);

        if (SAVE_HOOK != null) {
            //Check alive here
            System.out.println("yes, i am still alive :)");
            return true;
        }
        System.out.println("no, i am dead :(");
        return false;

    }

    public static void main(String[] args) throws Throwable {
        SAVE_HOOK = new FinalizeEscapeGC();
        boolean alive = false;
        //1) first time rescue
        alive = rescue();
        assert (alive);

        //2) second time rescue
        // finalize() will only be called once, will not work this tie
        alive = rescue();
        assert (!alive);
    }
}


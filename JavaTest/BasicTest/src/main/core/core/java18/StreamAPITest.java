package core.java18;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Test;

public class StreamAPITest {
    public static class Item {
        Integer id;

        public Item(Integer id) {
            this.id = id;
        }

        public Integer getId() {
            return id;
        }
    }

    @Test
    public void testList2Map() {

        List<Item> list = new LinkedList<>();
//        Map<Integer, Item> map = new HashMap<Integer, Item>();
        for (int i = 0; i < 10; ++i) {
            list.add(new Item(i));
        }
        Map<Integer, Item> map = list.stream().collect(Collectors.toMap(Item::getId, c -> c));
        System.out.println(map);

        map = list.stream().collect(Collectors.toMap(c -> c.getId(), c -> c));
        System.out.println(map);


        map = list.stream().collect(HashMap<Integer, Item>::new, (m, c) -> m.put(c.getId(), c),
            (m, u) -> {
            });
        System.out.println(map);



        AtomicInteger index = new AtomicInteger();
        map = list.stream().collect(Collectors.toMap(i -> index.getAndIncrement(), i -> i));
        System.out.println(map);
    }

    @Test
    public void testParallelStream() {

        Map<String, List<Integer>> numbersPerThread = IntStream.rangeClosed(1, 160).parallel()
            .boxed().collect(Collectors.groupingBy(i -> Thread.currentThread().getName()));
        numbersPerThread.forEach((k, v) -> System.out.println(String.format("%s >> %s", k, v)));

    }


    /**
     * filter: filter by specified condition
     * 
     * map: do some specified transformations on element
     * 
     * forEach: do some actions
     * 
     */
    @Test
    public void test160217() {
        String[] strArray = new String[] {"a", "sb", "c"};
        List<String> list = Arrays.asList(strArray);
        // final List<String> filtered =
        list.stream().filter(s -> s.startsWith("s")).map(s -> s.toUpperCase())
            .forEach(System.out::println);


        // list.stream().filter(s -> s.startsWith("s")).map(s -> s.toUpperCase()).map();
        // .forEach(System.out::println);

        // System.out.println(list);
        /**
         * 
         * final List<String> filtered = Lists.newArrayList(); for (String str : list) { if
         * (str.startsWith("s") { filtered.add(str.toUpperCase()); } }
         * 
         */
    }

    @Test
    public void testBasicDataStream() {
        IntStream.of(new int[] {1, 2, 3}).forEach(System.out::println);
        IntStream.range(1, 3).forEach(System.out::println);
        IntStream.rangeClosed(1, 3).forEach(System.out::println);
    }

    @Test
    public void testConvert() {
        // 1. Individual values
        Stream<String> stream = Stream.of("a", "b", "c");
        // 2. Arrays
        String[] strArray = new String[] {"a", "b", "c"};
        stream = Stream.of(strArray);
        stream = Arrays.stream(strArray);
        // 3. Collections
        List<String> list = Arrays.asList(strArray);
        stream = list.stream();

        // 1. Array
        String[] strArray1 = stream.toArray(String[]::new);
        // 2. Collection
        List<String> list1 = stream.collect(Collectors.toList());
        List<String> list2 = stream.collect(Collectors.toCollection(ArrayList::new));
        Set set1 = stream.collect(Collectors.toSet());
        Stack stack1 = stream.collect(Collectors.toCollection(Stack::new));
        // 3. String
        String str = stream.collect(Collectors.joining()).toString();



    }

    @Test
    public void testConvertToCapital() {

        String[] strArray = new String[] {"a", "b", "c"};
        List<String> wordList = Arrays.asList(strArray);
        wordList.stream().map(String::toUpperCase).collect(Collectors.toList())
            .forEach(System.out::println);
    }

    @Test
    public void testSquare() {
        List<Integer> nums = Arrays.asList(1, 2, 3, 4);
        List<Integer> squareNums = nums.stream().map(n -> n * n).collect(Collectors.toList());
    }

    @Test
    public void testFlatmap() {
        Stream<List<Integer>> inputStream =
            Stream.of(Arrays.asList(1), Arrays.asList(2, 3), Arrays.asList(4, 5, 6));
        Stream<Integer> outputStream = inputStream.flatMap((childList) -> childList.stream());
        outputStream.forEach(System.out::println);
    }

    @Test
    public void testFilter() {
        Integer[] sixNums = {1, 2, 3, 4, 5, 6};
        Stream.of(sixNums).filter(n -> n % 2 == 0).forEach(System.out::println);

    }

    /**
     * First parameter is the initial value, one of the parameter of binary operator
     */
    @Test
    public void testReduce() {
        // 字符串连接，concat = "ABCD"
        String concat = Stream.of("A", "B", "C", "D").reduce("", String::concat);
        // 求最小值，minValue = -3.0
        double minValue = Stream.of(-1.5, 1.0, -3.0, -2.0).reduce(Double.MAX_VALUE, Double::min);
        // 求和，sumValue = 10, 有起始值
        int sumValue = Stream.of(1, 2, 3, 4).reduce(0, Integer::sum);
        // 求和，sumValue = 10, 无起始值
        sumValue = Stream.of(1, 2, 3, 4).reduce(Integer::sum).get();
        // 过滤，字符串连接，concat = "ace"
        concat = Stream.of("a", "B", "c", "D", "e", "F").filter(x -> x.compareTo("Z") > 0)
            .reduce("", String::concat);
    }

    @Test
    public void testLimitAndSkip() {
        List<Person> persons = new ArrayList<Person>();
        for (int i = 1; i <= 10000; i++) {
            Person person = new Person(i, "name" + i);
            persons.add(person);
        }
        List<String> personList2 =
            persons.stream().map(Person::getName).limit(10).skip(3).collect(Collectors.toList());
        System.out.println(personList2);
    }



    @Test
    public void testFindLongest() {
        try (BufferedReader br = new BufferedReader(new FileReader("pom.xml"));) {
            Stream<String> streams = br.lines();
            // streams.forEach(System.out::println);
            int longest = streams.mapToInt(String::length).max().getAsInt();
            System.out.println(longest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testManipulateFile() {
        try (BufferedReader br = new BufferedReader(new FileReader("shakespeare.raw"));) {
            List<String> words = br.lines().flatMap(line -> Stream.of(line.split(" ")))
                .filter(word -> word.length() > 0).map(String::toLowerCase).distinct().sorted()
                .collect(Collectors.toList());
            System.out.println(words);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMatch() {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person(1, "name" + 1, 10));
        persons.add(new Person(2, "name" + 2, 21));
        persons.add(new Person(3, "name" + 3, 34));
        persons.add(new Person(4, "name" + 4, 6));
        persons.add(new Person(5, "name" + 5, 55));
        // all match, then return true
        boolean isAllAdult = persons.stream().allMatch(p -> p.getAge() > 18);
        System.out.println("All are adult? " + isAllAdult);
        boolean isThereAnyChild = persons.stream().anyMatch(p -> p.getAge() < 12);
        System.out.println("Any child? " + isThereAnyChild);
        boolean flag = persons.stream().noneMatch(p -> p.getAge() >= 60);
        System.out.println(flag);
    }

    @Test
    public void testCustomStream() {
        Random seed = new Random();
        Supplier<Integer> random = seed::nextInt;
        Stream.generate(random).limit(10).forEach(System.out::println);
        // Another way
        IntStream.generate(() -> (int) (System.nanoTime() % 100)).limit(10)
            .forEach(System.out::println);
    }



    @Test
    public void testCustomSupplier() {
        Stream.generate(new PersonSupplier()).limit(10)
            .forEach(p -> System.out.println(p.getName() + ", " + p.getAge()));

    }

    @Test
    public void testArithmeticProgression() {
        int d = 3;
        Stream.iterate(0, n -> n + d).limit(10).forEach(x -> System.out.print(x + " "));
    }

    @Test
    public void testGroupBy() {
        Map<Integer, List<Person>> personGroups = Stream.generate(new PersonSupplier()).limit(100)
            .collect(Collectors.groupingBy(Person::getAge));
        Iterator<Entry<Integer, List<Person>>> it = personGroups.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, List<Person>> persons = it.next();
            System.out.println("Age " + persons.getKey() + " = " + persons.getValue().size());
        }
    }

    @Test
    public void testPartitionBy() {
        Map<Boolean, List<Person>> children = Stream.generate(new PersonSupplier()).limit(100)
            .collect(Collectors.partitioningBy(p -> p.getAge() < 18));
        System.out.println("Children number: " + children.get(true).size());
        System.out.println("Adult number: " + children.get(false).size());
    }


    // /////////////////////////////////////////////////////////////////////////////

    private class Person {
        private int no;
        private String name;
        private int age;

        public Person(int no, String name, int age) {
            this.no = no;
            this.name = name;
            this.age = age;
        }

        public Person(int no, String name) {
            this(no, name, -1);
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getName() {
            // System.out.println(name);
            return name;
        }
    }


    private class PersonSupplier implements Supplier<Person> {
        private int index = 0;
        private Random random = new Random();

        @Override
        public Person get() {
            return new Person(index++, "StormTestUser" + index, random.nextInt(100));
        }
    }


    // ////////////////////////////////////////////////////////////////////////
}

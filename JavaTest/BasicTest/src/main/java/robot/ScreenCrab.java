package robot;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ScreenCrab {
    Robot robot;
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

    private BufferedImage getScreen() {
        return robot.createScreenCapture(new Rectangle(0, 0, d.width, d.height));
    }

    public ScreenCrab() {
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public void saveToFile(File f, BufferedImage currentImage) {

        try {

            ImageIO.write(currentImage, "jpg", f);

        } catch (IOException e) {

            e.printStackTrace();

        }
        // }

    }

    public static void main(String[] args) {

    }

}

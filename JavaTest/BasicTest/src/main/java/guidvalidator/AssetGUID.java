package guidvalidator;
import org.springframework.stereotype.Component;


@Component
public class AssetGUID {
  @GUIDValue
  private String guid;

  public AssetGUID() {
    this.guid = "aaed0ed2-ac1e-4ea3-b*";
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getGuid() {
    return guid;
  }
}

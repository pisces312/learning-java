package org.apache.lucene.demo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Test;

public class IngestDocDemo {
    @Test
    public void openExampleIndex() throws IOException {
        // Create a RAM-based index from our test data file
        RAMDirectory rd = new RAMDirectory();
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig iwConfig = new IndexWriterConfig(analyzer);
        
        final FieldType textNoNorms = new FieldType(TextField.TYPE_STORED);
        textNoNorms.setOmitNorms(true);
        
        try (IndexWriter writer = new IndexWriter(rd, iwConfig);) {
            InputStream dataIn = new FileInputStream("src/main/resources/data.tsv");
            try (BufferedReader br =
                new BufferedReader(new InputStreamReader(dataIn, StandardCharsets.UTF_8));) {
                String line = br.readLine();
                while (line != null) {
                    line = line.trim();
                    if (line.length() > 0) {
                        // parse row and create a document
                        StringTokenizer st = new StringTokenizer(line, "\t");
                        Document doc = new Document();
                        doc.add(new Field("location", st.nextToken(), textNoNorms));
                        doc.add(new Field("salary", st.nextToken(), textNoNorms));
                        doc.add(new Field("type", st.nextToken(), textNoNorms));
                        doc.add(new Field("description", st.nextToken(), textNoNorms));

                        System.out.println(doc);
                        writer.addDocument(doc);

                    }
                    line = br.readLine();
                }
            }
        }
        // open searcher
        // this example never closes it reader!
        try (IndexReader reader = DirectoryReader.open(rd);) {
            IndexSearcher searcher = new IndexSearcher(reader);
            //TODO
//            searcher.s
            System.out.println(searcher);
        }
    }
}

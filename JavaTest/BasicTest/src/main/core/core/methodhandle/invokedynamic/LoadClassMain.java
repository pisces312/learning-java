package core.methodhandle.invokedynamic;

import java.lang.reflect.Method;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Handle;

/**
 * Load dynamic generated class dynamically
 *
 */
public class LoadClassMain extends ClassLoader {



    public static void main(String[] args) throws Exception {
        String targetClassName = "ToUpperCaseMain";

        Handle BSM = ToUpperCaseGenerator.createBootstrapHandle();
        ClassWriter cw = ToUpperCaseGenerator.createClass(BSM, targetClassName);

        //Load dynamic generated class
        LoadClassMain classLoader = new LoadClassMain();
        byte[] array = cw.toByteArray();
        Class<?> clazz = classLoader.defineClass(null, array, 0, array.length);

        //Run method
        Method m = clazz.getMethod("main", String[].class);
        m.invoke(null, (Object) null);
    }
}

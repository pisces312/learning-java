package com.openv.spring3x.ioc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BankSecurityDaoImpl implements IBankSecurityDao {

	private static final Log log = LogFactory.getLog(BankSecurityDaoImpl.class);

	public BankSecurityDaoImpl() {
		log.info("ctr BankSecurityDaoImpl");
	}

	public void bankToSecurity(Double money) {
		log.info("即将从银行转入" + money + "元到券商");
	}

	public void securityToBank(Double money) {
		log.info("即将从券商转出" + money + "元到银行");
	}

}

package guidvalidator;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.junit.Test;



public class GUIDValidator implements ConstraintValidator<GUIDValue, String> {
  private String guidReg =
      "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$";
  private Pattern guidPat = Pattern.compile(guidReg);

  
  @Test
  public void testReg(){
    String value="try";
//    String value="aaed0ed2-ac1e-4ea3-bb00-489ac5c1b99";
//    String value="aaed0ed2-ac1e-4ea3-bb00-489ac5c1b999";
    System.out.println(guidPat.matcher(value).matches());
  }
  
  @Override
  public void initialize(GUIDValue guidValue) {
    
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext o) {
    if (value == null)
      return false;
    return guidPat.matcher(value).matches();
  }
}

package chap8.execution.reuse;

/**
 * it make sense to set null in some case 
 *
 */
public class LocalVarCollectedByNull {
    //-verbose:gc
    public static void main(String[] args) {
        byte[] placeholder = new byte[64 * 1024 * 1024];
        //set null
        placeholder = null;
        System.gc();

//        [GC (System.gc())  67532K->66192K(125952K), 0.0012697 secs]
        //after set null, it's collected
//            [Full GC (System.gc())  66192K->529K(125952K), 0.0541919 secs]

    }
}

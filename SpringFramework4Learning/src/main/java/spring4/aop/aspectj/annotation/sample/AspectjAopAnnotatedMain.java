package spring4.aop.aspectj.annotation.sample;

import org.apache.log4j.Level;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import spring4.utils.Utils;

/**
 * AOP annotated configuration and main entry
 *
 */
@Configuration
//
//Use jdk dynamic proxy if target is interface
//In this case, even we set false, it will set true internally and use cglib
//@EnableAspectJAutoProxy
@EnableAspectJAutoProxy(proxyTargetClass=false)
//
//Use cglib
//@EnableAspectJAutoProxy(proxyTargetClass=true)
@ComponentScan
public class AspectjAopAnnotatedMain {
    private static ApplicationContext applicationContext;

    public static void main(String[] args) {
        Utils.setSpringLogLevel(Level.DEBUG);
        applicationContext = new AnnotationConfigApplicationContext(AspectjAopAnnotatedMain.class);
        ClassToProfile instance = applicationContext.getBean(ClassToProfile.class);
        System.out.println("Enhanced class: "+instance.getClass());
        instance.handle();
    }
}

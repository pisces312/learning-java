package core;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void testLog() {
        //        Logger.global.
        Logger.getGlobal().setLevel(Level.INFO);
        String a = null;
        Logger.getGlobal().logp(Level.INFO, "class name", "method name", "{0}", a);
        Logger.getGlobal().logp(Level.INFO, "class name", "method name", "{0}", "abc");
        //no getGlobal()
        Logger.getGlobal().logp(Level.INFO, "class name", "method name", "{ 0 }", "abc");
    }

    static
    {
        System.out.println("Hello, World");
        Thread.setDefaultUncaughtExceptionHandler(
                new Thread.UncaughtExceptionHandler()
                {
                    public void uncaughtException(Thread t, Throwable e)
                    {
                        //                       save information in log file
                    };
                });
    }

    public static String constructLLA(String ipaddress) {
        ipaddress = "abc";
        return ipaddress;
    }

    class Employ {
//        private String name;

        @Override
        public boolean equals(Object arg0) {
//            Employ emp = (Employ)arg0;
//            String n = emp.name;
            return super.equals(arg0);
        }
    }

    public static void testTryFinally() throws Exception {
        try {
            try {
                //                throw new Exception();
            } finally {
                System.out.println("b");
            }
            System.out.println("c");
        } finally {
            System.out.println("a");
        }
        System.out.println("d");
    }

    public static int getSubchassisSlotFromTag(String tagStr) {
        int id = tagStr.lastIndexOf("subchassis");
        if (id < 0) {
            return -1;
        }
        int start = id + "subchassis".length();
        String slotStr = tagStr.substring(start, start + 2);
        return Integer.valueOf(slotStr);
    }

    @SuppressWarnings("null")
    public static void makeNPE() {
        String abc = null;
        //    String bc = abc.toLowerCase();
        abc.charAt(1);
    }

    public static void layer1() {
        //        layer2();
        try {
            layer2();
        } catch (Exception e) {
            System.out.println("catched by layer1");
            e.printStackTrace();
        }
    }

    public static void layer2() {
        layer3();
    }

    public static void layer3() {
        makeNPE();
    }

    /**
     * @param args
     */
    //    public static void main(String [] args) {
    public static void main(String... args) {

        @SuppressWarnings("null")
        long a = (Long)null;
        System.out.println("---------");
        System.out.println(a);
        System.out.println("---------");
        Long b = null;
        System.out.println("---------");
        System.out.println(b);
        System.out.println("---------");
        //        System.out.println(Arrays.toString(new boolean[20]));
        //        int[] a={3,2,7,4,6,2,7,9};
        //        List<Integer> list=new ArrayList<Integer>();
        //        list.add(5);
        //        list.add(2);
        //        list.add(3);
        //        list.add(6);
        //        list.add(4);
        //        list.add(8);
        //        list.add(1);
        //        Collections.sort(list);
        //        for(Integer i:list){
        //            System.out.println(i);
        //        }
        //
        //
        //
        //        try{
        //            makeNPE();
        //        }finally{
        //            System.out.println("finally");
        //        }
        //
        //
        //
        //        HashMap<String, String> runningMap = new HashMap<String, String>();
        //        runningMap.put("abc", "efg");
        //        String value= runningMap.get("b");
        //        System.out.println(value);
        //
        //
        //
        //        synchronized (args) {
        //            try {
        //                args.wait(1000);
        //                System.out.println("abc");
        //            } catch (InterruptedException e) {
        //                // TODO Auto-generated catch block
        //                e.printStackTrace();
        //            }
        //        }
        //
        //
        //
        //        if(2>1){
        //            
        //        }else{
        //           try{
        //               
        //           }finally{
        //               //won't go here
        //               System.out.println("abc");
        //           }
        //        }
        //
        //
        //        try{
        //            layer1();
        //        }catch(Exception e){
        //            System.out.println("catched");
        //            e.printStackTrace();
        //        }
        //        try {
        //            String abc = null;
        ////            String bc = abc.toLowerCase();
        //            abc.charAt(1);
        //            //            System.out.println(abc.toLowerCase());
        ////        }catch(Exception e){
        ////            e.printStackTrace();
        //        }
        //        finally {
        //            System.out.println("abc");
        //        }
        //        System.out.println(null+"abc");
        //        System.out.println(new Date());
        //        GregorianCalendar gc = new GregorianCalendar();
        //        System.out.println(gc.getTime());
        //        gc.add(Calendar.SECOND, 2);
        //        System.out.println(gc.getTime());
        //        System.out.println(getSubchassisSlotFromTag("modular01:subchassis11:node02"));
        //
        //
        //
        //        String tagStr = "modular01:subchassis11:node01";
        //        int id = tagStr.lastIndexOf("node");
        //        System.out.println(id);
        //        String slotStr = tagStr.substring(id + "node".length());
        //        System.out.println(slotStr);
        //        int slot=Integer.valueOf(slotStr);
        //        System.out.println(slot);
        // Get the system date/time 
        //        Date date = new Date(); 
        // ��ӡ��������꣬�£��գ�Сʱ�����ӣ������Լ�ʱ��
        //        System.out.println(date.getTime()); 
        //        
        //        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy/MM/dd");
        //
        //        String dateStr=myFormatter.format(date);
        //        System.out.println(dateStr);

        //        int x=0;

        //        String a="";
        //        boolean flag=(a===a);
        //        System.out.println((x===x));
        //        System.out.println(Integer.MAX_VALUE);
        //        System.out.println(Integer.MIN_VALUE);
        //        LinkedList<String> waitingList = new LinkedList<String>();
        //        waitingList.add("a");
        //        System.out.println(waitingList);
        //        String tag = "modular:node11";
        //        int slotCmp = Integer.valueOf(tag.substring(tag.lastIndexOf("node") + 4));
        //        System.out.println(slotCmp);

        //        testLog();
        //        System.out.println("hi");
        //        try {
        //            testTryFinally();
        //        } catch (Exception e) {
        //            // TODO Auto-generated catch block
        //            e.printStackTrace();
        //        }

        //        Proxy.newProxyInstance(arg0, arg1, arg2)

        //        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        //        System.out.println(currencyFormatter.getMinimumIntegerDigits());
        //        System.out.println(currencyFormatter.getMaximumIntegerDigits());
        //
        //
        //
        //        List<String> textList = new ArrayList<String>();
        //        textList.add("a");
        //        Object [] objArray = (Object [])(Object [])textList.toArray();
        //        List tempvars = new Vector();
        //        for (int i = 0; i < objArray.length; i++) {
        //            if (objArray[i] != null)
        //                tempvars.add(objArray[i].toString());
        //
        //        }
        //        System.out.println(tempvars);

        //        String [] strArrays = (String [])textList.toArray();
        //        System.out.println(strArrays);
        //
        //        Object obj = textList.toArray(new String[0]);
        //        String [] strArrays = (String [])obj;
        //        System.out.println(strArrays);
        //
        //        String [] strArrays = (String [])textList.toArray(new String[0]);
        //        System.out.println(strArrays);
        //
        //
        //        String filenamePatternRegex = "[\\s\\S]*";
        //        Pattern pattern = Pattern.compile(filenamePatternRegex);
        //        System.out.println(pattern.matcher("fawega3rafwf.tgz").matches());

        //
        //
        //        String ip="a";
        //        System.out.println(constructLLA(ip));
        //        System.out.println(ip);
        //
        //true
        //        System.out.println(sun.net.util.IPAddressUtil.isIPv4LiteralAddress("0"));
        //        System.out.println(sun.net.util.IPAddressUtil.isIPv4LiteralAddress("234"));
        //
        //0
        //        System.out.println("".hashCode());
        //        System.out.println("".hashCode());
        //
        //
        //        String a = null;
        //        if (a instanceof String) {
        //            System.out.println("true");
        //        } else {
        //            System.out.println("false");
        //        }
        //
        //        List subvars = new ArrayList();
        //        subvars.add("txt");
        //        Object obj = (Object)(subvars.toArray());
        //        List list2 = (List)obj;

    }

}

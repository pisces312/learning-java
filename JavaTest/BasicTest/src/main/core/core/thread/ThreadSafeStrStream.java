package core.thread;

import java.io.PrintWriter;

public class ThreadSafeStrStream {

    public static void main(String[] args) {
        PrintWriter pw = new PrintWriter(System.out);
        pw.print("abc");
        pw.flush();
        pw.close();

    }

}

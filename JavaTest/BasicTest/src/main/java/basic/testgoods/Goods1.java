package basic.testgoods;


public class Goods1 {

    public DestinationInterface dest(String s) {
        /**
         * 局部内部类 　　是的，Java内部类也可以是局部的，它可以定义在一个方法甚至一个代码块之内。
         * 上面就是这样一个例子。在方法dest中我们定义了一个内部类，最后由这个方法返回这个内部类的对象
         * 。如果我们在用一个内部类的时候仅需要创建它的一个对象并创给外部，就可以这样做。当然，定义在方法中的内部类可以使设计多样化，用途绝不仅仅在这一点。
         */
        class GDestination implements DestinationInterface {

            private String label;

            private GDestination(String whereTo) {
                label = whereTo;
            }

            public String readLabel() {
                return label;
            }
        }
        return new GDestination(s);
    }

    public static void main(String[] args) {
        Goods1 g = new Goods1();
        DestinationInterface d = g.dest("Beijing");
    }
}

package chap8.execution.reuse;
// Old slot is not reused, GC roots still keep local variable table
public class LocalVarNotCollected2 {

    //-verbose:gc
    public static void main(String[] args) {
        {
            byte[] placeholder = new byte[64 * 1024 * 1024];
        }
        System.gc();
        //Still not collected
//        [GC (System.gc())  67532K->66176K(125952K), 0.0009342 secs]
//            [Full GC (System.gc())  66176K->66065K(125952K), 0.0060373 secs]

    }

}

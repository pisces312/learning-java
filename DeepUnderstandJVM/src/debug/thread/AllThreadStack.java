package debug.thread;

import java.util.Map;

public class AllThreadStack {
    public static void main(String[] args) {
        for (Map.Entry<Thread, StackTraceElement[]> stackTrace : Thread.getAllStackTraces()
            .entrySet()) {
            Thread thread = (Thread) stackTrace.getKey();
            StackTraceElement[] stack = (StackTraceElement[]) stackTrace.getValue();
            if (thread.equals(Thread.currentThread())) {
                continue;
            }
            System.out.print("\n�̣߳�" + thread.getName() + "\n");
            for (StackTraceElement element : stack) {
                System.out.print("\t" + element + "\n");
            }
        }
    }
}

package osgi;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

public class OSGiTest implements ServiceTrackerCustomizer, BundleActivator {
    BundleContext       bc;
    ServiceTracker      tracker;
    ServiceRegistration serviceRef;

    //    private static ManagedThreadPoolExecutor threadPoolExecutor;

    class MyService {
        public void longRunningOperaiton() {}

    }

    public void longRunningOperaiton() {

    }

    //    public void start(BundleContext bc) throws Exception {
    //        longRunningOperaiton();
    //    }
    public void start(BundleContext bc) throws Exception {
        this.bc = bc;
        ServiceReference sr = bc.getServiceReference(MyService.class.getName());
        this.tracker = new ServiceTracker(bc, sr, this);
        this.tracker.open();

        //        threadPoolExecutor.submit(new Runnable() {
        //
        //            @Override
        //            public void run() {
        //                longRunningOperaiton();
        //            }
        //        });
    }

    @Override
    public Object addingService(ServiceReference reference) {
        Object service = this.bc.getService(reference);
        if (service instanceof MyService) {
            final MyService myService = (MyService)service;
            //            myService.longRunningOperaiton();
            //            threadPoolExecutor.submit(new Runnable() {
            //
            //                @Override
            //                public void run() {
            //                    myService.longRunningOperaiton();
            //                }
            //            });
        }
        return service;
    }

    public static void main(String [] args) {
        long a = (Long)null;
        //        System.out.println("---------");
        //        System.out.println(a);
        //        System.out.println("---------");
        //        Long b = null;
        //        System.out.println("---------");
        //        System.out.println(b);
        //        System.out.println("---------");
    }

    @Override
    public void modifiedService(ServiceReference reference, Object service) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removedService(ServiceReference reference, Object service) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if (this.tracker != null) {
            this.tracker.close();
            this.tracker = null;
        }
        if (this.serviceRef != null) {
            this.serviceRef.unregister();
            this.serviceRef = null;
        }
    }

}

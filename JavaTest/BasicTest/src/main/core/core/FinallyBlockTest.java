package core;

public class FinallyBlockTest {
    public static boolean test() {
        try {
            System.out.println("try block");
            return true;
        } catch (Exception ex) {
            return false;
        } finally {
            System.out.println("finally block");
        }

    }

    public static void main(String[] args) {
        test();
    }
}

package spring4.aop.aspectj.sample;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.DeclareParents;

/**
 * Advice can be declared with pointcut
 * 
 * Annotation stype by AspectJ
 *
 */
@Aspect
public class AspectJAdvice {
    //For Introduction
    //
    //"SayHello" will implement "Ok" interface using "Ok"'s implementation class "IntroductionOk"
    //"Ok" is the interface and declared as a member of aspect class
    //"IntroductionOk" is an implementation of it
    //
    //The value attribute is an AspectJ type pattern.
    //any bean of a matching type will implement the declared interface.
    @DeclareParents(value = "spring4.aop.aspectj.sample.SayHello", defaultImpl = IntroductionOk.class)
    public Ok ok;

    @After(value = "execution(* spring4.aop.aspectj.sample.Hello.sayHelloAfter(..))")
    public void afterAdvice(JoinPoint joinPoint) {
        System.out.println("After: " + joinPoint.getSignature()
            .getName());
    }

    @AfterReturning(value = "execution(* spring4.aop.aspectj.sample.Hello.sayHelloAfterReturning(..))", returning = "retVal")
    public void afterReturningAdvice(JoinPoint joinPoint, String retVal) {
        System.out.println("AfterReturning: " + joinPoint.getSignature()
            .getName());
        System.out.println("Return Value: " + retVal);
    }

    @AfterThrowing(value = "execution(* spring4.aop.aspectj.sample.Hello.sayHelloAfterThrowing(..))", throwing = "e")
    public void afterThrowingAdvice(JoinPoint joinPoint, Exception e) {
        System.out.println("AfterThrowing: " + joinPoint.getSignature()
            .getName());
        System.out.println("Exception Message: " + e.getMessage());
    }

    @Around(value = "execution(* spring4.aop.aspectj.sample.Hello.sayHelloAround(..))")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("Around: " + joinPoint.getSignature()
            .getName());
        System.out.println("Before");
        //Trigger method
        Object obj = joinPoint.proceed();
        System.out.println("End");
        return obj;
    }

    @Before(value = "execution(* spring4.aop.aspectj.sample.Hello.sayHelloBefore(..))")
    public void beforeAdvice(JoinPoint joinPoint) {
        System.out.println("Before: " + joinPoint.getSignature()
            .getName());
    }

}

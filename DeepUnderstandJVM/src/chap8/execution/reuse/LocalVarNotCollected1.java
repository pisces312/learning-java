package chap8.execution.reuse;
// reference is hold
public class LocalVarNotCollected1 {

    //-verbose:gc
    public static void main(String[] args) {
        byte[] placeholder = new byte[64 * 1024 * 1024];
        System.gc();
        //Since placeholder is local var, in the same scope, it will
        //not be collected
//        [GC (System.gc())  67532K->66208K(125952K), 0.0009629 secs]
//            [Full GC (System.gc())  66208K->66065K(125952K), 0.0060492 secs]
    }

}

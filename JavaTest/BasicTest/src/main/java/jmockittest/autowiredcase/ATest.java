package jmockittest.autowiredcase;

import org.junit.Test;
import org.junit.runner.RunWith;

import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class ATest {
    @Tested
    A a;
    @Injectable
    B b;

    @Test
    public void testMethod() {
        a.method();
//        b.callMethodInB();
        

        new Verifications() {
            {
                b.callMethodInB();
            }
        };
    }
}

package spring.test.web;

import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;


public class RestClientMockTest {
    public static class Hotel {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    @Test
    public void test() {
        RestTemplate restTemplate = new RestTemplate();
        MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);

        mockServer
            .expect(requestTo("/hotels/42"))
            .andExpect(method(HttpMethod.GET))
            .andRespond(
                withSuccess("{ \"id\" : \"42\", \"name\" : \"Holiday Inn\"}",
                    MediaType.APPLICATION_JSON));

        Hotel hotel = restTemplate.getForObject("/hotels/{id}", Hotel.class, 42);
        // Use the hotel instance...

        mockServer.verify();
    }
}

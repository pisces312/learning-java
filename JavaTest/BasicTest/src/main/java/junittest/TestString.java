package junittest;
/**
 *StringBuilder 的速度最快
基于使用StringUtils.join需导入第三方包，使用StringBuilder可能有线程安全的问题，实际使用的，我一般首选StringBuffer
总结：
用+的方式效率最差，concat由于是内部机制实现，比+的方式好了不少。
Join 和 StringBuffer，相差不大，Join方式要快些，可见这种JavaScript中快速拼接字符串的方式在Java中也非常适用。
StringBuilder 的速度最快，但其有线程安全的问题，而且只有JDK5支持。
 *
 * times=20000
2009-2-6 13:13:09 TestString testPlus
信息: + cost {15344}ms
2009-2-6 13:13:11 TestString testConcat
信息: concat cost {2140}ms
2009-2-6 13:13:11 TestString testJoin
信息: StringUtils.join cost {47}ms
2009-2-6 13:13:11 TestString testStringBuffer
信息: StringBuffer cost {16}ms
2009-2-6 13:13:11 TestString testStringBuilder
信息: StringBuilder cost {0}ms

 * @author DELL
 */
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

public class TestString {

    private final Logger logger = Logger.getLogger(this.getClass().getName());
    int times = 10000;

    @Test
    public void testPlus() {
        String s = "";
        long ts = System.currentTimeMillis();
        for (int i = 0; i < times; i++) {
            s = s + String.valueOf(i);
        }
        long te = System.currentTimeMillis();
        logger.info("+ cost {" + (te - ts) + "}ms");
    }

    @Test
    public void testConcat() {
        String s = "";
        long ts = System.currentTimeMillis();
        for (int i = 0; i < times; i++) {
            s = s.concat(String.valueOf(i));
        }
        long te = System.currentTimeMillis();
        logger.info("concat cost {" + (te - ts) + "}ms");
    }

    @Test
    public void testJoin() {
        List<String> list = new ArrayList<String>();
        long ts = System.currentTimeMillis();
        for (int i = 0; i < times; i++) {
            list.add(String.valueOf(i));
        }
        StringUtils.join(list, "");
        long te = System.currentTimeMillis();
        logger.info("StringUtils.join cost {" + (te - ts) + "}ms");
    }

    @Test
    public void testStringBuffer() {
        StringBuffer sb = new StringBuffer();
        long ts = System.currentTimeMillis();
        for (int i = 0; i < times; i++) {
            sb.append(String.valueOf(i));
        }
        sb.toString();
        long te = System.currentTimeMillis();
        logger.info("StringBuffer cost {" + (te - ts) + "}ms");
    }

    @Test
    public void testStringBuilder() {
        StringBuilder sb = new StringBuilder();
        long ts = System.currentTimeMillis();
        for (int i = 0; i < times; i++) {
            sb.append(String.valueOf(i));
        }
        sb.toString();
        long te = System.currentTimeMillis();
        logger.info("StringBuilder cost {" + (te - ts) + "}ms");
    }
}



package com.openv.spring3x.ioc.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.openv.spring3x.ioc.IBankSecurityService;
/**
 * For Java EE
 *
 */
public class ApplicationContextDemo {

	public static void main(String[] args) {
		// 从classpath路径上装载XML配置信息
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"ioc_applicationcontext.xml");

		// 获得受管Bean
		IBankSecurityService bankSecurityService = applicationContext
				.getBean(IBankSecurityService.class);

		bankSecurityService.bankToSecurity(2000.00);
		bankSecurityService.securityToBank(2000.00);
	}

}

package core.hotspot.sa;

import sun.jvm.hotspot.gc_implementation.parallelScavenge.PSYoungGen;
import sun.jvm.hotspot.gc_implementation.parallelScavenge.ParallelScavengeHeap;
import sun.jvm.hotspot.gc_interface.CollectedHeap;
import sun.jvm.hotspot.memory.GenCollectedHeap;
import sun.jvm.hotspot.memory.Generation;
import sun.jvm.hotspot.memory.Universe;
import sun.jvm.hotspot.oops.ObjectHeap;
import sun.jvm.hotspot.runtime.VM;
import sun.jvm.hotspot.tools.Tool;

public class PrintHeapInfo extends Tool {
    public static void main(String[] args) {
        PrintHeapInfo test = new PrintHeapInfo();
        //
        //from specified pid
        int pid = 1020;
        test.execute(new String[] {String.valueOf(pid)});

        //from parameter
//        test.execute(args);

    }

    @Override
    public void run() {
        VM vm = VM.getVM();
        Universe universe = vm.getUniverse();
        CollectedHeap heap = universe.heap();
        System.out.println("GC heap name: " + heap.kind());
        System.out.println(heap.getClass().getName());
        //
        if (heap instanceof ParallelScavengeHeap) {
            ParallelScavengeHeap psHeap = (ParallelScavengeHeap) heap;

            PSYoungGen yGen = psHeap.youngGen();
            yGen.printOn(System.out);

            //MutableSpace edenSpace = yGen.edenSpace();
            //ystem.out.println("Eden space: [" + edenSpace.bottom() + ", " + edenSpace.end() + "]");


//            PSPermGen perm = psHeap.permGen();
//            MutableSpace permObjSpace = perm.objectSpace();
//            puts("Perm gen: [" + permObjSpace.bottom() + ", " + permObjSpace.end() + ")");
//            long permSize = 0;
//            for (VM.Flag f : VM.getVM().getCommandLineFlags()) {
//                if ("PermSize".equals(f.getName())) {
//                    permSize = Long.parseLong(f.getValue());
//                    break;
//                }
//            }
//            puts("PermSize: " + permSize);
        } else if (heap instanceof GenCollectedHeap) { //For SerialGC
            GenCollectedHeap serialHeap = (GenCollectedHeap) heap;
            for (int i = 0; i < serialHeap.nGens(); ++i) {
                Generation gen = serialHeap.getGen(i);
                gen.printOn(System.out);
            }

        }

        System.out.println();

        printObjectHeapDetailed(vm);

    }



    private static void printObjectHeapDetailed(VM vm) {
        ObjectHeap objHeap = vm.getObjectHeap();
        //Print lots of things
//        objHeap.print();

        //Or, print the same thing
//        HeapVisitor heapVisitor = new HeapPrinter(System.out);
//        objHeap.iterate(heapVisitor);
    }

}

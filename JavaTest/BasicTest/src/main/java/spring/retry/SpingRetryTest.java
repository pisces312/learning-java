package spring.retry;

import java.rmi.Naming;
import java.rmi.Remote;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.retry.RecoveryCallback;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;


// @SpringBootApplication
public class SpingRetryTest {

    @Test
    public void testTimeoutPolicy() {
        RetryTemplate template = new RetryTemplate();

        int retryTimes = 3;
        Map<Class<? extends Throwable>, Boolean> exceptionMaps =
            new HashMap<Class<? extends Throwable>, Boolean>();
        template.setRetryPolicy(new SimpleRetryPolicy(retryTimes, exceptionMaps));


        // If that call fails then it is retried until a timeout is reached
        // TimeoutRetryPolicy timeoutPolicy = new TimeoutRetryPolicy();
        // timeoutPolicy.setTimeout(3000L);
        // template.setRetryPolicy(timeoutPolicy);

        // When retrying after a transient failure it often helps to wait a bit before trying again,
        // because usually the failure is caused by some problem that will only be resolved by
        // waiting.
        final FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(1000L);
        template.setBackOffPolicy(backOffPolicy);

        // First fail sleep 1s, then 2s, 4s...
        // final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        // backOffPolicy.setInitialInterval(1000L);
        // template.setBackOffPolicy(backOffPolicy);


        try {
            template.execute(new RetryCallback<Integer, Throwable>() {
                @Override
                public Integer doWithRetry(RetryContext retrycontext) throws Throwable {
                    System.out.println("doWithRetry");
                    throw new Exception("retry failed");
                }
            }, new RecoveryCallback<Integer>() { // All retry failed, it will be called

                    @Override
                    public Integer recover(RetryContext context) throws Exception {
                        System.out.println("try recover");
                        throw new Exception("recover failed");
                    }
                });
        } catch (Throwable e) {
            System.out.println(e.getMessage());
            // e.printStackTrace();
        }
    }

    @Test
    public void test() {
        int retryTimes = 3;
        final String rmiStr = "rmi://somehost:2106/MyApp";

        // create the retry template
        final RetryTemplate template = new RetryTemplate();

        Map<Class<? extends Throwable>, Boolean> exceptionMaps =
            new HashMap<Class<? extends Throwable>, Boolean>();
        template.setRetryPolicy(new SimpleRetryPolicy(retryTimes, exceptionMaps));


        // When retrying after a transient failure it often helps to wait a bit before trying again,
        // because usually the failure is caused by some problem that will only be resolved by
        // waiting.
        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(1000L);
        template.setBackOffPolicy(backOffPolicy);

        // execute the operation using the retry template
        try {
            template.execute(new RetryCallback<Remote, Throwable>() {
                @Override
                public Remote doWithRetry(final RetryContext context) throws Throwable {
                    System.out.println("retry");
                    return (Remote) Naming.lookup(rmiStr);
                }
            }, new RecoveryCallback<Remote>() {
                public Remote recover(RetryContext context) throws Exception {
                    // recover logic here
                    System.out.println("recover");
                    return null;
                }
            });
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



}

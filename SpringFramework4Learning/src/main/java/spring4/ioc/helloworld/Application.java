package spring4.ioc.helloworld;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
public class Application {

    //Define a bean
    @Bean
    MessageService mockMessageService() {
        return new MessageService() {
            public String getMessage() {
                return "Hello World!";
            }
        };
    }

    public static void main1() {
        //Create application context
        try (ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(Application.class);) {
            //Get a bean
            MessagePrinter printer = context.getBean(MessagePrinter.class);
            //Use the bean
            printer.printMessage();
        }
    }

    public static void main2() {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();) {
            context.register(Application.class);
            context.refresh();
            //Get a bean
            MessagePrinter printer = context.getBean(MessagePrinter.class);
            //Use the bean
            printer.printMessage();
        }
    }

    public static void main3() {
        String basePackage = Application.class.getPackage().getName();
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(basePackage);) {
            //Get a bean
            MessagePrinter printer = context.getBean(MessagePrinter.class);
            //Use the bean
            printer.printMessage();
        }
    }

    public static void main(String[] args) {
        //https://stackoverflow.com/questions/14184059/spring-applicationcontext-resource-leak-context-is-never-closed
        //      ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        //
        main1();

    }
}

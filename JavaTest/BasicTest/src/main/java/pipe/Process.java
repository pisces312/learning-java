/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pipe;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class Process implements Runnable {
    public enum State {

        UNKNOWN,
        LEADER;
    }

    static int countSend = 0;
    int uid;
    State state;
    PipedReader pr = new PipedReader();
    PipedWriter pw = new PipedWriter();
    Process nextProcess;

    public Process(int uid/*, Process nextProcess*/) {
        this.uid = uid;
        state = State.UNKNOWN;
//            this.nextProcess = nextProcess;
    }

    public void connect(Process p) {
        nextProcess = p;
        try {
            pw.connect(p.getPipedReader());
        } catch (IOException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PipedReader getPipedReader() {
        return pr;
    }

    @Override
    public String toString() {
        return String.valueOf(uid);
    }

    public void send(int id) {
        countSend++;
        try {
//                pw.
            System.out.println(uid + " send " + id + " to " + nextProcess);
            pw.write(id);
        } catch (IOException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public void run() {
        send(uid);
        try {
            while (true) {
                int v = pr.read();
                System.out.println(uid + " receives " + v);
                if (v == -1) {
                    break;
                } else if (v > uid) {
                    send(v);
                } else if (v == uid) {
                    state = State.LEADER;
                    System.out.println("leader is " + uid);
//                        send(-1);
                    break;
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //???只关闭管道一端是否可行
//                if (pr != null) {
//                    try {
//                        pr.close();
//                    } catch (IOException ex) {
//                        Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
            if (pw != null) {
                try {
                    pw.close();
                } catch (IOException ex) {
                    Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public int getCount(){
        return countSend;
    }
}

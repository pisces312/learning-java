package gziptest;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import jzlibtest.JzlibTest;


public class GzipTest {

    public static void main(String[] args) {
        try {
            byte[] b = JzlibTest.deflate("Hello world!".getBytes());
            String result = new String(GZIP.inflate(b));
            System.out.println(result);
        } catch (IOException ex) {
            Logger.getLogger(GzipTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

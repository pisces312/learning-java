package core;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BasicTest {

    public void testTypeOverflow() {
        short si = (short)32768;
        System.out.println(si);
        si = (short)32767;
        System.out.println(si);
    }

    public static void testCompare() {
        int r = Double.compare(0.001, 100.02);
        System.out.println(r);
    }

    public static void testTryCatch1() {
        List<String> list = new LinkedList<String>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");

        Iterator<String> itr = list.iterator();
        int i = 0;
        while (itr.hasNext()) {
            String s = itr.next();
            try {
                if (i == 2) {
                    throw new Exception();
                    //won't go to here
                }
                ++i;

            } catch (Exception e) {
                System.out.println("catch a exception");

            }
            System.out.println(s);
        }
    }

    class Base {

    }
    class Sub extends Base {

    }

    public static void testArrayCovariant() {

        //        Base[] b=new Base[1];
        //        Sub[] s=new Sub[1];
        //        System.out.println((Sub[]) instanceof (Base[]));
        //        System.out.println(Sub [].class.getSuperclass().getName());
        //        System.out.println(Base [].class.getName());
    }

    public static void testCheckedList() {
        //        List<?> list2=Arrays.asList("a","b","c");
        List<String> list = Arrays.asList("a", "b", "c");
        //Returns a dynamically typesafe view of the specified list.
        Collections.checkedList(list, String.class);

        //        Collections.checkedList(list2, String.class);
    }

    enum Animal {
    };

    //wrong
    //enum Dog extends Animal{}
    //
    //
/**
     * @param args
     */
    public static void main(String [] args) {
        String a=null;
        System.out.println(a);
//        int I=1;
//        assert( I-I == 0 );
//        assert( I---I == 1 );
//        assert( (I-----I) == 2 );
//        assert( I-------I == 3 );
        //	    testClassExtend();
        //	    testCompare();
        //        testArrayCovariant();
//        for (RoundingMode r : RoundingMode.values()) {
//            System.out.println(r);
//        }
        
//        String s;
//        System.out.println(s);
        

    }

}

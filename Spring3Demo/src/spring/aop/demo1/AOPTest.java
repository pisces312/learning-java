package spring.aop.demo1;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import spring.aop.demo1.services.AServiceImpl;
import spring.aop.demo1.services.BServiceImpl;

@SuppressWarnings("deprecation")
public class AOPTest extends AbstractDependencyInjectionSpringContextTests {

	private AServiceImpl aService;

	private BServiceImpl bService;

	protected String[] getConfigLocations() {
		String[] configs = new String[] { "aop_demo1.xml" };
		return configs;
	}

	/**
	 * ������������
	 */
	public void testCall() {
		System.out.println("SpringTest JUnit test");
		aService.fooA("JUnit test fooA");
		aService.barA();
		bService.fooB();
		bService.barB("JUnit test barB", 0);
	}

	/**
	 * ����After-Throwing
	 */
	public void testThrow() {
		try {
			bService.barB("JUnit call barB", 1);
		} catch (IllegalArgumentException e) {

		}
	}

	public void setAService(AServiceImpl service) {
		aService = service;
	}

	public void setBService(BServiceImpl service) {
		bService = service;
	}
}
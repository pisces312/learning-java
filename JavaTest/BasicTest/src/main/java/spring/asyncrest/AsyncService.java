package spring.asyncrest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

@Service
public class AsyncService {
    private static Logger logger = LoggerFactory.getLogger(AsyncService.class);
    private Map<String, Future<String>> futureMap;

    private Map<String, DeferredResult<String>> deferredResultMap;

    public AsyncService() {
        futureMap = new HashMap<>();
        deferredResultMap = new HashMap<>();
    }

    public void add(String jobId, Future<String> future) {
        futureMap.put(jobId, future);
    }

    public DeferredResult<String> getAsyncUpdate(String jobId) {
        return deferredResultMap.remove(jobId);
    }



    @Scheduled(fixedDelay = 1000)
    public void refresh() {
        logger.info("refresh");
        Iterator<Map.Entry<String, Future<String>>> itr = futureMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, Future<String>> entry = itr.next();
            String jobId = entry.getKey();
            Future<String> future = entry.getValue();
            if (future.isDone()) {
                DeferredResult<String> result = new DeferredResult<>();
                try {
                    String str = future.get();
                    logger.info("Get result:" + str);
                    result.setResult(str);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                deferredResultMap.put(jobId, result);
                itr.remove();
            }
        }
    }


}

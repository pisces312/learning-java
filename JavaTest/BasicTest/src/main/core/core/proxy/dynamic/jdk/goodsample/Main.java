package core.proxy.dynamic.jdk.goodsample;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Main {

    public static Subject getInstance() {
        //!Usually, we must have a real subject in actual scenario.
        Subject delegate = new RealSubject();

        //Use ctor to pass real subject
        InvocationHandler handler = new SubjectInvocationHandler(delegate);

        //!Use both delegat's class loader and interface
        //!so it will make sure the same method is provided
        return (Subject) Proxy.newProxyInstance(delegate.getClass().getClassLoader(),
            delegate.getClass().getInterfaces(), handler);
    }

    public static void main(String[] args) {
        Subject proxy = getInstance();
        proxy.dealTask("DBQueryTask");
    }

}

package $.nili.tools;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.io.CharStreams;

public final class JsonUtil {
    private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String toJson(Object src){
        Preconditions.checkNotNull(src, "Source can't be null");
        try {
            return mapper.writeValueAsString(src);
        } catch (Exception e) {
            logger.error("Failed to generate json: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJson(String json, Class<T> _class) {
        Preconditions.checkArgument(StringUtils.isNotBlank(json), "json can't be empty");
        try {
            return mapper.readValue(json, _class);
        } catch (IOException e) {
            logger.error("Failed to parse json: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
    
    public static <T> T fromJsonFile(String fileName, Class<T> _class){        
        String json;
        try {
            json = CharStreams.toString(new FileReader(new File(fileName)));
        } catch (Exception e) {
            logger.error("Failed to read json file: " + e.getMessage());
            throw new RuntimeException(e);
        }
        return fromJson(json, _class);
    }
}

package core.thread.producer_consumer;

import java.util.PriorityQueue;

public class WaitNotifyMain {
    private int queueSize = 10;
    private PriorityQueue<Integer> queue = new PriorityQueue<Integer>(queueSize);

    public static void main(String[] args) {
        WaitNotifyMain test = new WaitNotifyMain();
        Producer producer = test.new Producer();
        Consumer consumer = test.new Consumer();

        producer.start();
        consumer.start();
    }

    class Consumer extends Thread {

        @Override
        public void run() {
            consume();
        }

        private void consume() {
            while (true)
                synchronized (queue) {
                    try {
                        while (queue.size() == 0) {
                            System.out.println("队列空，等待数据");
                            queue.wait();
                        }
                        queue.poll(); // 每次移走队首元素
                        System.out.println("从队列取走一个元素，队列剩余" + queue.size() + "个元素");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        queue.notifyAll();
                    }
                }
        }
    }

    class Producer extends Thread {

        @Override
        public void run() {
            produce();
        }

        private void produce() {
            while (true)
                synchronized (queue) {
                    try {
                        while (queue.size() == queueSize) {
                            System.out.println("队列满，等待有空余空间");
                            queue.wait();
                        }
                        queue.offer(1); // 每次插入一个元素
                        System.out.println("向队列取中插入一个元素，队列剩余空间：" + (queueSize - queue.size()));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        queue.notifyAll();
                    }
                }
        }
        // private void produce() {
        // while (true) {
        // synchronized (queue) {
        // while (queue.size() == queueSize) {
        // try {
        // System.out.println("队列满，等待有空余空间");
        // queue.wait();
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // queue.notifyAll();
        // }
        // }
        // // Sleep a while for simulating producing
        // // try {
        // // Thread.sleep(1000);
        // // } catch (InterruptedException e) {
        // // e.printStackTrace();
        // // }
        // queue.offer(1); // 每次插入一个元素
        // queue.notifyAll();
        // System.out.println("向队列取中插入一个元素，队列剩余空间：" + (queueSize - queue.size()));
        // }
        // }
        // }
    }
}

package core.hotswap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HotswapTest {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private static class TestClass {
        public void func() {
            //You can modify any thing here
            System.out.println("test2");
        }
    }

    //Can not modify static method
    public static void main(String[] args) throws IOException {
        TestClass test = new TestClass();
        for (int i = 0; i < 10; i++) {
            //Wait user input to continue
            System.out.println("Please press any key to continue");
            reader.readLine();
            //Method to modify dynamically
            test.func();
        }
    }
}


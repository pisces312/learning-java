package core.java18;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import org.junit.Test;

public class InterfaceDefaultMethodTest {
  public interface CurrencyConverter {
    BigDecimal convert(Currency from, Currency to, BigDecimal amount);

    //!! declare a default method
    default List<BigDecimal> convert(Currency from, Currency to, List<BigDecimal> amounts) {
      List<BigDecimal> result = new ArrayList<BigDecimal>();
      for (BigDecimal amount : amounts) {
        result.add(convert(from, to, amount));
      }
      return result;
    }
    
    public static void info(){
        System.out.println("static method for interface");
    }
  }

  public static final class CurrencyConverterImpl implements CurrencyConverter {

    @Override
    public BigDecimal convert(Currency from, Currency to, BigDecimal amount) {
      return amount.multiply(new BigDecimal(6.3));
    }
    //Not implement another interface

  }

  @Test
  public void testDefaultMethod() {
    BigDecimal amount = new BigDecimal(3);
    CurrencyConverter cc = new CurrencyConverterImpl();
    BigDecimal result =
        cc.convert(Currency.getInstance(Locale.CHINA), Currency.getInstance(Locale.US), amount);
    System.out.println(result);
    
    List<BigDecimal> amounts=Arrays.asList(new BigDecimal[]{new BigDecimal(1),new BigDecimal(2),new BigDecimal(3)});
    List<BigDecimal> results=cc.convert(Currency.getInstance(Locale.CHINA), Currency.getInstance(Locale.US), amounts);
    System.out.println(results);
  }
}

package core;

import $.nili.tools.CLIGetOpts;

import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Locale;

public class TestRefletion {

    public class Circle {
        int    x;
        int    y;
        double r;

        public Circle(Integer x, Integer y, Double r) throws Exception {
            this.x = x;
            this.y = y;
            this.r = r;
            if (r <= 0) {
                throw new Exception("radius can't be lower than 0");
            }
        }

        //        public Circle(int x, int y, double r) {
        //            this.x = x;
        //            this.y = y;
        //            this.r = r;
        //        }
        @Override
        public String toString() {
            return x + "," + y + "," + r;
        }
    }

    public static void test1() {
        try {
            Class<?> c = TestRefletion.Circle.class;

            Constructor<?> constructors[] = c.getConstructors();
            System.out.println(constructors.length);
            for (Constructor<?> ctr : constructors) {
                System.out.println(ctr);
                Class<?> [] classes = ctr.getParameterTypes();
                for (Class<?> c2 : classes) {
                    System.out.println(c2);
                }
            }
            //
            //!!!!!!!
            //special for inner class, it must pass the parent class
            Class<?> [] paramtersList = {TestRefletion.class, Integer.class, Integer.class, Double.class};
            //            for (Class<?> c2 : paramtersList) {
            //                System.out.println(c2);
            //            }
            Constructor<?> constructor = c.getConstructor(paramtersList);
            Object object = constructor.newInstance(new TestRefletion(), Integer.valueOf(1), Integer.valueOf(2),
                    Double.valueOf(3.5));
            if (object != null) {
                System.out.println(object);
            }
            try {
                object = constructor.newInstance(new TestRefletion(), Integer.valueOf(1), Integer.valueOf(2),
                        Double.valueOf(-3.5));
                if (object != null) {
                    System.out.println(object);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int evttest(Locale locale, String [] args, String [] fileBuffers, Writer out, Writer err, Reader stdin)
            throws Exception {
        String [] newArgs = new String[args.length + 2];
        for (int i = 0; i < args.length; ++i) {
            newArgs[i] = args[i];
        }
        newArgs[newArgs.length - 2] = "-v";
        newArgs[newArgs.length - 1] = "testEvent";
        return apetest(locale, newArgs, fileBuffers, out, err, stdin);

    }

    public int apetest(Locale locale, String [] args, String [] fileBuffers, Writer out, Writer err, Reader stdin)
            throws Exception {
        try {
            String [] usage = {"v|verify="};

            //////////////////////////////////
            CLIGetOpts opts = new CLIGetOpts(usage);
            opts.parseCommandLine(args);
            if (!opts.argumentGiven("v")) {
                out.write("Need to specify which method to test\n");
                return -1;
            }
            if (opts.argumentGiven("v")) {
                String methodName = opts.argumentValue("v").trim();
                out.write("Run <" + methodName + ">\n");
                out.flush();
                Class<?> c = Class.forName(this.getClass().getName());
                Class<?> ptypes[] = new Class[] {opts.getClass(), Writer.class};
                Method m = c.getMethod(methodName, ptypes);
                Object obj = (Object)c.newInstance();
                Object arg[] = new Object[] {opts, out};
                Object r = m.invoke(obj, arg);
                out.write(r + "\n");
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
//
//    public static int testEvent(CLIGetOpts opts, Writer out) {
//        System.out.println("testEvent");
//        return 0;
//
//    }

    public static void main(String [] args) {
        PrintWriter pw = new PrintWriter(System.out);

        //        args=new String[]{"-v evttest"};
        try {
            new TestRefletion().evttest(null, args, null, pw, pw, null);
            //
            //            args = new String[] {"-v testEvent"};
            //            new TestRefletion().apetest(null, args, null, pw, pw, null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}

package spring4.ioc.demo.common;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BankSecurityServiceImpl implements IBankSecurityService {
    private static final Log log = LogFactory.getLog(BankSecurityServiceImpl.class);
    // DI
    @Autowired
    private IBankSecurityDao bankSecurityDao;

    private Properties properties;

    public BankSecurityServiceImpl() {
        log.info("ctr BankSecurityServiceImpl()");
    }

    public BankSecurityServiceImpl(IBankSecurityDao bankSecurityDao) {
        this.bankSecurityDao = bankSecurityDao;
        log.info("ctr BankSecurityServiceImpl(IBankSecurityDao)");
    }

    public BankSecurityServiceImpl(IBankSecurityDao bankSecurityDao, Properties properties) {
        this.bankSecurityDao = bankSecurityDao;
        this.properties = properties;
        log.info("ctr BankSecurityServiceImpl(IBankSecurityDao,Properties)");
    }

    public void setBankSecurityDao(IBankSecurityDao bankSecurityDao) {
        this.bankSecurityDao = bankSecurityDao;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public void bankToSecurity(Double money) {
        this.bankSecurityDao.bankToSecurity(money);
    }

    public void securityToBank(Double money) {
        this.bankSecurityDao.securityToBank(money);
    }

}

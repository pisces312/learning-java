package core.thread;

public class WaitNotifyTest {
    private volatile boolean go = false;

    public static void main(String args[]) throws InterruptedException {
        //As monitor
        final WaitNotifyTest test = new WaitNotifyTest();

        Runnable waitTask = new Runnable() {

            @Override
            public void run() {
                try {
                    test.shouldGo();
                    System.out.println(Thread.currentThread() + " finished Execution");
                } catch (InterruptedException e) {
                    System.out.println(Thread.currentThread() + " is not woken up and interrupted");
                }
                
            }
        };

        Runnable notifyTask = new Runnable() {

            @Override
            public void run() {
                test.go();
                System.out.println(Thread.currentThread() + " finished Execution");
            }
        };

        Thread t1 = new Thread(waitTask, "WT1"); //will wait
        Thread t2 = new Thread(waitTask, "WT2"); //will wait
        Thread t3 = new Thread(waitTask, "WT3"); //will wait
        Thread t4 = new Thread(notifyTask, "NT1"); //will notify

        //starting all waiting thread
        t1.start();
        t2.start();
        t3.start();

        //pause to ensure all waiting thread started successfully
        Thread.sleep(1000);

        //starting notifying thread
        t4.start();

        //Wait to notify all in while loop
        Thread.sleep(1000);

        //Stop all threads if only notify() is called
        t1.interrupt();
        t2.interrupt();
        t3.interrupt();
    }

    /*
     * wait and notify can only be called from synchronized method or bock
     */
    private synchronized void shouldGo() throws InterruptedException {

        while (go != true) {
            System.out.println(Thread.currentThread() + " is going to wait on this object");

            wait();
            //release lock and reacquires on wakeup
            System.out.println(Thread.currentThread() + " is woken up");
        }

        //Comment it, all thread will exit, otherwise some thread cannot exit
      //resetting condition, will cause other thread wait again
        //        go = false; 
    }

    /*
     * both shouldGo() and go() are locked on current object referenced by "this" keyword
     */
    private synchronized void go() {
        while (go == false) {
            System.out.println(Thread.currentThread() + " is going to notify all or one thread waiting on this object");

            go = true; //making condition true for waiting thread
            //!!!
//            notify(); // only one out of three waiting thread WT1, WT2,WT3 will woke up
                        notifyAll(); // all waiting thread  WT1, WT2,WT3 will woke up
        }

        //        System.out.println("");

    }


}

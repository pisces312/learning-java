package core.methodhandle.invokedynamic;

import java.io.IOException;
import java.lang.invoke.CallSite;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import core.methodhandle.WriteClassUtils;

/**
 * Only need to compile once
 *
 */
//javap -v ToUpperCaseMain.class
//
//{
//    public static void main(java.lang.String[]);
//      descriptor: ([Ljava/lang/String;)V
//      flags: ACC_PUBLIC, ACC_STATIC
//      Code:
//        stack=2, locals=1, args_size=1
//           0: getstatic     #12                 // Field java/lang/System.out:Ljava/io/PrintStream;
//           3: invokedynamic #25,  0             // InvokeDynamic #0:toUpperCase:()Ljava/lang/String;
//           8: invokevirtual #31                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
//          11: return
//  }
//  BootstrapMethods:
//    0: #19 invokestatic core/methodhandle/invokedynamic/ToUpperCase.bootstrap:(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/String;)Ljava/lang/invoke/CallSite;
//      Method arguments:
//        #21 Hello
//
//
//java -cp . ToUpperCaseMain
//HELLO

public final class ToUpperCaseGenerator {

    /*   
     * Create bootstrap method handle
     * 
     * ToUpperCase.bootstrap()
     * 
     * */
    public static Handle createBootstrapHandle() {
        //bootstrap class
        Class<?> bsmClass = ToUpperCase.class;
        //bootstrap name
        String bsmName = "bootstrap";
        //bootstrap method type
        //
        MethodType bsmType = MethodType.methodType(CallSite.class, Lookup.class, String.class,
            MethodType.class, String.class);
        Handle BSM = new Handle(Opcodes.H_INVOKESTATIC, bsmClass.getName().replace('.', '/'),
            bsmName, bsmType.toMethodDescriptorString(), bsmClass.isInterface());
        return BSM;
    }

    public static String getMethodDesc() {
        MethodType mt=MethodType.methodType(String.class);
        return mt.toMethodDescriptorString();
    }
    /**
     * Create class content
     * 
     * @param BSM
     * @param targetClassName
     * @return
     */
    public static ClassWriter createClass(Handle BSM, String targetClassName) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC | Opcodes.ACC_SUPER, targetClassName, null,
            "java/lang/Object", null);

        //Create a static 'main(String[])' method
        MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC, "main",
            "([Ljava/lang/String;)V", null, null);
        mv.visitCode();
        //
        mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        //
        //invokedynamic
        //BSM is the MethodHandle of bootstrap method
        //When invokedynamic bytecode opr is executed
        //1) Call bootstrap method
        //2) Return ConstantCallSite from bootstrap method
        //3) Get MethodHandle from ConstantCallSite.getTarget
        //4) Invoke MethodHandle
        //The last var args of visitInvokeDynamicInsn ("Hello") will be passed as the last arg of bootstrap
        //The return of MethodHandle is to convert "Hello" to upper case "HELLO"
        String methodName = "toUpperCaseMethod";
        String methodArg = "Hello";
        mv.visitInvokeDynamicInsn(methodName, getMethodDesc(), BSM, methodArg);
        //
        //
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println",
            "(Ljava/lang/String;)V", false);
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd(); //the last instruction of method

        //
        cw.visitEnd();
        return cw;
    }
    public static void main(String[] args) throws IOException {
        String targetClassName = "ToUpperCaseMain";
        Handle BSM = ToUpperCaseGenerator.createBootstrapHandle();
        ClassWriter cw = ToUpperCaseGenerator.createClass(BSM, targetClassName);

//for javap
        WriteClassUtils.writeToFile(targetClassName, cw.toByteArray());

    }



}

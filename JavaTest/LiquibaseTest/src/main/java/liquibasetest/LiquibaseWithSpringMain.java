package liquibasetest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import liquibase.integration.spring.SpringLiquibase;


@SpringBootApplication
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"liquibasetest"})

@Import(value = LiquibaseTestConfig.class)
public class LiquibaseWithSpringMain implements CommandLineRunner {
    @Autowired
    private DataSource dataSource;

    @Value("${db_table_name}")
    private String tableName;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(LiquibaseWithSpringMain.class);
        app.setWebEnvironment(false);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        try (Connection connection = dataSource.getConnection();) {
            try (Statement statement = connection.createStatement();) {
                statement.setQueryTimeout(30); // set timeout to 30 sec.
                try (ResultSet rs = statement.executeQuery("select * from " + tableName);) {
                    while (rs.next()) {
                        System.out.println("name = " + rs.getString("name"));
                        System.out.println("id = " + rs.getInt("id"));
                    }
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}

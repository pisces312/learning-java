package beancopy;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;



public class TestMain {
    public static void main(String[] args) {
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        for (Logger log : lc.getLoggerList()) {
            log.setLevel(Level.INFO);
        }

        FromBean fb = new FromBean();
        fb.setAddress("北京市朝阳区大屯路");
        fb.setAge(20);
        fb.setMoney(30000.111);
        fb.setIdno("110330219879208733");
        fb.setName("测试");

        IMethodCallBack apacheBeanutilCB = new IMethodCallBack() {

            @Override
            public String getMethodName() {
                return "org.apache.commons.beanutils.BeanUtil.copyProperties";
            }

            @Override
            public ToBean callMethod(FromBean frombean) throws Exception {

                ToBean toBean = new ToBean();
                org.apache.commons.beanutils.BeanUtils.copyProperties(toBean, frombean);

                //                org.apache.commons.beanutils.BeanUtils.
                return toBean;
            }
        };

        IMethodCallBack apachePropertyCB = new IMethodCallBack() {

            @Override
            public String getMethodName() {
                return "org.apache.commons.beanutils.PropertyUtils.copyProperties";
            }

            @Override
            public ToBean callMethod(FromBean frombean) throws Exception {
                ToBean toBean = new ToBean();
                org.apache.commons.beanutils.PropertyUtils.copyProperties(toBean, frombean);

                //                org.apache.commons.beanutils.PropertyUtils.setMappedProperty(bean, name, key, value);
                return toBean;
            }
        };

        IMethodCallBack springCB = new IMethodCallBack() {

            @Override
            public String getMethodName() {
                return "org.springframework.beans.BeanUtils.copyProperties";
            }

            @Override
            public ToBean callMethod(FromBean frombean) throws Exception {
                ToBean toBean = new ToBean();
                org.springframework.beans.BeanUtils.copyProperties(frombean, toBean);
                return toBean;
            }
        };

        IMethodCallBack cglibCB = new IMethodCallBack() {
            org.springframework.cglib.beans.BeanCopier bc =
                org.springframework.cglib.beans.BeanCopier.create(FromBean.class, ToBean.class,
                    false);

            @Override
            public String getMethodName() {
                return "BeanCopier.create";
            }

            @Override
            public ToBean callMethod(FromBean frombean) throws Exception {
                ToBean toBean = new ToBean();
                
                //                bc.
                bc.copy(frombean, toBean, null);
                return toBean;
            }
        };

        // 数量较少的时候，测试性能
        //        BenchmarkTest bt = new BenchmarkTest(100);
        //        bt.benchmark(apacheBeanutilCB, fb);
        //        bt.benchmark(apachePropertyCB, fb);
        //        bt.benchmark(springCB, fb);
        //        bt.benchmark(cglibCB, fb);

        // // 测试一万次性能测试
        BenchmarkTest bt10000 = new BenchmarkTest(10000);
        bt10000.benchmark(apacheBeanutilCB, fb);
        bt10000.benchmark(apachePropertyCB, fb);
        bt10000.benchmark(springCB, fb);
        bt10000.benchmark(cglibCB, fb);
        //
        // // 担心因为顺序问题影响测试结果
        // BenchmarkTest bt1000R = new BenchmarkTest(10000);
        // bt1000R.benchmark(cglibCB, fb);
        // bt1000R.benchmark(springCB, fb);
        // bt1000R.benchmark(apachePropertyCB, fb);
        // bt1000R.benchmark(apacheBeanutilCB, fb);

    }

}

package encryption;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

/**
 * 
 * https://stackoverflow.com/questions/15554296/simple-java-aes-encrypt-decrypt-example
 * 
 * 
 * @author nil4
 *
 */
public class AESTest {

    //Good
    @Test
    public void testEncrypt() {
        try {
            String s = "Hello there. How are you? Have a nice day.";

            //!! Create a random initialization vector
            //16 bytes IV, can use str.getBytes()
            //! Must use same iv for both encryption and decryption
            SecureRandom random = new SecureRandom();
            byte[] randBytes = new byte[16];
            random.nextBytes(randBytes);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(randBytes);

            // Generate AES key
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            //AES key length, by default, 128bits
            kgen.init(128);
            SecretKey aesKey = kgen.generateKey();
            System.out.println("AES alg:"+aesKey.getAlgorithm());
            
            
            byte[] aesKeyBytes=aesKey.getEncoded();
            System.out.println("random aes key ("+aesKeyBytes.length+")");
            for(byte b:aesKeyBytes) {
                System.out.print(Integer.toHexString(((int)b)&0xff));
//                System.out.println(Integer.toBinaryString(b));
            }
            System.out.println();
            

            // Encrypt cipher
            Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            encryptCipher.init(Cipher.ENCRYPT_MODE, aesKey, ivParameterSpec);



            // Encrypt
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, encryptCipher);
            //string to encrypt
            cipherOutputStream.write(s.getBytes());
            cipherOutputStream.flush();
            cipherOutputStream.close();



            byte[] encryptedBytes = outputStream.toByteArray();

            // Decrypt cipher
            Cipher decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            decryptCipher.init(Cipher.DECRYPT_MODE, aesKey, ivParameterSpec);

            // Decrypt
            outputStream = new ByteArrayOutputStream();
            ByteArrayInputStream inStream = new ByteArrayInputStream(encryptedBytes);
            try (CipherInputStream cipherInputStream = new CipherInputStream(inStream, decryptCipher);) {
                byte[] buf = new byte[1024];
                int bytesRead;
                while ((bytesRead = cipherInputStream.read(buf)) >= 0) {
                    outputStream.write(buf, 0, bytesRead);
                }

                System.out.println("Result: " + new String(outputStream.toByteArray()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //String initVector = "RandomInitVector"; // 16 bytes IV
    //    private static String IV_PARAM = "YWVzaXZwYXJhbQ==";


    /**
     * good
     * 
     * Returns a {@link Base64} encoded encrypted string.
     *
     * @param string The string to encrypt.
     * @param key The key to use for encryption.
     * @return The encrypted encoded string.
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static String encryptWithKey(String string, String key) throws Exception {
        String encryptedString;



        //!! Create a random initialization vector
        SecureRandom random = new SecureRandom();
        byte[] randBytes = new byte[16];
        random.nextBytes(randBytes);
        IvParameterSpec iv = new IvParameterSpec(randBytes);

        SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        //use iv to initialize
        cipher.init(Cipher.ENCRYPT_MODE, skey, iv);

        byte[] ivBytes = iv.getIV();
        byte[] inputBytes = string.getBytes();
        byte[] plaintext = new byte[ivBytes.length + inputBytes.length];

        System.arraycopy(ivBytes, 0, plaintext, 0, ivBytes.length);
        System.arraycopy(inputBytes, 0, plaintext, ivBytes.length, inputBytes.length);

        byte[] encryptedStringData = cipher.doFinal(plaintext);
        encryptedString = Base64.encodeBase64String(encryptedStringData);

        return encryptedString;
    }

}

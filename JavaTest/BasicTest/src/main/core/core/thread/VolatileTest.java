package core.thread;

import org.junit.Test;

public class VolatileTest {
    
    volatile VolatileTest a;
    @Test
    public void testNullVolatile() {
        //can be null
        System.out.println(a);
    }
    
    
    
    //Not thread safe
    public static class Counter1 {

        //Not use volatile and synch
        public static int count = 0;

        public static void inc() {
            try { // 这里延迟1毫秒，使得结果明显
                Thread.sleep(1);
            } catch (InterruptedException e) {
            }
            count++;
        }
    }

    @Test
    public void testCounter1() {
        // 同时启动1000个线程，去进行i++计算，看看实际结果
        for (int i = 0; i < 1000; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter1.inc();
                }
            }).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 这里每次运行的值都有可能不同,可能为1000
        System.out.println("运行结果:Counter.count=" + Counter1.count);
    }



    //Not thread safe
    public static class Counter2 {
        public volatile static int count = 0;

        //Have to use sync
        public static void inc() {
            try {// 这里延迟1毫秒，使得结果明显
                Thread.sleep(1);
            } catch (InterruptedException e) {
            }

            count++;
        }
    }

    @Test
    public void testCounter2() {
        int total = 1000;
        // 同时启动1000个线程，去进行i++计算，看看实际结果
        for (int i = 0; i < total; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter2.inc();
                }
            }).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 这里每次运行的值都有可能不同,可能为1000
        System.out.println("Counter.count=" + Counter2.count);
        //Not the same!!!
        System.out.println("Expected is: " + total);
    }

    //Thread safe
    public static class Counter3 {
        public static int[] count = new int[1];
        static {
            count[0] = 0;
        }

        public static void inc() {
            synchronized (count) {
                // 这里延迟1毫秒，使得结果明显
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                }

                count[0]++;
            }
        }


    }

    @Test
    public void testCounter3() {
        int c = 1000;
        Thread[] threads = new Thread[c];
        // 同时启动1000个线程，去进行i++计算，看看实际结果
        for (int i = 0; i < c; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter3.inc();
                }
            });
            threads[i].start();
        }
        for (int i = 0; i < c; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // 这里每次运行的值都有可能不同,可能为1000
        System.out.println("运行结果:Counter.count=" + Counter3.count[0]);
    }

    //Thread safe
    public static class Counter4 {

        public volatile static int count = 0;

        public static void inc() {
            synchronized (Counter4.class) {
                // 这里延迟1毫秒，使得结果明显
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                }

                count++;
            }
        }


    }

    @Test
    public void testCounter4() {
        int c = 1000;
        Thread[] threads = new Thread[c];
        // 同时启动1000个线程，去进行i++计算，看看实际结果
        for (int i = 0; i < c; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter4.inc();
                }
            });
            threads[i].start();
        }
        for (int i = 0; i < c; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // 这里每次运行的值都有可能不同,可能为1000
        System.out.println("运行结果:Counter.count=" + Counter4.count);
    }

    public static class Counter5 {
        public static int c = 1000;
        /**
         * correct one
         */
        public volatile static int[] count;

        public static void inc() {
            synchronized (count) {
                // 这里延迟1毫秒，使得结果明显
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                }

                count[0]++;
                if (count[0] == c) {
                    count.notify();
                }
            }
        }


    }


    @Test
    public void testCounter5() {
        int[] count2 = new int[1];
        count2[0] = 0;
        Counter5.count = count2;

        Thread[] threads = new Thread[Counter5.c];
        // 同时启动1000个线程，去进行i++计算，看看实际结果
        for (int i = 0; i < Counter5.c; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter5.inc();
                }
            });
            threads[i].start();
        }
        System.out.println("waiting...");
        synchronized (count2) {
            try {
                count2.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("notified...");
        for (int i = 0; i < Counter5.c; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // 这里每次运行的值都有可能不同,可能为1000
        System.out.println("运行结果:Counter.count=" + Counter5.count[0]);
    }

}

package jmockittest.autowiredcase;

import org.springframework.beans.factory.annotation.Autowired;

class A extends X {
    @Autowired
    B b;

    @Override
    public void method() {
        System.out.println("a method start");
        //do something
        System.out.println(b.getClass().getName());
        b.callMethodInB();
        //do something
        System.out.println("a method end");
    }

}

public class CodalityMain {

    
    //?public
    public static int solution2(int[] A, int K) {
        int n = A.length;
        int best = 0;
        int count = 1;
        for (int i = 0; i < n - K - 1; i++) {
            if (A[i] == A[i + 1])
                count = count + 1;
            else
                count = 0;
            if (count > best)
                best = count;
        }
        int result = best + 1 + K;

        return result;
    }

    public static void main(String[] args) {
        
        int a[]= {1,1,3,3,3,4,5,5,5,5};
        int n=solution2(a,2);
        System.out.println(n);

    }

}

package core;
import sun.misc.*;
/**
 * 
 * #VM arguments
-verbose.gc -XX:+PrintGCDetails

-verbose.gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCApplicationConcurrentTime
 *
 */
public class FinalizeFuncTest {
    private static Block holder;

    public static void main(String[] args) throws Exception {
        final JavaLangAccess jla = SharedSecrets.getJavaLangAccess();
        System.out.println(jla.getClass());
        
        
        holder = new Block();
        holder = null;
        System.out.println("---after set null----");
        System.out.println("---first gc-----");
        System.gc(); // System.in.read();
        //Wait finalize() finished
        Thread.sleep(1000);
        System.out.println("---second gc-----");
//        System.gc();
        //Sleep at last, make sure finalize() can be set breakpoing
        Thread.sleep(1000000);
    }

    static class Block {
        byte[] _200M = new byte[200 * 1024 * 1024];


/*
With finalize()

Not collected in first GC

[GC (System.gc()) [PSYoungGen: 3328K->902K(38400K)] 208128K->205710K(331264K), 0.0013083 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
[Full GC (System.gc()) [PSYoungGen: 902K->0K(38400K)] [ParOldGen: 204808K->205623K(292864K)] 205710K->205623K(331264K), [Metaspace: 2663K->2663K(1056768K)], 0.0065904 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 

invoke finalize

[GC (System.gc()) [PSYoungGen: 1331K->32K(38400K)] 206954K->205655K(331264K), 0.0048845 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
[Full GC (System.gc()) [PSYoungGen: 32K->0K(38400K)] [ParOldGen: 205623K->822K(292864K)] 205655K->822K(331264K), [Metaspace: 2664K->2664K(1056768K)], 0.0105760 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 

*/
        @Override
        protected void finalize() throws Throwable {
            System.out.println("invoke finalize");
            //!!Still alive here
            System.out.println(this);
//            try {
//            Thread.sleep(100000);
//            }catch(Exception e) {
//                e.printStackTrace();
//            }
        }
    }


    /*
    Without finalize()
    
    Collected!
    ParOldGen: 204808K->823K(292864K)
    
    [GC (System.gc()) [PSYoungGen: 3328K->934K(38400K)] 208128K->205742K(331264K), 0.0023717 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
    [Full GC (System.gc()) [PSYoungGen: 934K->0K(38400K)] [ParOldGen: 204808K->823K(292864K)] 205742K->823K(331264K), [Metaspace: 2662K->2662K(1056768K)], 0.0063984 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
     */


    /*
    Two times GC with finalize()
    
    0.310: Application time: 0.0638211 seconds
    0.311: [GC (System.gc()) [PSYoungGen: 2662K->934K(38400K)] 207462K->205742K(331264K), 0.0015520 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
    0.313: [Full GC (System.gc()) [PSYoungGen: 934K->0K(38400K)] [ParOldGen: 204808K->205626K(292864K)] 205742K->205626K(331264K), [Metaspace: 2689K->2689K(1056768K)], 0.0067422 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
    0.319: Total time for which application threads were stopped: 0.0085781 seconds, Stopping threads took: 0.0000295 seconds
    invoke finalize
    1.308: Application time: 0.9887372 seconds
    1.309: [GC (System.gc()) [PSYoungGen: 1331K->64K(38400K)] 206957K->205690K(331264K), 0.0082991 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
    1.317: [Full GC (System.gc()) [PSYoungGen: 64K->0K(38400K)] [ParOldGen: 205626K->825K(292864K)] 205690K->825K(331264K), [Metaspace: 2690K->2690K(1056768K)], 0.0182634 secs] [Times: user=0.06 sys=0.00, real=0.01 secs] 
    1.336: Total time for which application threads were stopped: 0.0274429 seconds, Stopping threads took: 0.0002999 seconds
    Heap
    PSYoungGen      total 38400K, used 333K [0x00000000d5b80000, 0x00000000d8600000, 0x0000000100000000)
    eden space 33280K, 1% used [0x00000000d5b80000,0x00000000d5bd34a8,0x00000000d7c00000)
    from space 5120K, 0% used [0x00000000d8100000,0x00000000d8100000,0x00000000d8600000)
    to   space 5120K, 0% used [0x00000000d7c00000,0x00000000d7c00000,0x00000000d8100000)
    ParOldGen       total 292864K, used 825K [0x0000000081200000, 0x0000000093000000, 0x00000000d5b80000)
    object space 292864K, 0% used [0x0000000081200000,0x00000000812ce670,0x0000000093000000)
    Metaspace       used 2696K, capacity 4486K, committed 4864K, reserved 1056768K
    class space    used 288K, capacity 386K, committed 512K, reserved 1048576K
    1.337: Application time: 0.0017149 seconds
    
    
    
     */

}


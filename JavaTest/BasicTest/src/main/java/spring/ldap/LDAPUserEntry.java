package spring.ldap;

import javax.naming.Name;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DN: cn=xxx,ou=yyy
 * 
 * @author nil4
 *
 */
@Entry(objectClasses = {"person", "top", "inetOrgPerson"})
public class LDAPUserEntry {

    @JsonIgnore
    @Id
    private Name dn;

    @JsonProperty(value = "userName")
    @Attribute(name = "cn")
    @DnAttribute(value = "cn", index = 1)
    private String userName;

    @JsonProperty(value = "surName")
    @Attribute(name = "sn")
    private String surName;

    @JsonProperty(value = "givenName")
    @Attribute(name = "givenName")
    private String givenName;

    @JsonProperty(value = "department")
    @Attribute(name = "ou")
    @DnAttribute(value = "ou", index = 0)
    private String department;

    @JsonProperty(value = "ntId")
    @Attribute(name = "uid")
    private String ntId;

    private String userPassword;

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Name getDn() {
        return dn;
    }

    public void setDn(Name dn) {
        this.dn = dn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getNtId() {
        return ntId;
    }

    public void setNtId(String ntId) {
        this.ntId = ntId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

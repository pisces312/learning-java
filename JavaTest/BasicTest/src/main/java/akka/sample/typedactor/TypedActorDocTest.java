package akka.sample.typedactor;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.TypedActor;
import akka.actor.TypedActorExtension;
import akka.actor.TypedActorFactory;
import akka.actor.TypedProps;
import akka.dispatch.Futures;
import akka.japi.Creator;
import akka.japi.Option;
import akka.routing.RoundRobinGroup;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;


public class TypedActorDocTest {

    ActorSystem system = null;


    @Before
    public void setUp() {
        Config config = ConfigFactory.parseString("akka.loglevel = DEBUG \n" +
            "akka.actor.debug.lifecycle = on");
        //json style
//        Config config = ConfigFactory.parseString("akka {loglevel = DEBUG}");
        system = ActorSystem.create("HelloActorSystem", config);
    }

    @After
    public void tearDown() {
        system.terminate();
    }

    static public interface Squarer {
        void squareDontCare(int i); //fire-forget

        Future<Integer> square(int i); //non-blocking send-request-reply

        Option<Integer> squareNowPlease(int i);//blocking send-request-reply

        int squareNow(int i); //blocking send-request-reply
    }

    static class SquarerImpl implements Squarer, TypedActor.PreStart, TypedActor.PostStop,
        TypedActor.PreRestart, TypedActor.PostRestart {
        private String name;

        private Squarer proxy;

        public SquarerImpl() {
            this.name = "default";
            //Returns the current ActorContext,
            // method only valid within methods of a TypedActor implementation
            ActorContext context = TypedActor.context();
            System.out.println("SquarerImpl - ActorContext:" + context);


            //Returns a contextual instance of the Typed Actor Extension
            //this means that if you create other Typed Actors with this,
            //they will become children to the current Typed Actor.
            TypedActorFactory typedActorFactory = TypedActor.get(TypedActor.context());
            System.out.println("SquarerImpl - TypedActorFactory:" + typedActorFactory);

            //Returns the external proxy of the current Typed Actor,
            // method only valid within methods of a TypedActor implementation
            Squarer sq = TypedActor.<Squarer>self();
            System.out.println("SquarerImpl - Proxy(Actor):" + sq);
            this.proxy = sq;
        }

        public SquarerImpl(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void squareDontCare(int i) {
            int sq = i * i; //Nobody cares :(
        }

        public Future<Integer> square(int i) {
            return Futures.successful(i * i);
        }

        public Option<Integer> squareNowPlease(int i) {
            return Option.some(i * i);
        }

        public int squareNow(int i) {
            return i * i;
        }


        /***************************************************
         * Lifecycle callbacks
         */

        @Override
        public void preStart() {
            System.out.println(proxy + " preStart");
        }

        @Override
        public void postRestart(Throwable reason) {
            System.out.println(proxy + " postRestart");
        }

        @Override
        public void preRestart(Throwable reason, scala.Option<Object> message) {
            System.out.println(proxy + " preRestart");
        }

        @Override
        public void postStop() {
            System.out.println(proxy + " postStop");
        }
    }

    @Test
    public void mustGetTheTypedActorExtension() {
        try {
            //Returns the Typed Actor Extension
            TypedActorExtension extension = TypedActor.get(system); //system is an instance of ActorSystem
            System.out.println(extension);


            Object someReference =
                TypedActor.get(system).typedActorOf(
                    new TypedProps<SquarerImpl>(Squarer.class, SquarerImpl.class));
            //Returns whether the reference is a Typed Actor Proxy or not
            boolean flag = TypedActor.get(system).isTypedActor(someReference);
            System.out.println("isTypedActor:" + flag);

            //Returns the backing Akka Actor behind an external Typed Actor Proxy
            ActorRef typedActor = TypedActor.get(system).getActorRefFor(someReference);
            System.out.println(typedActor);

        } catch (Exception e) {
            //dun care
            e.printStackTrace();
        }
    }

    @Test
    public void createATypedActor() {
        try {
            Squarer mySquarer =
                TypedActor.get(system).typedActorOf(
                    new TypedProps<SquarerImpl>(Squarer.class, SquarerImpl.class));
            Squarer otherSquarer =
                TypedActor.get(system).typedActorOf(
                    new TypedProps<SquarerImpl>(Squarer.class, new Creator<SquarerImpl>() {
                        private static final long serialVersionUID = -2720204590087002861L;

                        public SquarerImpl create() {
                            return new SquarerImpl("foo");
                        }
                    }), "name");



            //            One-way message send
            mySquarer.squareDontCare(10);

            //            Request-reply-with-future message send
            Future<Integer> fSquare = mySquarer.square(10); //A Future[Int]
            assertEquals(100, Await.result(fSquare, Duration.create(3, TimeUnit.SECONDS))
                .intValue());

            //            Request-reply message send
            Option<Integer> oSquare = mySquarer.squareNowPlease(10); //Option[Int]
            assertEquals(100, oSquare.get().intValue());

            //Block
            int iSquare = mySquarer.squareNow(10); //Int
            assertEquals(100, iSquare);



            TypedActor.get(system).stop(mySquarer);
            TypedActor.get(system).poisonPill(otherSquarer);
        } catch (Exception e) {
            //Ignore
        }
    }

    @Test
    public void createHierarchies() {
        try {
            Squarer childSquarer =
                TypedActor.get(TypedActor.context()).typedActorOf(
                    new TypedProps<SquarerImpl>(Squarer.class, SquarerImpl.class));
            System.out.println(childSquarer);
            //Use "childSquarer" as a Squarer
        } catch (Exception e) {
            //dun care
        }
    }

    @Test
    public void proxyAnyActorRef() {
        try {
            final ActorRef actorRefToRemoteActor = system.deadLetters();
            Squarer typedActor =
                TypedActor.get(system).typedActorOf(new TypedProps<Squarer>(Squarer.class),
                    actorRefToRemoteActor);
            //Use "typedActor" as a FooBar
            System.out.println(typedActor);
        } catch (Exception e) {
            //dun care
        }
    }

    public static interface HasName {
        String name();
    }
    public static class Named implements HasName {
        private int id = new Random().nextInt(1024);

        @Override
        public String name() {
            return "name-" + id;
        }
    }

    @Test
    public void typedRouterPattern() {
        try {
            // prepare routees
            TypedActorExtension typed = TypedActor.get(system);
            HasName named1 = typed.typedActorOf(new TypedProps<Named>(HasName.class, Named.class));
            HasName named2 = typed.typedActorOf(new TypedProps<Named>(HasName.class, Named.class));

//            List<HasName> routees = new ArrayList<HasName>();
//            routees.add(named1);
//            routees.add(named2);
            List<String> routeePaths = new ArrayList<String>();
            routeePaths.add(typed.getActorRefFor(named1).path().toStringWithoutAddress());
            routeePaths.add(typed.getActorRefFor(named2).path().toStringWithoutAddress());


            // prepare untyped router
            ActorRef router = system.actorOf(new RoundRobinGroup(routeePaths).props(), "router");
            // prepare typed proxy, forwarding MethodCall messages to ‘router‘
            HasName typedRouter =
                typed.typedActorOf(new TypedProps<Named>(HasName.class, Named.class), router);

            //Take place by turn, RoundRobin
            System.out.println("actor was: " + typedRouter.name()); // name-243
            System.out.println("actor was: " + typedRouter.name()); // name-614
            System.out.println("actor was: " + typedRouter.name()); // name-243
            System.out.println("actor was: " + typedRouter.name()); // name-614

            typed.poisonPill(named1);
            typed.poisonPill(named2);
            typed.poisonPill(typedRouter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

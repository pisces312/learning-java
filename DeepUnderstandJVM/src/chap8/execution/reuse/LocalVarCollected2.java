package chap8.execution.reuse;

public class LocalVarCollected2 {

    //-verbose:gc
    public static void main(String[] args) {
        {
            byte[] placeholder = new byte[64 * 1024 * 1024];
            //set null
            placeholder = null;
        }
        System.gc();
//        [GC (System.gc())  67532K->66176K(125952K), 0.0014954 secs]
//            [Full GC (System.gc())  66176K->529K(125952K), 0.0055914 secs]

    }

}

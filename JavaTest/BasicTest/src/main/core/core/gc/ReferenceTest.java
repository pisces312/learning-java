package core.gc;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import org.junit.Test;
import static org.junit.Assert.*;

public class ReferenceTest {

    @Test
    public void strongReference() {
        Object referent = new Object();

        /**
         * 通过赋值创建 StrongReference
         */
        Object strongReference = referent;

        assertSame(referent, strongReference);

        referent = null;
        System.gc();

        /**
         * StrongReference 在 GC 后不会被回收
         **/
        assertNotNull(strongReference);
    }

    @Test
    public void weakReference() {
        Object referent = new Object();
        WeakReference<Object> weakRerference = new WeakReference<Object>(referent);

        assertSame(referent, weakRerference.get());

        referent = null;
        System.gc();

        /**
         * 参考 http://blog.csdn.net/matrix_xu/article/details/8424038 一旦没有指向 referent 的强引用, weak
         * reference 在 GC 后会被自动回收
         */
        assertNull(weakRerference.get());
    }

    @Test
    public void weakHashMap() throws InterruptedException {
        Map<Object, Object> weakHashMap = new WeakHashMap<Object, Object>();
        Object key = new Object();
        Object value = new Object();
        weakHashMap.put(key, value);

        assertTrue(weakHashMap.containsValue(value));

        key = null;
        System.gc();

        /**
         * 等待无效 entries 进入 ReferenceQueue 以便下一次调用 getTable 时被清理
         */
        Thread.sleep(1000);

        /**
         * 一旦没有指向 key 的强引用, WeakHashMap 在 GC 后将自动删除相关的 entry
         */
        assertFalse(weakHashMap.containsValue(value));
    }

    @Test
    public void softReference() {
        Object referent = new Object();
        SoftReference<Object> softRerference = new SoftReference<Object>(referent);
        /*
         * Returns this reference object's referent. If this reference object has been cleared,
         * either by the program or by the garbage collector, then this method returns null.
         */
        assertNotNull(softRerference.get());

        referent = null;
        System.gc();

        /**
         * soft references 只有在 jvm OutOfMemory 之前才会被回收, 所以它非常适合缓存应用
         */
        assertNotNull(softRerference.get());
    }

    @Test
    public void phantomReferenceAlwaysNull() throws InterruptedException {
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<Object>();

        Object referent = new Object();
        PhantomReference<Object> phantomReference = new PhantomReference<Object>(referent, referenceQueue);
        //get() always return null
        assertNull(phantomReference.get());

        Reference<? extends Object> polled = referenceQueue.poll();
        assertNull(polled);


        referent = null;
        System.gc();

        Thread.sleep(1000);
        polled = referenceQueue.poll();
        assertNotNull(polled);


    }

    /*
     * 将一个 ReferenceQueue 传给一个 Reference的构造函数， 当对象被回收时， 虚拟机会自动将这个对象插入到 ReferenceQueue中。 WeakHashMap
     * 就是利用 ReferenceQueue 来清除 key 已经没有强引用的 entries。
     */
    @Test
    public void referenceQueue() throws InterruptedException {
        Object referent = new Object();
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<Object>();
        WeakReference<Object> weakReference = new WeakReference<Object>(referent, referenceQueue);

        assertFalse(weakReference.isEnqueued());
        Reference<? extends Object> polled = referenceQueue.poll();
        assertNull(polled);

        //Set StrongReference to null
        referent = null;
        System.gc();
        /*
         * 一旦没有指向 referent 的强引用, weak reference 在 GC 后会被回收 当对象被回收时， 虚拟机会自动将这个对象插入到 ReferenceQueue
         */
        Thread.sleep(1000);
        //May not be enqueued immediately
        assertTrue(weakReference.isEnqueued());
        Reference<? extends Object> removed = referenceQueue.remove();
        assertNotNull(removed);
    }


    @Test
    public void testWeakRef() {
        class LargeObj {
            private byte[] b;

            public LargeObj() {
                // b = new byte[1024 * 10];
                b = new byte[1024 * 1024];
            }
        }


        ReferenceQueue<LargeObj> q = new ReferenceQueue<>();
        Map<Integer, WeakReference<LargeObj>> map = new HashMap<>();
        for (int i = 0; i < 1000; i++)
            map.put(i, new WeakReference<LargeObj>(new LargeObj(), q));

        int i = 0;
        for (Reference<LargeObj> r : map.values())
            if (r.get() == null)
                i++;
        //747, not 1000
        System.out.println("被回收的对象数:" + i);

        i = 0;
        Reference<? extends LargeObj> ref = null;
        while ((ref = q.poll()) != null) {
            ++i;
            ref.clear();
        }

        System.out.println("Ref in queue i " + i);

    }
}

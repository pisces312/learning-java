package chap3.allocpolicy;

/**
 * For first allocation, not for GC (-XX:MaxTenuringThreshold)
 * 
 * Threshold for big object so that it can go to old generation directly
 * 
 * -XX:PretenureSizeThreshold
 * 设置大对象直接进入年老代的阈值。
 * Only for SerialGC and ParNew, not for ParScanvege
 * default is 0, it depends on JVM
 * Set 3MB for this demo
 * 
 * 
 * Enable ParNew+CMS
 * -XX:+UseConcMarkSweepGC
 * ParNew for young generation
 * CMS for old generation
 * 
 */
//VM args：
// -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:PretenureSizeThreshold=3145728 -XX:+UseConcMarkSweepGC
public class PretenureSizeThreshold {
    private static final int _1MB = 1024 * 1024;

    @SuppressWarnings("unused")
    public static void main(String[] args) {
//        Heap
//        par new generation   total 9216K, used 1147K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
//         eden space 8192K,  14% used [0x00000000fec00000, 0x00000000fed1ef50, 0x00000000ff400000)
//         from space 1024K,   0% used [0x00000000ff400000, 0x00000000ff400000, 0x00000000ff500000)
//         to   space 1024K,   0% used [0x00000000ff500000, 0x00000000ff500000, 0x00000000ff600000)
//        concurrent mark-sweep generation total 10240K, used 4096K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
//        Metaspace       used 2649K, capacity 4486K, committed 4864K, reserved 1056768K
//         class space    used 285K, capacity 386K, committed 512K, reserved 1048576K

        //allocated to CMS generation directly
        //without XX:PretenureSizeThreshold=3145728
        //it will be allocated to eden
        byte[] allocation = new byte[4 * _1MB];
    }
}

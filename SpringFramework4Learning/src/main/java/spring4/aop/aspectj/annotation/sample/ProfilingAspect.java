package spring4.aop.aspectj.annotation.sample;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * Define Aspect (Advice and Pointcut)
 *
 */
@Aspect
//Must use @Component to let IoC know
@Component
public class ProfilingAspect {

    /******************************************************************
     * Advice
     * 
     ******************************************************************/


    //Around advice
    //Calculate running time
    @Around("classToProfileBean()") //work
    //    @Around("publicHandleOperation()") //work
    //    @Around("anyPublicOperation()") //work
    //    @Around("curPkgOperation()") //work
    //    @Around("methodsToBeProfiled()")
    //define with pointcut
    //    @Around("execution(* spring4.aop.aspectj.annotation.sample.*.*(..))")
    public Object profileAroundAdvice(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("Profiling " + pjp.getSignature()
            .getDeclaringTypeName() + "."
            + pjp.getSignature()
                .getName());
        StopWatch sw = new StopWatch(getClass().getSimpleName());
        try {
            sw.start(pjp.getSignature()
                .getName());
            return pjp.proceed();
        } finally {
            sw.stop();
            System.out.println(sw.prettyPrint());
        }
    }

    @Before("classToProfileBean()")
    public void beforeAdvice(JoinPoint joinPoint) {
        System.out.println("Before: " + joinPoint.getSignature()
            .getDeclaringTypeName() + "."
            + joinPoint.getSignature()
                .getName());
    }

    @After("classToProfileBean()")
    public void afterAdvice(JoinPoint joinPoint) {
        System.out.println("After: " + joinPoint.getSignature()
            .getDeclaringTypeName() + "."
            + joinPoint.getSignature()
                .getName());
    }

    @AfterReturning(pointcut="classToProfileBean()", returning = "retVal")
    public void afterReturningAdvice(JoinPoint joinPoint, Object retVal) {
        System.out.println("AfterReturning: " + joinPoint.getSignature()
            .getDeclaringTypeName() + "."
            + joinPoint.getSignature()
                .getName());
        System.out.println("Return Value: " + retVal);
    }


    /******************************************************************
     * Point cut
     * 
     * Not execute function body, only indicate the entry of point cut
     * 
     * Point cut expression
     * 
     * execution(return type, package name, class name, method name, arg list)
     ******************************************************************/

    //reply on hard-coded package name
    @Pointcut("execution(* spring4.aop.aspectj.annotation.sample.*.*(..))")
    private void curPkgOperation() {}

    @Pointcut("execution(public * handle*(..))")
    private void publicHandleOperation() {}

    @Pointcut("bean(classToProfile)")
    private void classToProfileBean() {}

    @Pointcut("execution(public * foo..*.*(..))")
    private void methodsToBeProfiled() {}

    @Pointcut("execution(public * *(..))")
    private void anyPublicOperation() {}

    @Pointcut("within(spring4.aop.aspectj.annotation.sample.*)")
    private void inCurrentPackage() {}

    @Pointcut("anyPublicOperation() && inCurrentPackage()")
    private void curPublicOperation() {}
}

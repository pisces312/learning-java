package chap3.allocpolicy;

/**
 * ???still dynamic
 * -XX:MaxTenuringThreshold + -XX:-UseAdaptiveSizePolicy + -XX:+UseConcMarkSweepGC
 * 
 * 
 * 
 */
//VM������-verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:-UseAdaptiveSizePolicy -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=1 -XX:+PrintTenuringDistribution
public class MaxTenuringThresholdAdaptivePolicy {
    private static final int _1MB = 1024 * 1024;

    @SuppressWarnings("unused")
    public static void main(String[] args) {

        byte[] allocation1, allocation2, allocation3;
        allocation1 = new byte[_1MB / 4];
        allocation2 = new byte[4 * _1MB];
        //
        allocation3 = new byte[4 * _1MB];
//        Actually all survivors are moved to tenured generation
        allocation3 = null;
        allocation3 = new byte[4 * _1MB];
    }
}

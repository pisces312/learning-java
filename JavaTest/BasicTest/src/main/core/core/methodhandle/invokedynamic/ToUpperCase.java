package core.methodhandle.invokedynamic;

import java.lang.invoke.CallSite;
import java.lang.invoke.ConstantCallSite;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;

public class ToUpperCase {


    public static CallSite bootstrap(Lookup lookup, String name, MethodType type, String arg)
        throws Exception {
        System.out.println(
            "name=" + name + ", desc=" + type.toMethodDescriptorString() + ", value=" + arg);

        MethodHandle mh = null;
        //name can be anyone
        //type has to match what defined for invokedynamic
        if ("toUpperCaseMethod".equals(name)) {
            //TODO Use self-defined impl
//            Class<?> implClass = ToUpperCase.class;
//            String implMethod = "myToUpperCase";            
//            mh = lookup.findStatic(implClass, implMethod, type);//.bindTo(arg);
            //
            //or just call String.toUpperCase
            mh = lookup.findVirtual(String.class, "toUpperCase", type).bindTo(arg);

        } else {

        }


        //it can also use MutableCallSite or VolatileCallSite
        return new ConstantCallSite(mh);
    }

    public static String myToUpperCase(String val) {
        return val.toUpperCase();
    }

}

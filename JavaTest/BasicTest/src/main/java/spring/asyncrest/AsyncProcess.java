package spring.asyncrest;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

@Component
public class AsyncProcess {
    private static Logger logger = LoggerFactory.getLogger(AsyncProcess.class);

    @Async
    public Future<String> asyncCall() {
        try {
            logger.info("Sleeping now...");
            Thread.sleep(5000);
            logger.info("Return result");
            return new AsyncResult<String>("Hey");
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

}

package zookeeper.embeded;

import java.io.IOException;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EmbededZookeeperServerTest {

    
    private static EmbeddedZooKeeperServer _embeddedServer;
    private ZooKeeper _zooKeeper;
    
    private static final int ZK_PORT = 53181;
    private static final String ZK_CONNECTION_STRING = "localhost:" + ZK_PORT;
    @BeforeClass
    public static void beforeAll() throws IOException, InterruptedException {
        _embeddedServer = new EmbeddedZooKeeperServer(ZK_PORT);
        _embeddedServer.start();
    }

    @AfterClass
    public static void afterAll() {
        _embeddedServer.shutdown();
    }

    @Before
    public void setUp() throws IOException, InterruptedException {
        _zooKeeper = new ConnectionHelper().connect(ZK_CONNECTION_STRING);
    }

    @After
    public void tearDown() throws InterruptedException, KeeperException {
        _zooKeeper.close();
    }
    
    
    @Test
    public void test() throws Exception{
        
//        _zooKeeper.
        Thread.sleep(5000);
    }
}

package dozertest;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.dozer.DozerBeanMapper;

import org.junit.Test;

public class MyDozerTest {



    @Test
    public void testMapToHashMap() throws Exception {
        // Test simple Map --> Vo with custom mappings defined.
        DozerBeanMapper mapper = new DozerBeanMapper();
        InputStream in =
            new FileInputStream("src/main/java/dozertest/resources/MyDozerMapping.xml");
        mapper.addMapping(in);

        Map<String, String> src = new HashMap<String, String>();
        src.put("stringValue", "somevalue");

        HashMap<?, ?> dest = mapper.map(src, HashMap.class, "map-hashmap");

        System.out.println(dest);
        //        assertEquals("wrong value found for field1", "somevalue", dest.getField1());
    }

    @Test
    public void testMapToMyMap() throws Exception {
        // Test simple Map --> Vo with custom mappings defined.
        DozerBeanMapper mapper = new DozerBeanMapper();
        InputStream in =
            new FileInputStream("src/main/java/dozertest/resources/MyDozerMapping.xml");
        mapper.addMapping(in);

        Map<String, String> src = new HashMap<String, String>();
        src.put("stringValue", "somevalue");

        MyMap dest = mapper.map(src, MyMap.class, "map-map");

        System.out.println(dest);
        //        assertEquals("wrong value found for field1", "somevalue", dest.getField1());
    }


    //    @Test
    //    public void testMapToMap() throws Exception {
    //        // Test simple Map --> Vo with custom mappings defined.
    //        DozerBeanMapper mapper = new DozerBeanMapper();
    //        InputStream in =
    //            new FileInputStream("src/main/java/dozertest/resources/MyDozerMapping.xml");
    //        mapper.addMapping(in);
    //
    //        Map<String, String> src = new HashMap<String, String>();
    //        src.put("stringValue", "somevalue");
    //
    //        Map dest = mapper.map(src, Map.class, "map-map");
    //
    //        System.out.println(dest);
    ////        assertEquals("wrong value found for field1", "somevalue", dest.getField1());
    //    }



    @Test
    public void testMapToVoUsingMapInterface() throws Exception {
        // Test simple Map --> Vo with custom mappings defined.
        DozerBeanMapper mapper = new DozerBeanMapper();
        InputStream in =
            new FileInputStream("src/main/java/dozertest/resources/MyDozerMapping.xml");
        mapper.addMapping(in);

        Map<String, String> src = new HashMap<String, String>();
        src.put("stringValue", "somevalue");

        SimpleObj dest = mapper.map(src, SimpleObj.class, "test-id");

        assertEquals("wrong value found for field1", "somevalue", dest.getField1());
    }


    /**
     * Map -> Bean
     * 
     * @throws Exception
     */
    @Test
    public void testMapToVoUsingMapId() throws Exception {

        DozerBeanMapper mapper = new DozerBeanMapper();
        InputStream in =
            new FileInputStream("src/main/java/dozertest/resources/MyDozerMapping.xml");
        mapper.addMapping(in);


        Map<String, String> src = new HashMap<String, String>();
        src.put("field1", "field1value");
        src.put("field2", "field2value");

        NestedObjPrime dest = mapper.map(src, NestedObjPrime.class, "caseB");

        System.out.println(dest.getField1());
        System.out.println(dest.getField2());

        assertEquals(src.get("field1"), dest.getField1());
        assertEquals(src.get("field2"), dest.getField2());
    }



}

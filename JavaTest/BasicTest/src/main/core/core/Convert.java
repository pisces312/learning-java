package core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class Convert {
//public static int getIntValue(int position, byte[] data)
//            {
//        byte[] tmp = new byte[4];
//
//        System.arraycopy(data, position, tmp, 0, 4);
//
//        InputStream in = new ByteArrayInputStream(tmp);
//        DataInputStream instr = new DataInputStream(in);
//        int value = 0;
//        try {
//            value = instr.readInt();
//        } catch (IOException ex) {
//            Logger.getLogger(Convert.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return value;
//    }

    public static int getIntValue(int offset, byte[] b) {
        int s = 0;
        int i;
        for (i = offset; i < 3 + offset; i++) {
            s += b[i];
            if (b[i] < 0) {
                s += 256;
            }
            s *= 256;
        }
        s += b[i];
        if (b[i] < 0) {
            s += 256;
        //鏈?悗涓?釜涔嬫墍浠ヤ笉涔橈紝鏄洜涓哄彲鑳戒細婧㈠嚭
        }
        return s;
    }

    public static float getFloValue(int position, byte[] data)
            throws IOException {
        byte[] tmp = new byte[4];

        System.arraycopy(data, position, tmp, 0, 4);

        InputStream in = new ByteArrayInputStream(tmp);
        DataInputStream instr = new DataInputStream(in);
        float value = instr.readFloat();
//        Float.

        return value;
    }

    public static short getShortValue(int offset, byte[] b) {
        short s = 0;
        int i = offset;
        s += b[i];
        if (b[i] < 0) {
            s += 256;
        }
        s *= 256;
        i++;
        s += b[i];
        if (b[i] < 0) {
            s += 256;
        //鏈?悗涓?釜涔嬫墍浠ヤ笉涔橈紝鏄洜涓哄彲鑳戒細婧㈠嚭
        }
        return s;
    }
//    public static short getShortValue(int position, byte[] data)
//            throws IOException {
//        byte[] tmp = new byte[2];
//
//        System.arraycopy(data, position, tmp, 0, 2);
//
//        InputStream in = new ByteArrayInputStream(tmp);
//        DataInputStream instr = new DataInputStream(in);
//        short value = instr.readShort();
//
//        return value;
//    }

    public static String getStrValue(int position, byte[] data, int length)
            throws IOException {
        byte[] tmp = new byte[length];

        System.arraycopy(data, position, tmp, 0, length);

        InputStream in = new ByteArrayInputStream(tmp);
        DataInputStream instr = new DataInputStream(in);
        String value = instr.readUTF();
        return value;
    }

    public static char getCharValue(int position, byte[] data)
            throws IOException {
        
        byte[] tmp = new byte[2];

        System.arraycopy(data, position, tmp, 0, 2);

        InputStream in = new ByteArrayInputStream(tmp);
        DataInputStream instr = new DataInputStream(in);
        char value = instr.readChar();
        return value;
    }

    public static byte[] intToByte(final int number) {
        int temp = number;
        byte[] b = new byte[4];
        for (int i = b.length - 1; i > -1; i--) {
            b[i] = new Integer(temp & 0xff).byteValue();
            //灏嗘渶楂樹綅淇濆瓨鍦ㄦ渶浣庝綅
            temp = temp >> 8;
        //鍚戝彸绉?浣?        
        }
        return b;
    }

    public static void setIntValue(int value, int position, byte[] b) {

        int temp = value;
        position += 3;
        for (int i = 0; i < 4; i++, position--) {
            b[position] = (byte) (temp & 0xff);
            temp >>= 8;
        }
    }

    public static void setFloValue(float value, int position, byte[] data)
            throws IOException {

        OutputStream out = new ByteArrayOutputStream();
        DataOutputStream outstr = new DataOutputStream(out);

        outstr.writeFloat(value);

        byte[] B = ((ByteArrayOutputStream) out).toByteArray();

        System.arraycopy(B, 0, data, position, 4);

    }

    public static void setShortValue(int value, int position, byte[] b) {

        int temp = value;
        position++;
        for (int i = 0; i < 2; i++, position--) {
            b[position] = (byte) (temp & 0xff);
            temp >>= 8;
        }
    }
//    public static void setShortValue(short value, int position, byte[] data)
//            throws IOException {
//        OutputStream out = new ByteArrayOutputStream();
//        DataOutputStream outstr = new DataOutputStream(out);
//
//        outstr.writeShort(value);
//
//        byte[] B = ((ByteArrayOutputStream) out).toByteArray();
//
//        System.arraycopy(B, 0, data, position, 2);
//    }
//???濡傛灉娌″～婊?//    public static void setStrValue(String value, int position, byte[] data) {
//        byte[] strBytes = value.getBytes();
//        System.arraycopy(strBytes, 0, data, position, strBytes.length);
//    }

    /**
     * 濡傛灉瀛楄妭鏁扮粍娌¤濉弧涔熻兘澶勭悊锛侊紒
     * @param value
     * @param position
     * @param data
     * @throws java.io.IOException
     */
    public static void setStrValue(String value, int position, byte[] data)
            throws IOException {
        OutputStream out = new ByteArrayOutputStream();
        DataOutputStream outstr = new DataOutputStream(out);

        outstr.writeUTF(value);

        byte[] B = ((ByteArrayOutputStream) out).toByteArray();

        int sz = outstr.size();

        System.arraycopy(B, 0, data, position, sz);
    }

    public static void setCharValue(char value, int position, byte[] data)
            throws IOException {
        OutputStream out = new ByteArrayOutputStream();
        DataOutputStream outstr = new DataOutputStream(out);

        outstr.writeChar(value);

        byte[] B = ((ByteArrayOutputStream) out).toByteArray();

        System.arraycopy(B, 0, data, position, 2);
    }
}

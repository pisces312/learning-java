
package spring4.aop.aspectj.sample;

public interface Hello {

    void sayHelloBefore();

    void sayHelloAfter();


    String sayHelloAfterReturning();

    void sayHelloAfterThrowing();


    void sayHelloAround();

}

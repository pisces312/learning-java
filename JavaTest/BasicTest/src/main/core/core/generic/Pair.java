package core.generic;

public class Pair<T> {
    public Pair() {
        first = null;
        second = null;
    }

    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }

    public void setFirst(T newValue) {
        first = newValue;
    }

    public void setSecond(T newValue) {
        second = newValue;
    }

    public static <T> Pair<T> makePair(Class<T> cl) {
        try {
            return new Pair<T>(cl.newInstance(), cl.newInstance());
        } catch (Exception ex) {
            return null;
        }
    }

    private T first;
    private T second;

    static class ArrayAlg {
        /**
         * Gets the minimum and maximum of an array of strings.
         * 
         * @param a an array of strings
         * @return a pair with the min and max value, or null if a is null or
         *         empty
         */
        public static Pair<String> minmax(String[] a) {
            if (a == null || a.length == 0)
                return null;
            String min = a[0];
            String max = a[0];
            for (int i = 1; i < a.length; i++) {
                if (min.compareTo(a[i]) > 0)
                    min = a[i];
                if (max.compareTo(a[i]) < 0)
                    max = a[i];
            }
            return new Pair<String>(min, max);
        }

        public static <T> T getMiddle(T[] a) {
            return a[a.length / 2];
        }

        /**
         * Gets the minimum and maximum of an array of objects of type T.
         * 
         * @param a an array of objects of type T
         * @return a pair with the min and max value, or null if a is
         *         null or empty
         */
        public static <T extends Comparable> Pair<T> minmax(T[] a) {
            if (a == null || a.length == 0)
                return null;

            T min = a[0];
            T max = a[0];
            for (int i = 1; i < a.length; i++) {
                if (min.compareTo(a[i]) > 0)
                    min = a[i];
                if (max.compareTo(a[i]) < 0)
                    max = a[i];
            }
            return new Pair<T>(min, max);
        }
    }

    public static void main(String[] args) {
        //        Pair<String> pStr;
        String[] words = {"Mary", "had", "a", "little", "lamb"};
        Pair<String> mm = ArrayAlg.minmax(words);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
        System.out.println(ArrayAlg.getMiddle(words));

        Pair<String> p = Pair.makePair(String.class);
        System.out.println(p);
        //        JButton obj=ArrayAlg.getMiddle(words);
        //        Object obj=ArrayAlg.getMiddle(words);
        //        System.out.println(obj);
        //        double middle = ArrayAlg.getMiddle(3.14, 1729.0, 0.0);
    }
}

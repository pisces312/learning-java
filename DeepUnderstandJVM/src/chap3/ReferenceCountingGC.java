package chap3;

/**
 * 
 * Java can handle circular reference,
 * not using reference counting GC
 * 
 * -verbose.gc -XX:+PrintGCDetails
 */
public class ReferenceCountingGC {
    private static final int _1MB = 1024 * 1024;

    /* members */
    //circular reference
    public Object instance = null;

    //cost lot of memory, to demonstrate GC
    private byte[] bigSize = new byte[2 * _1MB];


    public static void main(String[] args) {
        ReferenceCountingGC objA = new ReferenceCountingGC();
        ReferenceCountingGC objB = new ReferenceCountingGC();

        objA.instance = objB;
        objB.instance = objA;

        objA = null;
        objB = null;

        // 假设在这行发生GC，objA和objB是否能被回收？
        System.gc();
    }
}


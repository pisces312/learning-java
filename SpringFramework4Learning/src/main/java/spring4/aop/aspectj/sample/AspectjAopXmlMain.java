package spring4.aop.aspectj.sample;

import org.apache.log4j.Level;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import spring4.utils.Utils;

public class AspectjAopXmlMain {
    private static ClassPathXmlApplicationContext applicationContext;

    private static Hello hello;

    //main style
    public static void main(String[] args) {
        Utils.setSpringLogLevel(Level.DEBUG);
        String xmlPath = Utils.getXmlPath(AspectjAopXmlMain.class, "AspectjAopConfig.xml");
        applicationContext = new ClassPathXmlApplicationContext(xmlPath);
        hello = (Hello) applicationContext.getBean("hello");
        hello.sayHelloBefore();
        hello.sayHelloAround();
        hello.sayHelloAfter();
        hello.sayHelloAfterReturning();
        try {
            hello.sayHelloAfterThrowing();
        } catch (Exception e) {
        }
    }


    @Before
    public void before() throws Exception {
        Utils.setSpringLogLevel(Level.DEBUG);
        String xmlPath = Utils.getXmlPath(AspectjAopXmlMain.class, "AspectjAopConfig.xml");
        applicationContext = new ClassPathXmlApplicationContext(xmlPath);
        hello = (Hello) applicationContext.getBean("hello");
    }

    @Test
    public void testBefore() {
        System.out.println("=====Before Advice Begin=====");
        hello.sayHelloBefore();
        System.out.println("=====Before Advice End=====");
    }

    @Test
    public void testAfter() {
        System.out.println("=====After Advice Begin=====");
        hello.sayHelloAfter();
        System.out.println("=====After Advice End=====");
    }

    @Test
    public void testAfterReturning() {
        System.out.println("=====After Returning Advice Begin=====");
        hello.sayHelloAfterReturning();
        System.out.println("=====After Returning Advice End=====");
    }

    @Test
    public void testAround() {
        System.out.println("=====Around Advice Begin=====");
        hello.sayHelloAround();
        System.out.println("=====Around Advice End=====");
    }

    @Test
    public void testThrows() {
        System.out.println("=====Throws Advice Begin=====");
        try {
            hello.sayHelloAfterThrowing();
            assert (false);
        } catch (Exception e) {

        }
        System.out.println("=====Throws Advice End=====");
    }

    @Test
    public final void testIntroduction() {
        System.out.println("=====Introduction Begin=====");
        System.out.println(hello.getClass().getName());
        // 由于对SayHello引入(introduct)了Ok接口，使其具备了Ok接口的功能
        ((Ok) hello).sayOk();
        System.out.println("=====Introduction End=====");
    }

}

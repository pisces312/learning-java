/**
 * 静态类
 * 
 * @author DELL
 */
public class StaticClass {
    int a;
    static {

    };

    static class TestClass {

        public void a() {}
    }
    public class TestClass2 {

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TestClass a2 = new TestClass();

        StaticClass.TestClass a = new TestClass();
        // StaticClass.TestClass2 b=new StaticClass().TestClass2();
        // ??????
        // StaticClass.TestClass2 b=new TestClass2();
        // a.a();
    }
}

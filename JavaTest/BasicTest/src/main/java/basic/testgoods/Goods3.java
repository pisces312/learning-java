/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package basic.testgoods;

/**
 *匿名内部类,没有类名，只有实现的类！！
　　java的匿名内部类的语法规则看上去有些古怪，不过如同匿名数组一样，当你只需要创建一个类的对象而且用不上它的名字时，使用内部类可以使代码看上去简洁清楚。它的语法规则是这样的：
new interfacename(){......}; 或 new superclassname(){......}; 
 * @author pisces
 */
public class Goods3 {

    public ContentsInterface cont() {
        /**
         * 有一点需要注意的是，匿名内部类由于没有名字，所以它没有构造函数（但是如果这个匿名内部类继承了一个只含有带参数构造函数的父类，创建它的时候必须带上这些参数，并在实现的过程中使用super关键字调用相应的内容）。如果你想要初始化它的成员变量，有下面几种方法：
        　　如果是在一个方法的匿名内部类，可以利用这个方法传进你想要的参数，不过记住，这些参数必须被声明为final。 
        　　将匿名内部类改造成有名字的局部内部类，这样它就可以拥有构造函数了。 
        　　在这个匿名内部类中使用初始化代码块。 
         * @return
         */
        return new ContentsInterface() {

            private int i = 11;

            public int value() {
                return i;
            }
        };
    }
}

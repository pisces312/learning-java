package spring.hivetemplate;

import java.util.List;

import javax.sql.DataSource;

import org.apache.hive.jdbc.HiveDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.hadoop.hive.HiveClient;
import org.springframework.data.hadoop.hive.HiveClientCallback;
import org.springframework.data.hadoop.hive.HiveClientFactoryBean;
import org.springframework.data.hadoop.hive.HiveTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.util.CollectionUtils;

@SpringBootApplication
public class HiveTemplateTest implements CommandLineRunner {


    @Autowired
    private HiveTemplate hiveTemplate;



    @Override
    public void run(String... args) throws Exception {
        List<String> databaseList = hiveTemplate.query("show databases");
        System.out.println(databaseList);
        if(CollectionUtils.isEmpty(databaseList)){
            return;
        }
        
        List<String> tableList = hiveTemplate.query("show tables");
        System.out.println(tableList);
        if(CollectionUtils.isEmpty(tableList)){
            return;
        }
        String tableName=tableList.get(0);
        List<String> showCreateTableState=hiveTemplate.query("show create table "+tableName);
        System.out.println(showCreateTableState);
        
        


    }


    public List<String> getDbs() {
        return hiveTemplate.execute(new HiveClientCallback<List<String>>() {
           @Override
           public List<String> doInHive(HiveClient hiveClient) throws Exception {
              return hiveClient.execute("show databases");
           }
        });
    }
    
    

    @Bean
    public HiveClientFactoryBean hiveClientFactoryBean(DataSource dataSource) {
        HiveClientFactoryBean hiveClientFactoryBean = new HiveClientFactoryBean();
        hiveClientFactoryBean.setHiveDataSource(dataSource);

        return hiveClientFactoryBean;
    }

    @Bean
    public HiveTemplate hiveTemplate(HiveClientFactoryBean hiveClientFactoryBean) {
        return new HiveTemplate(hiveClientFactoryBean.getObject());
    }



    @Bean
    public DataSource hiveDataSource() {
        System.setProperty("socksProxyHost", "10.108.219.37");
        System.setProperty("socksProxyPort", "9876");
        String url = "jdbc:hive2://172.101.105.237:10000";
        String username = "hive";
        String password = "hive";
        return new SimpleDriverDataSource(new HiveDriver(), url, username, password);
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(HiveTemplateTest.class);
        app.setWebEnvironment(false);
        app.run(args);
    }


    //@Autowired 
    //private HiveOperations hiveTemplate; 


    //      @Bean
    //  public HiveTemplate hiveTemplate(){
    //          return new HiveTemplate(new HiveClientFactory() {
    //              
    //              @Override
    //              public HiveClient getHiveClient() {
    //                  // TODO Auto-generated method stub
    //                  return null;
    //              }
    //          });
    //      }  
    //    @Autowired
    //    private HiveClientFactory hiveClientFactory;
    //      @Bean
    //      public HiveTemplate hiveTemplate(DataSource dataSource) throws Throwable {
    //          System.out.println(dataSource.getConnection());
    ////          System.setProperty("socksProxyHost", "10.108.219.37");
    ////          System.setProperty("socksProxyPort", "9876");
    ////          String url = "jdbc:hive2://172.101.105.237:10000";
    ////          String username = "hive";
    ////          String password = "hive";
    ////          DataSource hiveDataSource =new SimpleDriverDataSource(new HiveDriver(), url, username, password);
    ////          
    //          
    //          HiveClientFactoryBean hiveClientFactoryBean= new HiveClientFactoryBean();
    //          hiveClientFactoryBean.setHiveDataSource(dataSource);
    //          
    //          HiveClientFactory hiveClientFactory= hiveClientFactoryBean.getObject();
    //          
    //          return new HiveTemplate(hiveClientFactoryBean.getObject());
    //          //        return new HiveTemplate(new HiveClientFactoryBean().set .getObject());
    //      }

}

package ssl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * https://stackoverflow.com/questions/19723415/java-overriding-function-to-disable-ssl-certificate-check
 * 
 * @author nil4
 *
 */
public class SkipSSL {

    private static Logger logger = LoggerFactory.getLogger(SkipSSL.class);

    // Create a trust manager that does not validate certificate chains
    public static final TrustManager[] ALL_TRUSTING_TRUST_MANAGER = new TrustManager[] {new X509TrustManager() {
        public X509Certificate[] getAcceptedIssuers() {
            return null;
            //            return new X509Certificate[0];//also ok
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {}

        public void checkServerTrusted(X509Certificate[] certs, String authType) {}
    }};


    @Test
    public void testDisableSSL() throws Exception {

        //URL url = new URL("https://google.com/");
        URL url = new URL("https://pie6.rtp.lab.emc.com");

        // Disable first
        request(url, false);

        // Enable; verifies our previous disable isn't still in effect.
        //                request(url, true);
    }

    public static void request(URL url, boolean enableCertCheck) throws Exception {
        BufferedReader reader = null;
        // Repeat several times to check persistence.
        System.out.println("Cert checking=[" + (enableCertCheck ? "enabled" : "disabled") + "]");
        try {

            HttpURLConnection httpConnection = (HttpsURLConnection) url.openConnection();

            // Normally, instanceof would also be used to check the type.
            if (!enableCertCheck) {
                disableSSLForConnection((HttpsURLConnection) httpConnection);
            }

            reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()), 1);

            char[] buf = new char[1024];
            StringBuilder sb = new StringBuilder();
            int count = 0;
            while (-1 < (count = reader.read(buf))) {
                sb.append(buf, 0, count);
            }
            System.out.println(sb.toString());

            reader.close();

        } catch (IOException ex) {
            System.out.println(ex);

            if (null != reader) {
                reader.close();
            }
        }
    }

    /**
     * Overrides the SSL TrustManager and HostnameVerifier to allow all certs and hostnames.
     * WARNING: This should only be used for testing, or in a "safe" (i.e. firewalled) environment.
     * 
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    public static void disableSSLForConnection(HttpsURLConnection connection)
        throws NoSuchAlgorithmException, KeyManagementException {
        //            SSLContext sc = SSLContext.getInstance("SSL");
        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(null, ALL_TRUSTING_TRUST_MANAGER, new java.security.SecureRandom());
        SSLSocketFactory sslSocketFactory = sc.getSocketFactory();
        connection.setSSLSocketFactory(sslSocketFactory);
        // Since we may be using a cert with a different name, we need to ignore
        // the hostname as well.
        connection.setHostnameVerifier(ALL_TRUSTING_HOSTNAME_VERIFIER);
    }


    private static final HostnameVerifier ALL_TRUSTING_HOSTNAME_VERIFIER = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    ////////////////////////////////////////////////////////////////////////////


    @Test
    public void testDisableSSL2() throws Exception {
        disableGlobalSSL();
        //URL url = new URL("https://google.com/");
        URL url = new URL("https://pie6.rtp.lab.emc.com");
        try {

            HttpURLConnection httpConnection = (HttpsURLConnection) url.openConnection();


            try (BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()), 1);) {

                char[] buf = new char[1024];
                StringBuilder sb = new StringBuilder();
                int count = 0;
                while (-1 < (count = reader.read(buf))) {
                    sb.append(buf, 0, count);
                }
                System.out.println(sb.toString());

            }


        } catch (IOException ex) {
            System.out.println(ex);
        }
    }


    public static void disableGlobalSSL() {
        try {
            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, ALL_TRUSTING_TRUST_MANAGER, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = (hostname, session) -> true;

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            logger.error(e.getLocalizedMessage());
        }
    }

}

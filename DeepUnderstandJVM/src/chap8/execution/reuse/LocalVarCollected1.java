package chap8.execution.reuse;

public class LocalVarCollected1 {

    //-verbose:gc
    public static void main(String[] args) {
        {
            byte[] placeholder = new byte[64 * 1024 * 1024];
        }
        //! reuse slot here
        int a = 0;
        System.gc();
        //Still not collected
//        [GC (System.gc())  67533K->66176K(125952K), 0.0009707 secs]
        //! collected !!
//            [Full GC (System.gc())  66176K->529K(125952K), 0.0066404 secs]

    }

}

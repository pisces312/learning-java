package basic.testgoods;

/**
 * 一个更怪的例子
 * 
 * @author pisces
 */
public class Goods2 {

    private void internalTracking(boolean b) {
        /**
         * 你不能在if之外创建这个内部类的对象，因为这已经超出了它的作用域。不过在编译的时候，内部类TrackingSlip和其他类一样同时被编译，只不过它由它自己的作用域，
         * 超出了这个范围就无效，除此之外它和其他内部类并没有区别。
         */
        if (b) {
            class TrackingSlip {

                private String id;

                TrackingSlip(String s) {
                    id = s;
                }

                String getSlip() {
                    return id;
                }
            }
            TrackingSlip ts = new TrackingSlip("slip");
            ts.getSlip();
        }
    }

    public void track() {
        internalTracking(true);
    }

    public static void main(String[] args) {
        Goods2 g = new Goods2();
        g.track();
    }
}

package powermocktest;

import static org.junit.Assert.fail;
import static org.powermock.api.mockito.PowerMockito.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.util.StringUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest(XTest.X.class)
public class XTest {



    public static final class X {
        public void y() throws Exception {
            // This class will be mocked
            new MyClass();
        }
    }
    public static final class MyClass {
        public MyClass() {
            System.out.println("real MyClass");
        }

        public static String staticMethod() {
            return "real static method";
        }

    }

    @Test
    public void testPrepareForTest() {
        String msg = "mocked MyClass";
        try {
            whenNew(XTest.MyClass.class).withNoArguments().thenThrow(new Exception(msg));
        } catch (Exception e) {
            fail("should not have exception");
        }

        // Mock the new instance in x.y()
        X x = new X();

        try {
            x.y();
            fail("mocked MyClass should be called");
        } catch (Exception e) {
            Assert.assertEquals(msg, e.getMessage());
        } // y is the method doing "new MyClass()"

    }

    @Test
    public void testStaticMethod() {
        String msg = "mocked static method";

        mockStatic(MyClass.class);
        when(MyClass.staticMethod()).thenReturn(msg);

        Assert.assertEquals(msg, MyClass.staticMethod());
    }


    // ////////////////////////////////////////////////////////////////////


    @Mock
    private List<String> list;

    @Test
    public void testInterface() {
        list.add("a");
        list.add("b");
        Assert.assertTrue(list.getClass().toString().contains("EnhancerByMockitoWithCGLIB"));
        Assert.assertNull(StringUtils.toStringArray(list));
    }



}

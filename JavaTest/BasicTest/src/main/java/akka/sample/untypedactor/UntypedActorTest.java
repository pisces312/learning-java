package akka.sample.untypedactor;

import static akka.pattern.Patterns.gracefulStop;

import java.util.concurrent.TimeUnit;

import org.apache.commons.beanutils.PropertyUtils;
import org.junit.Test;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class UntypedActorTest {

    @Test
    public void testGracefulStop() {
        Config config = ConfigFactory.parseString("akka {loglevel = DEBUG}");
        // ActorSystem is a heavy object: create only one per application
        final ActorSystem system = ActorSystem.create("HelloActorSystem", config);


        final ActorRef myUntypedActor = system.actorOf(MyUntypedActor.props(), "MyUntypedActor");

        Future<Boolean> stopped =
            gracefulStop(myUntypedActor, Duration.create(5, TimeUnit.SECONDS),
                MyUntypedActor.GRACEFUL_STOP);
        try {
            Await.result(stopped, Duration.create(5, TimeUnit.SECONDS));
//            System.out.println(stopped);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }

    @Test
    public void testCreation() {
        Config config = ConfigFactory.parseString("akka {loglevel = DEBUG}");
        // ActorSystem is a heavy object: create only one per application
        final ActorSystem system = ActorSystem.create("HelloActorSystem", config);


        final ActorRef myUntypedActor = system.actorOf(MyUntypedActor.props(), "MyUntypedActor");
        System.out.println(myUntypedActor);
        final ActorRef myUntypedActor2 =
            system.actorOf(Props.create(MyUntypedActor.class), "MyUntypedActor2");
        System.out.println(myUntypedActor2);
        final ActorRef myUntypedActor3 = system.actorOf(Props.create(MyUntypedActor.class));
        System.out.println(myUntypedActor3);


        // Communicate with actors outside of the actor
        // It will use internal actors
        // If you want to send a msg to an actor outside an actor, it needs Inbox to send
        final Inbox inbox = Inbox.create(system);
        inbox.send(myUntypedActor, "hello");
        try {
            Object recv = inbox.receive(Duration.create(1, TimeUnit.SECONDS));
            System.out.println(recv);
        } catch (java.util.concurrent.TimeoutException e) {
            // timeout
        }

        inbox.send(myUntypedActor, new MyUntypedActor.ReplyableMsg("ask for reply"));
        try {
            Object recv = inbox.receive(Duration.create(1, TimeUnit.SECONDS));
            System.out.println(recv);
        } catch (java.util.concurrent.TimeoutException e) {
            // timeout
        }

        inbox.send(myUntypedActor, new MyUntypedActor.MyBlockMsg("block msg"));

        inbox.send(myUntypedActor, new MyUntypedActor.MyNonBlockMsg("non-blocking msg"));

        // myUntypedActor2.tell("hello2", ActorRef.noSender());
        // inbox.send(myUntypedActor2, msg);
        inbox.watch(myUntypedActor2);
        myUntypedActor2.tell(PoisonPill.getInstance(), ActorRef.noSender());
        try {
            Object obj = inbox.receive(Duration.create(1, TimeUnit.SECONDS));
            assert obj instanceof Terminated;
            System.out.println(obj);
        } catch (java.util.concurrent.TimeoutException e) {
            // timeout
        }

    }

}

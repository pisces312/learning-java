package akka.sample.untypedactor;

import static akka.pattern.Patterns.gracefulStop;

import java.util.concurrent.TimeUnit;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.PoisonPill;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class MySubUntypedActor extends UntypedActor {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    public static final String SHUTDOWN = "shutdown";

    @Override
    public void onReceive(Object message) throws Exception {
//        System.out.println(message);
        if (SHUTDOWN.equals(message)) {
//            System.out.println("Stopping child");
            //Do some clean up work
            Thread.sleep(3000);
            //        if (message instanceof PoisonPill) {
            //            
            //            Thread.sleep(5000);
//            System.out.println("Child is stopped");
            getContext().stop(getSelf());
        }
    }
    @Override
    public void postStop() throws Exception {
        log.info("{} is doing postStop.", getSelf());
        super.postStop();
    }

}

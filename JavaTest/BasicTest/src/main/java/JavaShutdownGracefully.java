
public class JavaShutdownGracefully {

  public static void main(String[] args) {
    System.out.println("add shutdown hook");
    
    /**
     * Not support ctrl+c
     */
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        System.out.println("Inside Add Shutdown Hook");
      }
    });
    
    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}

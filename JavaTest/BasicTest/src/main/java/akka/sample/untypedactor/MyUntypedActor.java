package akka.sample.untypedactor;



import static akka.pattern.Patterns.gracefulStop;

import java.util.concurrent.TimeUnit;

import org.apache.commons.beanutils.MethodUtils;

import scala.Option;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Procedure;

/**
 * 160320
 * 
 * @author nil4
 *
 */
public class MyUntypedActor extends UntypedActor {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    private final ActorRef child = getContext().actorOf(Props.create(MySubUntypedActor.class), "myChild");

    //Send Terminated message if child is stopped
    private final ActorRef watcher = getContext().watch(child);
    private ActorRef lastSender = getContext().system().deadLetters();
    

    private final static int CHILD_TIMEOUT = 2;

    public static final String GRACEFUL_STOP = "gracefulstop";

    //    public static final String SHUTDOWN = "shutdown";
    /**
     * Hotswap
     * 
     * Akka supports hotswapping the Actor’s message loop (e.g. its implementation) at runtime. Use
     * the getContext().become method from within the Actor. The hotswapped code is kept in a Stack
     * which can be pushed (replacing or adding at the top) and popped
     * 
     * the actor will revert to its original behavior when restarted by its Supervisor
     */
    Procedure<Object> shuttingdownMsgLoop = new Procedure<Object>() {
        @Override
        public void apply(Object message) {
            if (message instanceof Terminated) {
                Terminated terminated = (Terminated) message;
                log.info("{} is terminated in shutdown phase.", terminated.actor());
                if (terminated.getActor() == child) {
                    lastSender.tell("finished", getSelf());
                }
                
                //When all children are terminated, begin to stop
                log.info("{} starts stopping", getSelf());
                getContext().stop(getSelf());
                log.info("{} is stopped", getSelf());
            } else {
                getSender().tell("service unavailable, shutting down", getSelf());
            }
        }
    };


    public MyUntypedActor() {
        // To set an initial delay
        getContext().setReceiveTimeout(Duration.create("30 seconds"));
    }

    /**
     * Another good practice is to declare what messages an Actor can receive as close to the actor
     * deﬁnition as possible (e.g. as static classes inside the Actor or using other suitable
     * class), which makes it easier to know what it can receive.
     */
    public static class ReplyableMsg {
        private String msg;

        public ReplyableMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        @Override
        public String toString() {
            return msg;
        }
    }
    public static class MyNonBlockMsg {
        private String msg;

        public MyNonBlockMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        @Override
        public String toString() {
            return msg;
        }
    }
    public static class MyBlockMsg {
        private String msg;

        public MyBlockMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        @Override
        public String toString() {
            return msg;
        }
    }


    /************************************************************
     * Handlers
     * **********************************************************/
    public void handle(String str) {
        log.info("Handle String message: {}", str);
        if (GRACEFUL_STOP.equals(str)) {
            try {
                //Stop child
                log.info("{} starts stopping.", watcher);
                Future<Boolean> stopped =
                    gracefulStop(watcher, Duration.create(CHILD_TIMEOUT, TimeUnit.SECONDS),
                        MySubUntypedActor.SHUTDOWN);
                Await.result(stopped, Duration.create(CHILD_TIMEOUT, TimeUnit.SECONDS));
                log.info("{} is stopped gracefully.", watcher);
            } catch (Exception e) {
                log.warning(
                    "{} wasn't stopped within {} seconds. Send Terminated to supervisor now.",
                    watcher, CHILD_TIMEOUT);
            }
            
            //Only shutdown action to stop parent
            getContext().become(shuttingdownMsgLoop);
        } else {
            getSender().tell(str + " received", getSelf());
        }
        // If sender is null, don't need to reply
        // getSender().tell(message + " received",null);
    }


    /**
     * Cover child terminate in common logic
     * 
     * @param terminated
     */
    public void handle(Terminated terminated) {
        log.info("{} is terminated not in parent shutdown phase.", terminated.actor());
    }

    public void handle(PoisonPill msg) {
        log.info("Handle PoisonPill message: {}", msg);
        getContext().stop(getSelf());
    }

    ///////////////////////////////////////////////////////////////////
    public void handle(ReplyableMsg str) {
        log.info("Handle ReplyableMsg message: {}", str);
        getSender().tell(str.getMsg() + " received", getSelf());
        // Don't need to reply
        // getSender().tell(message + " received",null);
    }

    public void handle(MyNonBlockMsg msg) {
        log.info("Handle MyNonBlockMsg message: {}", msg);
    }

    public void handle(MyBlockMsg msg) {
        log.info("Handle MyBlockMsg message: {}", msg);
    }



    public void handle(Object msg) {
        unhandled(msg);
    }

    @Override
    public void onReceive(Object message) throws Exception {
        //        if (message == null) {
        //            System.out.println("null");
        //            return;
        //        }
        //        System.out.println(message);
        MethodUtils.invokeMethod(this, "handle", message);
    }

    //    @Override
    //    public void onReceive(Object message) throws Exception {
    //        if (message instanceof String) {// Echo
    //            log.info("Received String message: {}", message);
    //            getSender().tell(message + " received", getSelf());
    //
    //            //Don't need to reply
    //            // getSender().tell(message + " received",null);
    //        } else if (message instanceof MyNonBlockMsg) {
    //            System.out.println(message);
    //
    //        } else if (message instanceof MyBlockMsg) {
    //            System.out.println(message);
    //
    //
    //        } else if (message instanceof PoisonPill) {
    //            System.out.println("stop");
    //            getContext().stop(getSelf());
    //        } else {
    //            unhandled(message);
    //        }
    //    }

    /**
     * [bestpractice] Create Props for an actor of this type.
     * 
     * It is a good idea to provide static factory methods on the UntypedActor which help keeping
     * the creation of suitable Props as close to the actor deﬁnition as possible. This also allows
     * usage of the Creator-based methods which statically verify that the used constructor actually
     * exists instead relying only on a runtime check.
     * 
     * @return a Props for creating this actor, which can then be further configured (e.g. calling
     *         ‘.withDispatcher()‘ on it)
     */
    public static Props props() {
        return Props.create(new MyActorCreator());
    }

    /**
     * Life-cycle methods
     */
    @Override
    public void preRestart(Throwable reason, Option<Object> message) throws Exception {
        super.preRestart(reason, message);
    }

    @Override
    public void preStart() throws Exception {
        //        System.out.println(child);
    }

    @Override
    public void postRestart(Throwable reason) throws Exception {
        super.postRestart(reason);
    }

    @Override
    public void postStop() throws Exception {
        log.info("{} is doing postStop.", getSelf());
        super.postStop();
    }



}

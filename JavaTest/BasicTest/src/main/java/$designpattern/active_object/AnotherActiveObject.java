package $designpattern.active_object;

import java.util.concurrent.ForkJoinPool;

import org.junit.Test;

public class AnotherActiveObject {
    private double val;

    // container for tasks
    // decides which request to execute next
    // asyncMode=true means our worker thread processes its local task queue in the FIFO order
    // only single thread may modify internal state
    private final ForkJoinPool fj = new ForkJoinPool(1,
        ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);

    // implementation of active object method
    public void doSomething() throws InterruptedException {
        fj.execute(new Runnable() {
            @Override
            public void run() {
                val = 1.0;
            }
        });
    }

    // implementation of active object method
    public void doSomethingElse() throws InterruptedException {
        fj.execute(new Runnable() {
            @Override
            public void run() {
                val = 2.0;
            }
        });
    }
    
    
    @Test
    public void test() throws Exception{
        new AnotherActiveObject().doSomething();
        
    }
}

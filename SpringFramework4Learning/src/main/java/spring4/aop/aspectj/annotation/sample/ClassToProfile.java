package spring4.aop.aspectj.annotation.sample;

import org.springframework.stereotype.Component;

@Component
public class ClassToProfile {
    
    public boolean handle() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

}

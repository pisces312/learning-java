package core;

public class InfiniteRecursiveLoop {
    int count = 0;

    public String foundType() {
        count++;
        System.out.println(count);
        if (count > 100) {
            return null;
        }
        return this.foundType();
    }

    public static void main(String[] args) {
        new InfiniteRecursiveLoop().foundType();
    }
}

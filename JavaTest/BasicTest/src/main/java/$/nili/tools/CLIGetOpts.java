package $.nili.tools;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Director CLI GetOpts implementation for parsing command line (argv) arguments. This
 * implementation resembles the PERL getopts implementation.
 *
 * <P>
 * To use this class: <br>
 * <LI>First create a usage array with the valid arguments<br>
 * <LI>Then create a <code>CLIGetOpts</code> object with the usage array<br>
 * <LI>Call the <code>parseCommandLine</code> method on this object with the argv array to be parsed
 * <br>
 * <LI>Use the <code>argumentGiven</code> and <code>argumentValue</code> methods to determine if
 * arguments were in argv and any values given to them.
 * <P>
 * Example Usage:
 * <P>
 * <code>
 *  String usage [] = {<br>
 *      "n|nodes=",<br>
 *  };<br>
 *      CLIGetOpts parser = new CLIGetOpts(usage);<br>
 *      argv = parser.parseCommandLine(argv); //argv is now the array at the end of the arguments<br>
 *      if(parser.argumentGiven("n|nodes")){<br>
 *      String nodes = parser.argumentValue("n|nodes"); //only for arguments with strings<br>
 *      }<br>
 * </code>
 * 
 * @author Santosh Bs1
 * @version 1.1 07/05/22 10:12:45
 */
public class CLIGetOpts {

    private HashMap<String, Boolean> usagevalues; // given usage
    private HashMap<String, String> aliases; // maps n to n|nodes
    private HashMap<String, String> optionvalues; // option values
    private HashMap<String, String> optiongiven; // indicates if an option was specified in argv
    private boolean relaxed = false;

    // public static String rootDir = System.getProperty("twg.directory.root");
    // public static final String BACKLISTED_HTMLTAGS_FILE_PATH = rootDir + File.separator + "lwi" +
    // File.separator
    // + "runtime" + File.separator + "isc"
    // + File.separator
    // + "BlackListedHtmlTags.properties";
    // private static List<String> blackListedHtmlTags = new ArrayList<String>();
    // private String failingRegx = null;

    // public static final String SECURITY_AUDIT_CATEGORY_ID = "Security";
    // public static final String REQUEST_REJECTED_ACTIONID = "REQUEST_REJECTED";

    /**
     * Constructs the CLIGetOpts object with the valid usage array. This array should contain all
     * the valid options that can be given in argv.
     *
     * @param usage string array of valid usage options. Options that take a value must end with a
     *        =. Options that are synonymous should be in one string and separated by a |. Example:
     *        "n|nodes="
     */
    public CLIGetOpts(String usage[]) { // constructor

        aliases = new HashMap<String, String>(); // maps "n" to "n|nodes"
        usagevalues = new HashMap<String, Boolean>(); // maps "n|nodes" to if it takes a value

        // first put usage into a nice hash
        for (int i = 0; i < usage.length; i++) {

            boolean takes_string = false;
            String argument = usage[i];

            if (argument.charAt(argument.length() - 1) == '=') {
                takes_string = true;
                argument = argument.substring(0, argument.length() - 1); // take off the =
            }

            usagevalues.put(argument, new Boolean(takes_string));

            String args[] = argument.split("\\|");

            for (int j = 0; j < args.length; j++) {
                aliases.put(args[j], argument);
            }
        }

    }

    /**
     * Pase arguments ignoring error conditions
     */
    void setRelaxedParse(boolean _relaxed) {
        relaxed = _relaxed;
    }

    /**
     * Parses the given arguments and compares them to the valid usage stored in the object.
     *
     * @param argv string array from the command line
     * @return array of the remaining argv (everything after the arguments)
     * @throws CLIInvalidArgumentException if an invalid argument was given in argv.
     * @throws InvalidInputStringException
     */
    public String[] parseCommandLine(String argv[]) throws Exception {
        // todo: consider supporting -- to mean end of options

        // boolean isvalue = false; //set this when we have found an argument that takes a value
        String newargv[] = null;
        optionvalues = new HashMap<String, String>();
        optiongiven = new HashMap<String, String>();
        // now go through argv and make sure everthing that starts with - is in the usageargs hash
        for (int i = 0; i < argv.length; i++) {
            String option = argv[i];
            String opt_short = null;
            String name;

            if (Pattern.matches("--\\S.*", option)) {
                // double dash
                opt_short = option.substring(2);
                name = (String) aliases.get(opt_short);

                boolean valid = aliases.containsKey(opt_short);
                if (!valid && relaxed) {
                    continue;
                }
                if (!valid || opt_short.length() <= 1) {
                    throw new Exception("Invalid option: " + option);
                }

                // see if it takes a value
                boolean takes_string = takesValue(name);
                if (takes_string) {
                    i = putNextValue(name, i, argv);
                } else { // does not take a value
                    optiongiven.put(name, "");
                }
            } else if (Pattern.matches("-\\S.*", option)) {
                // single dash
                opt_short = option.substring(1);

                // look at first letter and see if it takes an option
                for (int j = 0; j < opt_short.length(); j++) {
                    String opt = opt_short.substring(j, j + 1);

                    // see if this option exists
                    if (!aliases.containsKey(opt)) {
                        if (relaxed) {
                            continue;
                        }
                        throw new Exception("Invalid option: " + opt);
                    }

                    // option exists
                    name = (String) aliases.get(opt);
                    boolean takes_string = takesValue(name);
                    if (takes_string) {
                        // see if there are remaining characters
                        if (j < opt_short.length() - 1) { // characters left
                            String next_value = opt_short.substring(j + 1, opt_short.length());
                            optionvalues.put(name, next_value);
                            optiongiven.put(name, "");
                        } else {
                            // else use the next value in argv
                            i = putNextValue(name, i, argv);
                        }
                        break;
                    } else {
                        optiongiven.put(name, "");
                    }

                }
            } else {
                // Does not start with a dash (start of operands)
                if (relaxed) {
                    continue;
                }
                newargv = new String[argv.length - i];
                for (int j = i; j < argv.length; j++) {
                    newargv[j - i] = argv[j];
                }
                break;
            }
        }

        if (newargv == null) {
            newargv = new String[0];
        }
        return newargv;
    }

    /**
     * Returns the number of options given with this command call.
     *
     * @return int Number of options given; 0 if parseCommandLine was not called yet
     */
    public int getNumOptionsGiven() {
        if (optiongiven == null)
            return 0;
        else
            return optiongiven.size();
    }

    private boolean takesValue(String name) {
        // see if it takes a value
        Boolean takes_string = (Boolean) usagevalues.get(name);
        if (takes_string == null) {
            return false;
        }
        return takes_string.booleanValue();
    }

    private int putNextValue(String name, int i, String argv[]) throws Exception {
        // todo: check to see that an alias of the option has not already put a value in the hash
        if (i + 1 >= argv.length) {
            if (relaxed) {
                return i + 1;
            }
            throw new Exception("No value provided to option: " + name);
        }
        String value = argv[i + 1];

        optionvalues.put(name, value);
        optiongiven.put(name, "");

        i++; // increament i by 1
        return i;
    }

    /**
     * Returns the string value given to the argument. Throws exceptions if the argument was not
     * specified in argv, or if it was not specified in the usage array.
     *
     * @param arg argument name string. Example: "n|nodes"
     * @return the string value given to the argument
     * @throws CLIOptionNotSpecifiedInArgvException if the given option was not in argv
     * @throws CLIOptionNotInUsageException if you ask for the value of an argument that you did not
     *         define in the usage array
     */
    public String argumentValue(String arg) throws Exception {

        // remove trailing '=' if the client left it on the option
        if (arg.charAt(arg.length() - 1) == '=') {
            arg = arg.substring(0, arg.length() - 1);
        }

        // see if its in usage values
        if (!usagevalues.containsKey(arg)) {
            if (!aliases.containsKey(arg)) {
                throw new Exception(
                    "Programming error: this option was not specified in usage array = " + arg);
            } else {
                arg = (String) aliases.get(arg);
            }
        }

        if (!argumentGiven(arg)) {
            // option was not specified in argv, throw exception
            throw new Exception("This value was not specified in argv");
        }

        // see if it takes a value
        if (!takesValue(arg)) {
            throw new Exception("Programming error: this option does not take an argument = " + arg);
        }

        return (String) optionvalues.get(arg);
    }

    /**
     * This returns true if the argument was specified and false if it was not.
     *
     * @param arg argument name string. Example: "n|nodes"
     * @return boolean for whether or not the arument was givin in argv
     * @throws CLIOptionNotInUsageException if you ask for the value of an argument that you did not
     *         define in the usage array
     */
    public boolean argumentGiven(String arg) throws Exception {

        // remove trailing '=' if the client left it on the option
        if (arg.charAt(arg.length() - 1) == '=') {
            arg = arg.substring(0, arg.length() - 1);
        }

        // first see if it was given with as specified
        if (optiongiven.containsKey(arg)) {
            return true;
        } else if (aliases.containsKey(arg)) { // check aliases
            String name = (String) aliases.get(arg);
            if (optiongiven.containsKey(name)) {
                return true;
            }
            return false;
        } else if (!usagevalues.containsKey(arg)) {
            throw new Exception(
                "Programming error: this option was not specified in usage array = " + arg);
        }
        return false;
    }

}

import guidvalidator.AssetGUID;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.groups.Default;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration
// @SpringApplicationConfiguration(SpringBootConfig.class)
// @Configuration
// extends AbstractJUnit4SpringContextTests
// @SpringBootApplication
public class URLRegExpTest {



    @Autowired
    private AssetGUID guid;

    // public AssetGUID getGuid() {
    // return guid;
    // }

    public void useGUID(@Valid AssetGUID guid) throws Exception {

    }

    @Test
    public void testGUID() {
        // getGuid();
        // AssetGUID guid = new AssetGUID();
        // // guid.setGuid("aaed0ed2-ac1e-4ea3-bb00-489ac5c1b999");
        // // guid.setGuid("aaed0ed2-ac1e-4ea3-bb00-489ac5c1b99*");
        // guid.setGuid("aaed0ed2-ac1e-4ea3-b*");
        //
        try {
            useGUID(guid);
        } catch (Exception e) {
            // // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }



    @Test
    public void testGUIDValidator() {
        AssetGUID guid = new AssetGUID();
        guid.setGuid("aaed0ed2-ac1e-4ea3-bb00-489ac5c1b999");
        guid.setGuid("aaed0ed2-ac1e-4ea3-bb00-489ac5c1b99*");
        // guid.setGuid("asdf");
        // guid="rty";


        // ValidationResult result = ValidationUtils.validateEntity(se);
        // System.out.println("--------------------------");
        // System.out.println(result);
        // Assert.assertTrue(result.isHasErrors());



        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<AssetGUID>> constraintViolations =
            validator.validate(guid, Default.class);

        for (ConstraintViolation<AssetGUID> constraintViolation : constraintViolations) {
            System.out.println(constraintViolation.getMessage());
        }
        if (constraintViolations.size() == 0) {
            System.out.println("valid");
        }
        // assertNotNull(validator);


        // setGuid("aaed0ed2-ac1e-4ea3-bb00-489ac5c1b99");
        // guid="aaed0ed2-ac1e-4ea3-bb00-489ac5c1b99";
        // guid="aaed0ed2-ac1e-4ea3-bb00-489ac5c1b999";
        // System.out.println(guid);
        // guid="aaed0ed2-ac1e-4ea3-bb00-489ac5c1b99";
        // System.out.println(guid);
        // guid="rty";
        // System.out.println(guid);
    }

    /**
     * http https
     */
    @Test
    public void test_http_https_ftp_Validation() {
        String[] shouldMatch =
            {"http://foo.com/blah_blah", "http://foo.com/blah_blah/",
                "http://foo.com/blah_blah_(wikipedia)",
                "http://foo.com/blah_blah_(wikipedia)_(again)",
                "http://www.example.com/wpstyle/?p=364",
                "https://www.example.com/foo/?bar=baz&inga=42&quux", "http://âœªdf.ws/123",
                "http://userid:password@example.com:8080",
                "http://userid:password@example.com:8080/", "http://userid@example.com",
                "http://userid@example.com/", "http://userid@example.com:8080",
                "http://userid@example.com:8080/", "http://userid:password@example.com",
                "http://userid:password@example.com/", "http://142.42.1.1/",
                "http://142.42.1.1:8080/", "http://âž¡.ws/ä¨¹", "http://âŒ˜.ws", "http://âŒ˜.ws/",
                "http://foo.com/blah_(wikipedia)#cite-1",
                "http://foo.com/blah_(wikipedia)_blah#cite-1",
                "http://foo.com/unicode_(âœª)_in_parens",
                "http://foo.com/(something)?after=parens", "http://â˜º.damowmow.com/",
                "http://code.google.com/events/#&product=browser", "http://j.mp",
                "ftp://foo.bar/baz", "http://foo.bar/?q=Test%20URL-encoded%20stuff",
                "http://Ù…Ø«Ø§Ù„.Ø¥Ø®ØªØ¨Ø§Ø±", "http://ä¾‹å­�.æµ‹è¯•",
                "http://à¤‰à¤¦à¤¾à¤¹à¤°à¤£.à¤ªà¤°à¥€à¤•à¥�à¤·à¤¾",
                "http://-.~_!$&'()*+,;=:%40:80%2f::::::@example.com", "http://1337.net",
                "http://a.b-c.de", "http://223.255.255.254"

            // "http://10.102.1.218/root"
            };

        String[] shouldNotMatch =
            {"http://", "http://.", "http://..", "http://../", "http://?", "http://??",
                "http://??/", "http://#", "http://##", "http://##/",
                "http://foo.bar?q=Spaces should be encoded", "//", "//a", "///a", "///",
                "http:///a", "foo.com", "rdar://1234", "h://test", "http:// shouldfail.com",
                ":// should fail", "http://foo.bar/foo(bar)baz quux", "ftps://foo.bar/",
                "http://-error-.invalid/", "http://a.b--c.de/", "http://-a.b.co", "http://a.b-.co",
                "http://0.0.0.0", "http://10.1.1.0", "http://10.1.1.255", "http://224.1.1.1",
                "http://1.1.1.1.1", "http://123.123.123", "http://3628126748",
                "http://.www.foo.bar/", "http://www.foo.bar./", "http://.www.foo.bar./",
                "http://10.1.1.1"};
        // Can work
        // String
        // regexp="^(http(?:s)?\\:\\/\\/[a-zA-Z0-9]+(?:(?:\\.|\\-)[a-zA-Z0-9]+)+(?:\\:\\d+)?(?:\\/[\\w\\-]+)*(?:\\/?|\\/\\w+\\.[a-zA-Z]{2,4}(?:\\?[\\w]+\\=[\\w\\-]+)?)?(?:\\&[\\w]+\\=[\\w\\-]+)*)$";

        // !!not allow 10.x.x ip
        String regexp =
            "^(?:(?:https?|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\.254(?:\\.\\d{1,3}){2})(?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|"
                + "(?:(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)*(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}]{2,})))(?::\\d{2,5})?(?:/[^\\s]*)?$";
        Pattern pattern = Pattern.compile(regexp);
        // String test = "http://example.com:8080";
        // String test = "http://userid:password@example.com:8080";
        // String test = "http://userid:password@example.com:8080/";
        for (String url : shouldMatch) {
            Matcher matcher = pattern.matcher(url);
            boolean b = matcher.matches();
            // System.out.println(b);
            System.out.println(url);
            org.junit.Assert.assertTrue(b);
        }

        for (String url : shouldNotMatch) {
            Matcher matcher = pattern.matcher(url);
            boolean b = matcher.matches();
            // System.out.println(b);
            System.out.println(url);
            org.junit.Assert.assertFalse(b);
        }

    }

    @Test
    public void testURIAndURL() {
        String filePath = "file:///C:\\Dev\\Git\\ReleaseNotes.html";
        try {
            URL url = new URL(filePath);
            System.out.println(url);
            System.out.println(url.toURI());
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // InputStream in = null;
        // try {
        // in = new URL(filePath).openStream();
        // // in.
        // // IOUtils.copyBytes(in, System.out, 4096, false);
        // } catch (MalformedURLException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // } finally {
        // // IOUtils.closeStream(in);
        // }
        catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // @Test
    // public void testRemoteFileURL(){
    // URL url = new URL("file://UserID:password@server_ip/path of dir/filename.txt");
    // URLConnection connection = url.openConnection();
    // logger.info("connection -->"+connection);
    // logger.info("connection 11111-->"+connection.getURL());
    // connection.setDoInput(true);
    // InputStream inStream = connection.getInputStream();
    // logger.info("inStream -->"+inStream.toString());
    // BufferedReader input = new BufferedReader(new InputStreamReader(
    // inStream));
    // String line = "";
    // monitors = new HashMap<String, String>();
    // String[] str = null;
    // while ((line = input.readLine()) != null){
    // logger.info("Files in the folder are -->"+line);
    // /*str = line.split(":");
    // monitors.put(str[0], str[1]);
    // logger.info(str[0]+" --- "+str[1]);*/
    // }
    // }

    // need allow hostname
    // no user/password
    @Test
    public void testHdfs() {
        // String regexp =
        // "^(?:(?:hdfs)://)(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\.254(?:\\.\\d{1,3}){2})(?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)*(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}]{2,})))(?::\\d{2,5})?(?:/[^\\s]*)?$";

        // exclude 10/192/127/172
        String regexp =
            "^(?:(?:hdfs)://)" // hdfs:
                + "(?:" // ip
                + "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])"
                + "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}"
                + "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))"
                + "|" // hostname
                + "(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)"
                + "(?:\\.(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)*"
                + "|"
                + "(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)"
                + "|" + "(?:/)?" // local /
                + ")" + "(?::\\d{2,5})?" // port, optional
                + "(?:/[^\\s]*)?$";
        // "^(?:(?:hdfs)://)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)*(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}]{2,})))(?::\\d{2,5})?(?:/[^\\s]*)?$";



        String regexp2 =
            "^(?:(?:hdfs)://)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)*|(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)|(?:/)?)(?::\\d{2,5})?(?:/[^\\s]*)?$";



        Pattern pattern = Pattern.compile(regexp);
        String[] hdfsUrls =


            {"abcd", "hdf://example.com:8080", "hdfs://example.com:8080", "hdfs://a.b.c.d:8080",
                "hdfs://10.102.1.218/root/install.log", "hdfs://localhost/root/install.log",
                "hdfs://KevinCentOS/root/install.log", "hdfs:///root/install.log",

            };

        for (String url : hdfsUrls) {
            Matcher matcher = pattern.matcher(url);
            boolean b = matcher.matches();
            System.out.println(b);
            System.out.println(url);
            // org.junit.Assert.assertFalse(b);
        }


    }

    public static void test1(String[] args) {

        String regexp =
            "^(?:(?:file)://)" // file:
                + "(?:\\S+(?::\\S*)?@)?"
                + "(?:" // ip
                + "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])"
                + "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}"
                + "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))"
                + "|" // hostname
                + "(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)"
                + "(?:\\.(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)*"
                + "|"
                + "(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)"
                + "|" + "(?:/)?" // local /
                + ")"
                // + "(?::\\d{2,5})?" //port, optional
                + "(?:/[^\\s]*)?$";

        // String regexp2 =
        // "^(?:(?:file)://)?(?:\\S+(?::\\S*)?@)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)*|(?:(?:[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-zA-Z\\x{00a1}-\\x{ffff}0-9]+)|(?:/)?)(?::\\d{2,5})?(?:/[^\\s]*)?$";

        // String regexp2 =
        // "^(?:(?:file)://)"
        // + "(?:\\S+(?::\\S*)?@)?"
        // +
        // "(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)*(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}]{2,})))(?::\\d{2,5})?(?:/[^\\s]*)?$";


        Pattern pattern = Pattern.compile(regexp);

        String[] fileUrls = {"file://localhost/", "file://userid:password@example.com",

        "file://a:b@10.102.1.218/root/install.log", "file://KevinCentOS/root/install.log",

            //
            "file://userid:password@example.com:8080",

        };

        for (String url : fileUrls) {
            Matcher matcher = pattern.matcher(url);
            boolean b = matcher.matches();
            System.out.println(b);
            System.out.println(url);
            // org.junit.Assert.assertFalse(b);
        }



        // String hostnameReg="(?:[a-z\\u00a1-\\uffff0-9]-?)*[a-z\\u00a1-\\uffff0-9]";
        //
        // Pattern pattern2 = Pattern.compile(hostnameReg);
        // Matcher matcher = pattern.matcher("abcd");
        // boolean b = matcher.matches();
        // System.out.println(b);
        // System.out.println(url);

    }
}

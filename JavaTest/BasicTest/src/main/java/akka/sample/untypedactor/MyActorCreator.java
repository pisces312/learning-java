package akka.sample.untypedactor;

import akka.japi.Creator;

public class MyActorCreator implements Creator<MyUntypedActor>{

    /**
     * 
     */
    private static final long serialVersionUID = -3661636616397778480L;

    @Override
    public MyUntypedActor create() throws Exception {
        return new MyUntypedActor();
    }

}

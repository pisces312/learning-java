package chap9.rpc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;


public class MyRPCMain {



    private static String prepareSourceFile(boolean isDelete) throws Exception {
        String className = "RPCTestMain";
        String srcFilePath = className + ".java";

        //Java source file content
        String sourceFileStr = "public class " + className + " {\r\n"
            + "    public static void main(String[] args) {\r\n"
            + "        System.out.println(\"pass\");\r\n" + "    }\r\n" + "}";

        File file = new File(srcFilePath);
        if (file.exists()) {
            if (isDelete) {
                file.delete();
            } else {
                return srcFilePath;
            }
        }

        try (FileWriter fw = new FileWriter(file);) {
            fw.write(sourceFileStr);
        }
        return srcFilePath;
    }

    private static String compile(String srcFilePath) {
        //if src file is not changed, will not change class file
        com.sun.tools.javac.Main.compile(new String[] {srcFilePath});
        int idx = srcFilePath.lastIndexOf('.');
        return srcFilePath.substring(0, idx) + ".class";
    }

    private static byte[] readClassAsStream(String classFilePath) throws Exception {
        InputStream is = new FileInputStream(classFilePath);
        int len = is.available();
        byte[] b = new byte[len];
        is.read(b);
        is.close();
        return b;
    }

    public static void main(String[] args) throws Exception {
        //Client prepares source file and transfers to server
        String srcFilePath = prepareSourceFile(false);

        //Server compiles source file
        String classFilePath = compile(srcFilePath);

        //Server reads class file as stream
        byte[] b = readClassAsStream(classFilePath);

        //Server runs class
        System.out.println("Start RPC");
        System.out.println(JavaClassExecuter.execute(b));

    }

}

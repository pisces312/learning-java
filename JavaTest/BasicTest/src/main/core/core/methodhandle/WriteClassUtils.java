package core.methodhandle;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WriteClassUtils {
    public static void writeToFile(String targetClassName, byte[] classByteArr)
        throws IOException {
//      Path path = Paths.get(targetClassName + ".class");
        Path path = Paths.get("target/classes/" + targetClassName + ".class");
        System.out.println(path.toFile().getAbsolutePath());
        Files.deleteIfExists(path);
        //Write new class file
        Files.write(path, classByteArr);
    }
}

package json;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import $.nili.tools.JsonUtil;


public class JsonInjectionTest {
    //    String outputJsonFileName = "user_info.json";


    private String injectedJsonStr() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();


        String username = "admin";
        String password = "123456\",\"role\":\"admin";
        JsonFactory jfactory = new JsonFactory();

        JsonGenerator jGenerator = jfactory.createGenerator(baos, JsonEncoding.UTF8);

        //        JsonGenerator jGenerator =
        //            jfactory.createGenerator(new File(outputJsonFileName), JsonEncoding.UTF8);

        jGenerator.writeStartObject();
        jGenerator.writeFieldName("username");
        jGenerator.writeRawValue("\"" + username + "\"");
        jGenerator.writeFieldName("password");

        jGenerator.writeRawValue("\"" + password + "\"");
        jGenerator.writeFieldName("role");
        jGenerator.writeRawValue("\"default\"");
        jGenerator.writeEndObject();
        jGenerator.close();
        return baos.toString();
    }

    private Map<String, String> parseToMap(String content) throws Exception {
        Map<String, String> userInfo = new HashMap<>();

        JsonFactory jfactory = new JsonFactory();
        JsonParser jParser = jfactory.createParser(content);
        while (jParser.nextToken() != JsonToken.END_OBJECT) {
            String fieldname = jParser.getCurrentName();
            if ("username".equals(fieldname)) {
                jParser.nextToken();
                userInfo.put(fieldname, jParser.getText());
            }
            if ("password".equals(fieldname)) {
                jParser.nextToken();
                userInfo.put(fieldname, jParser.getText());
            }
            if ("role".equals(fieldname)) {
                jParser.nextToken();
                userInfo.put(fieldname, jParser.getText());
            }
            if (userInfo.size() == 3)
                break;
        }
        jParser.close();
        return userInfo;
    }

    @Test
    public void testInjection() throws Exception {
        String jsonStr = injectedJsonStr();
        System.out.println(jsonStr);

        Map<String, String> userInfo = parseToMap(jsonStr);
        System.out.println(userInfo);

    }


    //Use writeString instead of writeRaw
    @Test
    public void testFixInjection() throws Exception {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();


        String username = "admin";
        String password = "123456\",\"role\":\"admin";
        JsonFactory jfactory = new JsonFactory();

        JsonGenerator jGenerator = jfactory.createGenerator(baos, JsonEncoding.UTF8);



        jGenerator.writeStartObject();
        jGenerator.writeFieldName("username");
        jGenerator.writeString(username);
        jGenerator.writeFieldName("password");
        jGenerator.writeString(password);
        jGenerator.writeFieldName("role");
        jGenerator.writeString("default");
        jGenerator.writeEndObject();
        jGenerator.close();

        System.out.println(baos);

        Map<String, String> userInfo = parseToMap(baos.toString());
        for (Map.Entry<String, String> entry : userInfo.entrySet()) {
            System.out.println("key:" + entry.getKey());
            System.out.println("value:" + entry.getValue());
        }
        //        System.out.println(userInfo);
    }

    @Test

    public void test() {
        //        String jsonStr = "{\"role\":\"default,\"role\":\\\"admin\\\"\"}";
        String value = "123456\",\"role\":\"admin";
        String jsonStr = String.format("{\"role\":\"%s\"}", value);//  "{\"role\":\"default,\\\"role\\\":\\\"admin\\\"\"}";

        System.out.println(jsonStr);
        Map<String, String> map = JsonUtil.fromJson(jsonStr, Map.class);
        System.out.println(map);


        jsonStr = "{\"role\":\"default,\\\"role\\\":\\\"admin\\\"\"}";
        System.out.println(jsonStr);
        map = JsonUtil.fromJson(jsonStr, Map.class);

        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println("key:" + entry.getKey());
            System.out.println("value:" + entry.getValue());
        }

    }



}
